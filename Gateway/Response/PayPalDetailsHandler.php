<?php
namespace Eye4Fraud\Connector\Gateway\Response;

use Eye4Fraud\Connector\Gateway\SubjectReaderFactory;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Model\Order\Payment;

/**
 * Handler to catch PayPal details from Braintree PayPal version
 */
class PayPalDetailsHandler implements HandlerInterface
{
    public const PAYER_ID = 'payerId';
    public const PAYER_STATUS = 'payerStatus';

    /** @var \PayPal\Braintree\Gateway\Helper\SubjectReader */
    public $subjectReader;

    /**
     * Constructor
     *
     * @param SubjectReaderFactory $factory
     */
    public function __construct(SubjectReaderFactory $factory)
    {
        $this->subjectReader = $factory->create();
    }

    /**
     * @inheritdoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function handle(array $handlingSubject, array $response)
    {
        if (!$this->subjectReader) {
            return;
        }
        $paymentDO = $this->subjectReader->readPayment($handlingSubject);

        /** @var \Braintree\Transaction $transaction */
        $transaction = $this->subjectReader->readTransaction($response);

        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();

        $payPal = $this->subjectReader->readPayPal($transaction);
        $payment->setAdditionalInformation(self::PAYER_ID, $payPal[self::PAYER_ID]);
        $payment->setAdditionalInformation(self::PAYER_STATUS, $payPal[self::PAYER_STATUS]);
    }
}
