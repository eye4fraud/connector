<?php
namespace Eye4Fraud\Connector\Gateway;

use Magento\Framework\App\Config\Base;
use Magento\Framework\ObjectManagerInterface;

/**
 * SubjectReaderFactory for backward compatibility
 */
class SubjectReaderFactory
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create config model
     *
     * @return Base
     */
    public function create()
    {
        $object = null;
        if (class_exists(\Magento\Braintree\Gateway\SubjectReader::class)) {
            $object = $this->_objectManager->create(\Magento\Braintree\Gateway\SubjectReader::class);
        }
        if (class_exists(\Magento\Braintree\Gateway\Helper\SubjectReader::class)) {
            $object = $this->_objectManager->create(\Magento\Braintree\Gateway\Helper\SubjectReader::class);
        }
        if (class_exists(\PayPal\Braintree\Gateway\Helper\SubjectReader::class)) {
            $object = $this->_objectManager->create(\PayPal\Braintree\Gateway\Helper\SubjectReader::class);
        }
        return $object;
    }
}
