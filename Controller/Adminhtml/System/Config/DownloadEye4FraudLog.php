<?php

namespace Eye4Fraud\Connector\Controller\Adminhtml\System\Config;

use Exception;
use Eye4Fraud\Connector\Helper\Files;
use Magento\Backend\App\Action\Context;
use Magento\Config\Controller\Adminhtml\System\AbstractConfig;
use Magento\Config\Model\Config\Structure;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Config\Controller\Adminhtml\System\ConfigSectionChecker;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;

class DownloadEye4FraudLog extends AbstractConfig
{
    /**
     * @var FileFactory
     */
    public $fileFactory;
    /**
     * @var Files
     */
    public $filesHelper;

    /**
     * @var File
     */
    private File $ioFile;

    /**
     * @param Context $context
     * @param Structure $configStructure
     * @param ConfigSectionChecker $sectionChecker
     * @param FileFactory $fileFactory
     * @param Files $filesHelper
     * @param File $ioFile
     */
    public function __construct(
        Context $context,
        Structure $configStructure,
        ConfigSectionChecker $sectionChecker,
        FileFactory $fileFactory,
        Files $filesHelper,
        File $ioFile
    ) {
        $this->fileFactory = $fileFactory;
        $this->filesHelper = $filesHelper;
        $this->ioFile = $ioFile;
        parent::__construct($context, $configStructure, $sectionChecker);
    }

    /**
     * Export shipping table rates in csv format
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        $fileName = 'eye4fraud-debug.log';
        $file = $this->getRequest()->getParam('file');
        if ($file) {
            $logFiles = $this->filesHelper->getLogFiles();
            if (isset($logFiles[$file])) {
                $fileName = $file;
            }
        }

        $fileData = $this->ioFile->getPathInfo($fileName);
        $contentType = match ($fileData['extension']) {
            'zip' => 'application/zip',
            default => 'text/plain',
        };

        $content['type'] = 'filename';
        $content['value'] = $fileName;

        try {
            $result = $this->fileFactory->create(
                $fileName,
                $content,
                DirectoryList::LOG,
                $contentType
            );
        } catch (Exception $e) {
            $this->_redirect('admin');
            $this->messageManager->addErrorMessage($e->getMessage());
            $result = $this->_response;
        }

        return $result;
    }
}
