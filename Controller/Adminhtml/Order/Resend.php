<?php
/**
 * Copyright © 2020 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Controller\Adminhtml\Order;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Store\Model\App\Emulation;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;

class Resend extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    public const ADMIN_RESOURCE = 'Eye4Fraud_Connector::resend_order';

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var string
     */
    protected $redirectUrl = '*/*/';
    /**
     * @var Emulation
     */
    private $emulation;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $orderCollectionFactory
     * @param Emulation $emulation
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $orderCollectionFactory,
        Emulation $emulation,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct($context);
        $this->collectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->emulation = $emulation;
        $this->filter = $filter;
    }

    /**
     * Resend orders
     *
     * @return Redirect|ResponseInterface|\Magento\Framework\Controller\Result\Redirect|ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $countPreparationOrder = 0;
            /** @var Order $order */
            foreach ($collection->getItems() as $order) {
                $this->emulation->startEnvironmentEmulation($order->getStoreId());
                $this->_eventManager->dispatch(
                    'eye_fraud_resend_order',
                    ['payment' => $order->getPayment(), 'resend_order' => 1]
                );
                $this->emulation->stopEnvironmentEmulation();
                $countPreparationOrder++;
            }

            if ($countPreparationOrder) {
                $this->messageManager->addSuccessMessage(__('Processed %1 orders.', $countPreparationOrder));
            } else {
                $this->messageManager->addErrorMessage(__('No orders was processed.'));
            }
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath($this->filter->getComponentRefererUrl() ?: 'sales/*/');
            return $resultRedirect;
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->redirectUrl);
        }
    }
}
