<?php
/**
 * Copyright © 2024 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Controller\Adminhtml\Order;

use Exception;
use Eye4Fraud\Connector\Helper\Statuses;
use Eye4Fraud\Connector\Model\ResourceModel\Status\Collection;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;

class Status extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    public const ADMIN_RESOURCE = 'Eye4Fraud_Connector::resend_order';

    /**
     * @var CollectionFactory
     */
    public $collectionFactory;

    /**
     * @var Filter
     */
    public $filter;

    /**
     * @var OrderRepositoryInterface
     */
    public $orderRepository;
    /**
     * @var string
     */
    public $redirectUrl = '*/*/';

    /**
     * @var Collection
     */
    public $statusCollection;

    /**
     * @var Statuses
     */
    public $statusesHelper;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $orderCollectionFactory
     * @param Collection $statusCollection
     * @param Statuses $statusesHelper
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $orderCollectionFactory,
        Collection $statusCollection,
        Statuses $statusesHelper,
        OrderRepositoryInterface $orderRepository
    ) {
        parent::__construct($context);
        $this->collectionFactory = $orderCollectionFactory;
        $this->statusCollection = $statusCollection;
        $this->orderRepository = $orderRepository;
        $this->statusesHelper = $statusesHelper;
        $this->filter = $filter;
    }

    /**
     * Resend orders
     *
     * @return Redirect|ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {

            /** @var OrderCollection $collection */
            $collection = $this->filter->getCollection($this->collectionFactory->create());

            $countPreparationOrder = $collection->count();
            if ($countPreparationOrder) {
                $this->statusesHelper->updateFromGridAction($collection);
                $this->messageManager->addSuccessMessage(__('Processed %1 orders.', $countPreparationOrder));
            } else {
                $this->messageManager->addErrorMessage(__('No orders was processed.'));
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath($this->filter->getComponentRefererUrl() ?: 'sales/*/');
            return $resultRedirect;
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->redirectUrl);
        }
    }
}
