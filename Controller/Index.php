<?php
/**
 */
namespace Eye4Fraud\Connector\Controller;

use Magento\Framework\App\Action\Action;
use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

/**
 * Index: Show cards and form for the default or chosen payment method.
 */
class Index extends Action
{
    /**
     * @var SavedCardRepositoryInterface
     */
    protected $savedCardRepository;

    /**
     * @var GatheredOrderData
     */
    protected $info;

    /**
     * Index constructor.
     * @param SavedCardRepositoryInterface $savedCardRepository
     * @param GatheredOrderData $info
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        SavedCardRepositoryInterface $savedCardRepository,
        GatheredOrderData $info,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->savedCardRepository = $savedCardRepository;
        $this->info = $info;
        parent::__construct($context);
    }

    /**
     * Payment data index page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
//        $savedCard = $this->savedCardRepository->create();
//        $savedCard->setMethod('test');
//        $savedCard->setCardId('111');
//        $savedCard->setCachedData([]);
//        $this->savedCardRepository->save($savedCard);

        return '';
    }
}
