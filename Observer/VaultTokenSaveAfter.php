<?php
namespace Eye4Fraud\Connector\Observer;

use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Vault\Model\PaymentToken;

class VaultTokenSaveAfter implements ObserverInterface
{
    /**
     * @var SavedCardRepositoryInterface
     */
    protected $savedCardRepository;

    /**
     * @var GatheredOrderData
     */
    protected $info;

    /**
     * @param SavedCardRepositoryInterface $savedCardRepository
     * @param GatheredOrderData $info
     */
    public function __construct(
        SavedCardRepositoryInterface $savedCardRepository,
        GatheredOrderData $info
    ) {
        $this->savedCardRepository = $savedCardRepository;
        $this->info = $info;
    }

    /**
     * Execute event
     *
     * @param Observer $observer
     * @return mixed
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var PaymentToken $card */
        $card = $observer->getData('data_object');
        /** Card marked for deletion */
        if ($card->getIsActive() === 0) {
            $savedCard = $this->savedCardRepository->loadCard($card->getPaymentMethodCode(), $card->getGatewayToken());
            if ($savedCard!==false) {
                $this->savedCardRepository->delete($savedCard);
            }
            return $card;
        }

        if (!$this->info->CCFirst6) {
            return $card;
        }

        $cachedData = [
            'CCFirst6' => $this->info->CCFirst6,
            'CCLast4' =>  $this->info->CCLast4
        ];

        $savedCard = $this->savedCardRepository->loadCard($card->getPaymentMethodCode(), $card->getGatewayToken());

        if (!$savedCard) {
            $savedCard = $this->savedCardRepository->create();
            $savedCard->setPaymentMethod($card->getPaymentMethodCode());
            $savedCard->setCardId($card->getGatewayToken());
        }

        $savedCard->setCachedData($cachedData);
        $this->savedCardRepository->save($savedCard);

        return $card;
    }
}
