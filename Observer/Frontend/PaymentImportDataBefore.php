<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Observer\Frontend;

use Exception;
use Eye4Fraud\Connector\Model\Payment\PaymentInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Directory\Model\Region;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Payment;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

use Eye4Fraud\Connector\Helper\RequestHelper;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Eye4Fraud\Connector\Model\Request;
use Eye4Fraud\Connector\Model\Payment\Factory as PaymentFactory;

/**
 * Class AddVatRequestParamsOrderComment
 */
class PaymentImportDataBefore implements ObserverInterface
{
    /** @var RequestHelper */
    public $helper;
    /** @var GatheredOrderData */
    public $info;
    /** @var ScopeConfigInterface */
    public $scopeConfig;
    /** @var Region */
    public $region;
    /** @var LoggerInterface */
    public $logger;
    /** @var Request */
    public $delayedRequest;
    /** @var PaymentFactory */
    public $paymentFactory;
    /** @var SerializerInterface */
    public $serializer;

    /**
     * @param RequestHelper $helper
     * @param GatheredOrderData $info
     * @param ScopeConfigInterface $scopeConfig
     * @param Request $delayedRequest
     * @param Region $region
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     * @param PaymentFactory $paymentFactory
     */
    public function __construct(
        RequestHelper $helper,
        GatheredOrderData $info,
        ScopeConfigInterface $scopeConfig,
        Request $delayedRequest,
        Region $region,
        LoggerInterface $logger,
        SerializerInterface $serializer,
        PaymentFactory $paymentFactory
    ) {
        $this->helper = $helper;
        $this->info = $info;
        $this->scopeConfig = $scopeConfig;
        $this->region = $region;
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->delayedRequest = $delayedRequest;
        $this->paymentFactory = $paymentFactory;
    }

    /**
     * Add VAT validation request date and identifier to order comments
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE
        ) == "1";
        if (!$enabled) {
            return;
        }

        /** @var Payment $paymentMethod */
        $paymentMethod = $observer->getData('payment');
        $paymentData = $observer->getData('input');

        $skipMethods = $this->scopeConfig->getValue(
            'eye4fraud_connector/additional/skip_methods'
        );
        if ($skipMethods) {
            $skipMethods = explode("\n", $skipMethods);
            $skipMethods = array_map('trim', $skipMethods);

            if (in_array($paymentData->getMethod(), $skipMethods)) {
                return;
            }
        }

        if (!$paymentMethod->getQuoteId()) {
            return;
        }

        $paymentMethodFiller = $this->paymentFactory->create($paymentData->getMethod());

        if ($paymentMethodFiller === null) {
            $this->helper->log('Payment method "' . $paymentData->getMethod() . '" not supported');
            return;
        }

        if (!($paymentMethodFiller instanceof PaymentInterface)) {
            return;
        }

        $this->delayedRequest->load($paymentMethod->getQuoteId(), 'quote_id');
        if (!$this->delayedRequest->isEmpty()) {
            try {
                /** @var array $request_data */
                $requestData = $this->serializer->unserialize($this->delayedRequest->getData('request_data'));
            } catch (\InvalidArgumentException $e) {
                $this->helper->log('Error while unserialize data for quote #'.$paymentMethod->getQuoteId());
                return;
            }
            $this->info->fill($requestData);
        }

        $updateFlag = $paymentMethodFiller->fillFirstCardDigits($this->info, $paymentData);

        if ($updateFlag) {
            $this->helper->log('Save first 6 digits info for quote_id ' . $paymentMethod->getQuoteId());
            $this->delayedRequest->setData('quote_id', $paymentMethod->getQuoteId());
            $this->delayedRequest->setData('payment_method', $paymentMethod->getMethod());
            $requestData = $this->serializer->serialize(get_object_vars($this->info));
            $this->delayedRequest->setData('request_data', $requestData);
            try {
                $this->delayedRequest->save();
            } catch (Exception $e) {
                $this->helper->log('Error while saving delayed request:');
                $this->helper->log($e->getMessage());
            }
        }
    }
}
