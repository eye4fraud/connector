<?php
/**
 * Copyright © 2013-2023 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Observer\Frontend;

use Exception;
use Eye4Fraud\Connector\Model\Payment\UpdateInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderDataFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

use Eye4Fraud\Connector\Api\RequestRepositoryInterface;
use Eye4Fraud\Connector\Helper\RequestHelper;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusFactory;
use Eye4Fraud\Connector\Model\Payment\Factory as PaymentFactory;
use Eye4Fraud\Connector\Model\Request;

/**
 * Event - after order save
 */
class SalesOrderSaveAfter implements ObserverInterface
{
    /** @var RequestHelper */
    public $helper;
    /** @var ScopeConfigInterface */
    public $scopeConfig;
    /** @var LoggerInterface */
    public $logger;
    /** @var StatusFactory */
    public $statusFactory;
    /** @var Status  */
    public $status;
    /** @var RequestRepositoryInterface */
    public $requestRepository;
    /** @var PaymentFactory */
    public $paymentFactory;
    /** @var SerializerInterface */
    public $serializer;
    /** @var GatheredOrderDataFactory */
    public $infoFactory;

    /**
     * @param RequestHelper $helper
     * @param ScopeConfigInterface $scopeConfig
     * @param StatusFactory $statusFactory
     * @param RequestRepositoryInterface $requestRepository
     * @param GatheredOrderDataFactory $infoFactory
     * @param PaymentFactory $paymentFactory
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestHelper $helper,
        ScopeConfigInterface $scopeConfig,
        StatusFactory $statusFactory,
        RequestRepositoryInterface $requestRepository,
        GatheredOrderDataFactory $infoFactory,
        PaymentFactory $paymentFactory,
        SerializerInterface $serializer,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->statusFactory = $statusFactory;
        $this->paymentFactory = $paymentFactory;
        $this->serializer = $serializer;
        $this->requestRepository = $requestRepository;
        $this->infoFactory = $infoFactory;
    }

    /**
     * Update a fraud status item with generated order_id
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE
        ) == "1";
        if (!$enabled) {
            return;
        }

        $this->addOrderIdToStatus($observer);
        $this->updateCachedRequest($observer);
        $this->saveStatus();
    }

    /**
     * Add order ID to status object, required in some payment processes
     *
     * @param Observer $observer
     * @return void
     */
    public function addOrderIdToStatus(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');
        if (time() - strtotime($order->getData('created_at')) > 400) {
            return;
        }

        $status = $this->getStatusObject($order->getIncrementId());
        if ($status->isEmpty()) {
            $this->helper->log('Status for order #' . $order->getIncrementId() . ' not found');
        } elseif ($status->getData('order_id')) {
            $this->helper->log('Order ID of order #' . $order->getIncrementId().
               ' already set to fraud status object and '.
               (
                   $status->getData('order_id') == $order->getId() ?
                   "match" :
                   "not match to " . $order->getId()
               ));
        } else {
            $status->setData('order_id', $order->getId());
        }
    }

    /**
     * Activate or remove cached order data
     *
     * @param Observer $observer
     * @return void
     */
    public function updateCachedRequest(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');
        $stateChanged = $order->getOrigData('state') !== $order->getData('state');
        if (!$stateChanged) {
            return;
        }

        switch ($order->getData('state')) {
            case Order::STATE_PROCESSING:
                $this->setRequestReady($order);
                break;
            case Order::STATE_CANCELED:
            case Order::STATE_CLOSED:
                $this->removeCachedRequest($order);
                break;
        }
    }

    /**
     * Set cached request as active for this order if state was changed to processing
     *
     * @param Order $order
     * @return void
     */
    public function setRequestReady(Order $order)
    {
        $collection = $this->requestRepository->getNotActiveRequests($order->getIncrementId());
        if (!$collection->count()) {
            return;
        }
        $paymentMethod = $order->getPayment();
        $paymentMethodFiller = $this->paymentFactory->create($paymentMethod->getMethod());

        /** @var Request $cachedRequest */
        foreach ($collection->getItems() as $cachedRequest) {
            $cachedRequest->setData('is_ready', 1);
            if ($paymentMethodFiller instanceof UpdateInterface) {
                try {
                    /** @var array $request_data */
                    $requestData = $this->serializer->unserialize($cachedRequest->getData('request_data'));
                    $info = $this->infoFactory->create();
                    $info->fill($requestData);
                    $result = $paymentMethodFiller->updateFromPayment($paymentMethod, $info);
                    if ($result) {
                        $requestData = $this->serializer->serialize(get_object_vars($info));
                        $cachedRequest->setData('request_data', $requestData);
                    }
                } catch (Exception $e) {
                    $this->helper->log('Error while unserialize data for #'.$order->getIncrementId());
                    return;
                }
            }
        }
        $collection->save();
    }

    /**
     * Remove cached and not active request on order cancel or close
     *
     * @param Order $order
     * @return void
     */
    public function removeCachedRequest(Order $order)
    {
        $collection = $this->requestRepository->getNotActiveRequests($order->getIncrementId());
        if (!$collection->count()) {
            return;
        }

        /** @var Request $cachedRequest */
        foreach ($collection->getItems() as $cachedRequest) {
            $this->requestRepository->delete($cachedRequest);
        }
    }

    /**
     * Load and cache status object
     *
     * @param string $incrementId
     * @return Status
     */
    public function getStatusObject(string $incrementId): Status
    {
        if (!$this->status) {
            $this->status = $this->statusFactory->create();
            $this->status->loadByIncrementId($incrementId);
        }
        return $this->status;
    }

    /**
     * Save status object if it was loaded and have data changes
     *
     * @return void
     */
    public function saveStatus()
    {
        if (!$this->status || !$this->status->hasDataChanges()) {
            return;
        }

        try {
            $this->status->save();
        } catch (Exception $e) {
            $this->helper->log('Error while saving a fraud status: '.$e->getMessage());
        }
    }
}
