<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Observer\Frontend;

use Exception;
use InvalidArgumentException;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Paypal\Model\AbstractConfig;
use Magento\Paypal\Model\Info as PayPalInfo;
use Magento\Paypal\Model\PayflowExpress;
use Magento\Sales\Model\Order;
use Magento\Directory\Model\Region;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

use Eye4Fraud\Connector\Helper\Order\Config;
use Eye4Fraud\Connector\Helper\Order\Management;
use Eye4Fraud\Connector\Helper\RequestHelper;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Eye4Fraud\Connector\Model\Request;
use Eye4Fraud\Connector\Model\Payment\Factory as PaymentFactory;

/**
 * PayPal IPN postdispatch event
 */
class PayPalIpnPostDispatch implements ObserverInterface
{
    /** @var RequestHelper */
    public $helper;
    /** @var Config */
    public $orderConfig;
    /** @var GatheredOrderData */
    public $info;
    /** @var ScopeConfigInterface */
    public $scopeConfig;
    /** @var Region */
    public $region;
    /** @var LoggerInterface */
    public $logger;
    /** @var Request */
    public $delayedRequest;
    /** @var PaymentFactory */
    public $paymentFactory;
    /** @var Order */
    public $order;
    /** @var OrderFactory */
    public $orderFactory;
    /** @var Management */
    public $orderManagement;
    /** @var SerializerInterface */
    public $serializer;

    /**
     * @param RequestHelper $helper
     * @param Config $orderConfig
     * @param Management $orderManagement
     * @param GatheredOrderData $info
     * @param ScopeConfigInterface $scopeConfig
     * @param Request $delayedRequest
     * @param Region $region
     * @param LoggerInterface $logger
     * @param PaymentFactory $paymentFactory
     * @param OrderFactory $orderFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        RequestHelper $helper,
        Config $orderConfig,
        Management $orderManagement,
        GatheredOrderData $info,
        ScopeConfigInterface $scopeConfig,
        Request $delayedRequest,
        Region $region,
        LoggerInterface $logger,
        PaymentFactory $paymentFactory,
        OrderFactory $orderFactory,
        SerializerInterface $serializer
    ) {
        $this->helper = $helper;
        $this->orderConfig = $orderConfig;
        $this->orderManagement = $orderManagement;
        $this->info = $info;
        $this->scopeConfig = $scopeConfig;
        $this->region = $region;
        $this->logger = $logger;
        $this->delayedRequest = $delayedRequest;
        $this->paymentFactory = $paymentFactory;
        $this->orderFactory = $orderFactory;
        $this->serializer = $serializer;
    }

    /**
     * Process PayPal IPN notification
     *
     * @param Observer $observer
     * @return void
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $this->helper->log('=====  PayPal Ipn post dispatch event');
        $enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE
        ) == "1";
        if (!$enabled) {
            return;
        }

        /** @var RequestInterface $request */
        $request = $observer->getData('request');

        $this->_getOrder($request);

        if (!$this->order->getId()) {
            $this->helper->log('PayPal Ipn: order not loaded - ' . $request->getParam('invoice'));
            return;
        }

        /** @var Payment $paymentMethod */
        $paymentMethod = $this->order->getPayment();

        $skipMethods = $this->scopeConfig->getValue(
            'eye4fraud_connector/additional/skip_methods'
        );
        if ($skipMethods) {
            $skipMethods = explode("\n", $skipMethods);
            $skipMethods = array_map('trim', $skipMethods);

            if (in_array($paymentMethod->getMethod(), $skipMethods)) {
                return;
            }
        }

        $this->helper->log('Quote Id:' . $this->order->getQuoteId());
        if ((int)$this->order->getQuoteId() === 0) {
            $this->helper->log('Order skipped, quote id missing');
            return;
        }

        /*if (($this->_orderConfig->enabled() &&
        $this->_orderConfig->getCreateState() != $this->_order->getState()) ||
        (!$this->_orderConfig->enabled() && $this->_order->getState() != Order::STATE_PROCESSING)) {
            $this->_helper->log('Order skipped, order state ' . $this->_order->getState());
            return;
        }*/

        /** @var \Eye4Fraud\Connector\Model\Payment\PayFlowExpress $paymentMethodFiller */
        $paymentMethodFiller = $this->paymentFactory->create($paymentMethod->getMethod());

        if ($paymentMethodFiller===null || !method_exists($paymentMethodFiller, 'processIpnResponse')) {
            $this->helper->log('Payment method "' . $paymentMethod->getMethod() . '" not supported');
            return;
        }

        // If PayPal set to authorize and this is Capture transaction then update order status
        /** @var Payment $payment */
        $payment = $this->order->getPayment();
        try {
            /** @var PayflowExpress  $paymentMethod */
            $paymentMethod = $payment->getMethodInstance();
        } catch (Exception $e) {
            $this->helper->log('Error while getting a payment method instance: ' . $e->getMessage());
            return;
        }

        $paymentStatus = $request->getParam('payment_status');
        if ($paymentMethod->getConfigData('payment_action') === AbstractConfig::PAYMENT_ACTION_AUTH &&
            $paymentMethodFiller->filterPaymentStatus($paymentStatus) === PayPalInfo::PAYMENTSTATUS_COMPLETED) {
            $this->orderManagement->setApprovedStatus($this->order);
            return;
        }

        $this->delayedRequest->load($this->order->getQuoteId(), 'quote_id');
        if ($this->delayedRequest->isEmpty()) {
            $this->helper->log('Delayed order for quote '.$this->order->getQuoteId().' not exists');
            //$this->_delayedRequest->setData('quote_id', $this->_order->getQuoteId());
            return;
        } else {
            $this->helper->log('Prepared request data loaded');
            try {
                $requestData = $this->serializer->unserialize($this->delayedRequest->getData('request_data'));
                $this->info->fill($requestData);
            } catch (InvalidArgumentException $argumentException) {
                $this->helper->log('Error while unserialize request data for #'.$this->delayedRequest->getId());
                return;
            }
        }

        $this->helper->log('Fill payment data from Ipn answer');
        $paymentMethodFiller->processIpnResponse($request, $this->info);

        $requestData = $this->serializer->serialize(get_object_vars($this->info));
        $this->delayedRequest->setData('request_data', $requestData);

        $this->helper->log('Save delayed request');
        $this->delayedRequest->save();
    }

    /**
     * Load order
     *
     * @param RequestInterface $request
     */
    protected function _getOrder(RequestInterface $request)
    {
        $incrementId = $request->getParam('invoice');
        $this->order = $this->orderFactory->create()->loadByIncrementId($incrementId);
    }
}
