<?php

/**
 * Copyright © 2013-2022 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Observer\Frontend;

use Exception;
use Eye4Fraud\Connector\Helper\Order\Management;
use Eye4Fraud\Connector\Model\Payment\PaymentInterface;
use InvalidArgumentException;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Model\Order;
use Magento\Directory\Model\Region;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

use Eye4Fraud\Connector\Helper\RequestHelper;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Eye4Fraud\Connector\Model\Request;
use Eye4Fraud\Connector\Model\RequestFactory;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusFactory;
use Eye4Fraud\Connector\Model\Payment\Factory as PaymentFactory;

/**
 * Class PaymentPlaceAfter
 * vendor/magento/module-sales/Model/Order/Payment.php
 * @event sales_order_payment_place_end
 * @noinspection PhpUnused
 */
class PaymentPlaceAfter implements ObserverInterface
{
    /** @var RequestHelper */
    protected $helper;
    /** @var Management */
    protected $management;
    /** @var GatheredOrderData */
    protected $info;
    /** @var ScopeConfigInterface */
    protected $scopeConfig;
    /** @var Region */
    protected $region;
    /** @var LoggerInterface */
    protected $logger;
    /** @var Request */
    protected $delayedRequest;
    /** @var PaymentFactory */
    protected $paymentFactory;
    /** @var StatusFactory */
    protected $statusFactory;
    /**
     * @var RequestFactory
     */
    public $delayedRequestFactory;

    /**
     * @var GroupRepositoryInterface
     */
    public $groupRepository;
    /** @var SerializerInterface */
    public $serializer;

    /**
     * @param RequestHelper $helper
     * @param Management $management
     * @param GatheredOrderData $info
     * @param ScopeConfigInterface $scopeConfig
     * @param Request $delayedRequest
     * @param RequestFactory $delayedRequestFactory
     * @param StatusFactory $statusFactory
     * @param Region $region
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     * @param GroupRepositoryInterface $groupRepository
     * @param PaymentFactory $paymentFactory
     */
    public function __construct(
        RequestHelper $helper,
        Management $management,
        GatheredOrderData $info,
        ScopeConfigInterface $scopeConfig,
        Request $delayedRequest,
        RequestFactory $delayedRequestFactory,
        StatusFactory $statusFactory,
        Region $region,
        LoggerInterface $logger,
        SerializerInterface $serializer,
        GroupRepositoryInterface $groupRepository,
        PaymentFactory $paymentFactory
    ) {
        $this->helper = $helper;
        $this->management = $management;
        $this->info = $info;
        $this->scopeConfig = $scopeConfig;
        $this->region = $region;
        $this->logger = $logger;
        $this->delayedRequest = $delayedRequest;
        $this->statusFactory = $statusFactory;
        $this->groupRepository = $groupRepository;
        $this->paymentFactory = $paymentFactory;
        $this->serializer = $serializer;
        $this->delayedRequestFactory = $delayedRequestFactory;
    }

    /**
     * Cache data to send it to E4F service later
     *
     * @param Observer $observer
     * @return void
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE
        ) == "1";
        if (!$enabled) {
            return;
        }

        /** @var Payment $paymentMethod */
        $paymentMethod = $observer->getData('payment');

        $skipMethods = $this->scopeConfig->getValue(
            'eye4fraud_connector/additional/skip_methods'
        );
        if ($skipMethods) {
            $skipMethods = explode("\n", $skipMethods);
            $skipMethods = array_map('trim', $skipMethods);

            if (in_array($paymentMethod->getMethod(), $skipMethods)) {
                return;
            }
        }

        /** @var Status $status */
        $status = $this->statusFactory->create();
        /** @var Order $orderInstance */
        $orderInstance = $paymentMethod->getOrder();

        $this->helper->log('Quote Id:' . $orderInstance->getQuoteId());
        if ($orderInstance->getQuoteId() === null) {
            $this->helper->log('Order skipped, quote id missing');
            return;
        }

        $this->delayedRequest = $this->delayedRequestFactory->create();
        $this->delayedRequest->load($orderInstance->getQuoteId(), 'quote_id');

        $resendOrder = $observer->getData('resend_order') === 1;
        if ($resendOrder) {
            $this->helper->log(
                '  ==  Resend order. Order state - '
                . $orderInstance->getState().'. Order: ' . $orderInstance->getIncrementId()
                . ' Payment method: ' . $paymentMethod->getMethod()
            );
            $status->loadByIncrementId($orderInstance->getIncrementId());
            if ($status->getId()) {
                $status->setData('created_at', date('Y-m-d H:i:s'));
            }
            if ($this->delayedRequest->getId()) {
                $this->helper->log("Resend cached data for ".$orderInstance->getIncrementId());
                $this->delayedRequest->setData('created_at', date('Y-m-d H:i:s'));
                $this->delayedRequest->setData('send_time', date('Y-m-d H:i:s'));
                $this->delayedRequest->setData('attempts', 0);
                $this->delayedRequest->setData('is_ready', 1);
                $status->setQueued($orderInstance);
                $this->save($status);
                return;
            }
        } else {
            $this->helper->log(
                '  ==  After payment event. Order state - '
                . $orderInstance->getState().'. Order: ' . $orderInstance->getIncrementId()
                . ' Payment method: ' . $paymentMethod->getMethod()
            );
        }

        $allowAllMethods = $this->scopeConfig->isSetFlag('eye4fraud_connector/additional/allow_all_methods');
        if (!$resendOrder && !$allowAllMethods && $orderInstance->getState() != Order::STATE_PROCESSING) {
            $this->helper->log('Order skipped, order state ' . $orderInstance->getState());
            return;
        }

        $paymentMethodFiller = $this->paymentFactory->create($paymentMethod->getMethod());

        if ($paymentMethodFiller === null) {
            $this->helper->log('Payment method "' . $paymentMethod->getMethod() . '" not supported');
            return;
        }

        if (!$this->delayedRequest->isEmpty()) {
            $this->helper->log('Prepared request data loaded');
            try {
                /** @var array $request_data */
                $requestData = $this->serializer->unserialize($this->delayedRequest->getData('request_data'));
            } catch (InvalidArgumentException $e) {
                $this->helper->log('Error while unserialize data for #'.$orderInstance->getIncrementId());
                return;
            }
            $this->info->fill($requestData);
        }

        $this->delayedRequest->setData('is_ready', 1);
        if (!$resendOrder && $orderInstance->getState() != Order::STATE_PROCESSING) {
            $this->helper->log('Order not ready yet, order state ' . $orderInstance->getState());
            $this->delayedRequest->setData('is_ready', 0);
        }

        $this->helper->log('Fill common info');
        $this->fillCommonInfo($orderInstance, $status);
        $this->helper->log('Fill payment data');
        if ($paymentMethodFiller instanceof PaymentInterface) {
            $paymentMethodFiller->fillPaymentInfo($paymentMethod, $this->info);
        }

        if ($this->info->SiteName===null || $this->info->ApiLogin===null || $this->info->ApiKey===null) {
            return;
        }

        if (!$this->info->CIDResponse) {
            $this->info->CIDResponse = 'S';
        }

        if ($this->info->CCLast4) {
            $this->info->CCLast4 = preg_replace('/\D+/', '', $this->info->CCLast4);
        }
        if (strlen($this->info->CCLast4) < 4) {
            $this->info->CCLast4 = '';
        }

        if ($this->info->CCFirst6) {
            $this->info->CCFirst6 = preg_replace('/\D+/', '', $this->info->CCFirst6);
        }
        if (strlen($this->info->CCFirst6) < 6) {
            $this->info->CCFirst6 = '';
        }

        // Check and update CCType
        $this->info->CCType = $this->helper->updateCCType($this->info->CCType);
        $this->info->CustomerComments = "method:".$paymentMethod->getMethod();

        $customerGroup = $this->getCustomerGroup($orderInstance->getCustomerGroupId());
        if ($customerGroup) {
            $this->info->CustomerComments .= ';customerGroup:'.$customerGroup->getCode();
            $this->info->CustomerComments .= ';customerTax:'.$customerGroup->getTaxClassName();
        }
        $salesGroupName = $orderInstance->getData('sales_group_name');
        if ($salesGroupName) {
            $this->info->CustomerComments .= ';salesGroup:'.$salesGroupName;
        }

        $this->delayedRequest->setData('quote_id', $orderInstance->getQuoteId());
        $this->delayedRequest->setData('increment_id', $orderInstance->getIncrementId());
        $requestData = $this->serializer->serialize(get_object_vars($this->info));
        $this->delayedRequest->setData('request_data', $requestData);
        $this->delayedRequest->setData('payment_method', $paymentMethod->getMethod());

        $status->setQueued($orderInstance);
        $this->save($status);

        $this->management->orderCreated($paymentMethod);
    }

    /**
     * Save objects
     *
     * @param Status $status
     * @return void
     */
    public function save($status)
    {
        $this->helper->log('Save delayed request');
        try {
            $this->delayedRequest->save();
        } catch (Exception $e) {
            $this->helper->log('Error while a delayed request was saved: '.$e->getMessage());
        }

        try {
            $status->save();
        } catch (Exception $e) {
            $this->helper->log('Error while a fraud status was saved: '.$e->getMessage());
        }
    }

    /**
     * Fill common info from order
     *
     * @param Order $orderInstance
     * @param Status $status
     */
    protected function fillCommonInfo(Order $orderInstance, Status $status)
    {
        $this->info->SiteName = $this->scopeConfig->getValue(
            'eye4fraud_connector/auth/site_name',
            ScopeInterface::SCOPE_STORE
        );
        $this->info->ApiLogin = $this->scopeConfig->getValue(
            'eye4fraud_connector/auth/login',
            ScopeInterface::SCOPE_STORE
        );
        $this->info->ApiKey = $this->scopeConfig->getValue('eye4fraud_connector/auth/key', ScopeInterface::SCOPE_STORE);

        $billing_address = $orderInstance->getBillingAddress();
        if ($billing_address) {
            $this->info->BillingFirstName = $billing_address->getFirstname();
            $this->info->BillingLastName = $billing_address->getLastname();
            $name_parts = [];
            if (!$this->info->BillingLastName && $this->info->BillingFirstName) {
                $name_parts = explode(' ', $this->info->BillingFirstName);
            }
            if (!$this->info->BillingFirstName && $this->info->BillingLastName) {
                $name_parts = explode(' ', $this->info->BillingLastName);
            }
            if (count($name_parts)>1) {
                $this->info->BillingLastName = array_pop($name_parts);
                $this->info->BillingFirstName = implode(' ', $name_parts);
            }
            $this->info->BillingMiddleName = $billing_address->getMiddlename();
            $this->info->BillingCompany = $billing_address->getCompany();
            $this->info->BillingAddress1 = $billing_address->getStreetLine(1);
            $this->info->BillingAddress2 = $billing_address->getStreetLine(2);
            $this->info->BillingCity = $billing_address->getCity();
            $this->region->loadByName($billing_address->getRegion(), $billing_address->getCountryId());
            $this->info->BillingState = $this->region->getData('code');
            $this->info->BillingCountry = $billing_address->getCountryId();
            $this->info->BillingZip = $billing_address->getPostcode();
            $this->info->BillingEveningPhone = $billing_address->getTelephone();
            $this->info->BillingEmail = $billing_address->getEmail();
        }

        $shipping_address = $orderInstance->getShippingAddress();
        if ($shipping_address) {
            $this->info->ShippingFirstName = $shipping_address->getFirstname();
            $this->info->ShippingLastName = $shipping_address->getLastname();
            $name_parts = [];
            if (!$this->info->ShippingLastName && $this->info->ShippingFirstName) {
                $name_parts = explode(' ', $this->info->ShippingFirstName);
            }
            if (!$this->info->ShippingFirstName && $this->info->ShippingLastName) {
                $name_parts = explode(' ', $this->info->ShippingLastName);
            }
            if (count($name_parts)>1) {
                $this->info->ShippingLastName = array_pop($name_parts);
                $this->info->ShippingFirstName = implode(' ', $name_parts);
            }
            $this->info->ShippingMiddleName = $shipping_address->getMiddlename();
            $this->info->ShippingCompany = $shipping_address->getCompany();
            $this->info->ShippingAddress1 = $shipping_address->getStreetLine(1);
            $this->info->ShippingAddress2 = $shipping_address->getStreetLine(2);
            $this->info->ShippingCity = $shipping_address->getCity();
            $this->region->loadByName($shipping_address->getRegion(), $shipping_address->getCountryId());
            $this->info->ShippingState = $this->region->getData('code');
            $this->info->ShippingCountry = $shipping_address->getCountryId();
            $this->info->ShippingZip = $shipping_address->getPostcode();
            $this->info->ShippingEveningPhone = $shipping_address->getTelephone();
            $this->info->ShippingEmail = $shipping_address->getEmail();
        } elseif ($billing_address) {
            $this->info->ShippingFirstName = $billing_address->getFirstname();
            $this->info->ShippingLastName = $billing_address->getLastname();
            $this->info->ShippingMiddleName = $billing_address->getMiddlename();
            $this->info->ShippingCompany = $billing_address->getCompany();
            $this->info->ShippingAddress1 = $billing_address->getStreetLine(1);
            $this->info->ShippingAddress2 = $billing_address->getStreetLine(2);
            $this->info->ShippingCity = $billing_address->getCity();
            $this->region->loadByName($billing_address->getRegion(), $billing_address->getCountryId());
            $this->info->ShippingState = $this->region->getData('code');
            $this->info->ShippingCountry = $billing_address->getCountryId();
            $this->info->ShippingZip = $billing_address->getPostcode();
            $this->info->ShippingEveningPhone = $billing_address->getTelephone();
            $this->info->ShippingEmail = $billing_address->getEmail();
        }

        $this->info->GrandTotal = doubleval($orderInstance->getBaseGrandTotal());
        $this->info->ShippingCost = doubleval($orderInstance->getBaseShippingAmount());

        $this->info->OrderNumber = $this->helper->getIncrementIdVersion($orderInstance->getIncrementId(), $status);
        $created_at = $orderInstance->getCreatedAt();
        if ($created_at===null) {
            $created_at = date('Y-m-d H:i:s');
        }
        $this->info->OrderDate = $created_at;
        $this->info->IPAddress = $this->getIpAddress();
        $this->info->RawShippingMethod = $orderInstance->getShippingMethod();
        $this->info->ShippingMethod = $orderInstance->getShippingMethod();
        if (!in_array($orderInstance->getShippingMethod(), ['1d', '2d', '3d'])) {
            $this->info->ShippingMethod = 'other';
        }

        $this->info->LineItems = [];
        foreach ($orderInstance->getItems() as $item) {
            $description = $item->getProduct()->getDescription() ?? "";
            /** @var Order\Item $item */
            $item_data = [
                'ProductName' => $item->getSku(),
                'ProductDescription' => urlencode($description),
                'ProductSellingPrice' => doubleval($item->getBasePrice()),
                'ProductQty' => (int)$item->getQtyOrdered(),
                'ProductCostPrice' => doubleval($item->getBaseRowTotal())
            ];
            $this->info->LineItems[] = $item_data;
        }
    }

    /**
     * Detect IP address
     *
     * @return mixed
     */
    protected function getIpAddress()
    {
        // check for shared internet/ISP IP
        if ($this->helper->getServer('HTTP_CLIENT_IP') &&
            $this->validateIp($this->helper->getServer('HTTP_CLIENT_IP'))) {
            return $this->helper->getServer('HTTP_CLIENT_IP');
        }

        // Use Fastly client IP address if found
        if ($this->helper->getServer('HTTP_FASTLY_CLIENT_IP') &&
            $this->validateIp($this->helper->getServer('HTTP_FASTLY_CLIENT_IP'))) {
            return $this->helper->getServer('HTTP_FASTLY_CLIENT_IP');
        }

        // check for IPs passing through proxies
        if ($this->helper->getServer('HTTP_X_FORWARDED_FOR')) {
            // check if multiple ips exist in var
            if (strpos($this->helper->getServer('HTTP_X_FORWARDED_FOR'), ',') !== false) {
                $iplist = explode(',', $this->helper->getServer('HTTP_X_FORWARDED_FOR'));
                foreach ($iplist as $ip) {
                    if ($this->validateIp($ip)) {
                        return $ip;
                    }
                }
            } else {
                if ($this->validateIp($this->helper->getServer('HTTP_X_FORWARDED_FOR'))) {
                    return $this->helper->getServer('HTTP_X_FORWARDED_FOR');
                }
            }
        }
        if ($this->helper->getServer('HTTP_X_FORWARDED') &&
            $this->validateIp($this->helper->getServer('HTTP_X_FORWARDED'))) {
            return $this->helper->getServer('HTTP_X_FORWARDED');
        }
        if ($this->helper->getServer('HTTP_X_CLUSTER_CLIENT_IP') &&
            $this->validateIp($this->helper->getServer('HTTP_X_CLUSTER_CLIENT_IP'))) {
            return $this->helper->getServer('HTTP_X_CLUSTER_CLIENT_IP');
        }
        if ($this->helper->getServer('HTTP_FORWARDED_FOR') &&
            $this->validateIp($this->helper->getServer('HTTP_FORWARDED_FOR'))) {
            return $this->helper->getServer('HTTP_FORWARDED_FOR');
        }
        if ($this->helper->getServer('HTTP_FORWARDED') &&
            $this->validateIp($this->helper->getServer('HTTP_FORWARDED'))) {
            return $this->helper->getServer('HTTP_FORWARDED');
        }

        // return unreliable ip since all else failed
        return $this->helper->getServer('REMOTE_ADDR');
    }

    /**
     * Ensures an ip address is both a valid IP and does not fall within a private network range.
     *
     * @param string $ip
     * @return bool
     */
    protected function validateIp($ip)
    {
        if (strtolower($ip) === 'unknown') {
            return false;
        }

        // generate ipv4 network address
        $ip = ip2long($ip);

        // if the ip is set and not equivalent to 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // make sure to get unsigned long representation of ip
            // due to discrepancies between 32 and 64 bit OSes and
            // signed numbers (ints default to signed in PHP)
            $ip = sprintf('%u', $ip);
            // do private network range checking
            if ($ip >= 0 && $ip <= 50331647) {
                return false;
            }
            if ($ip >= 167772160 && $ip <= 184549375) {
                return false;
            }
            if ($ip >= 2130706432 && $ip <= 2147483647) {
                return false;
            }
            if ($ip >= 2851995648 && $ip <= 2852061183) {
                return false;
            }
            if ($ip >= 2886729728 && $ip <= 2887778303) {
                return false;
            }
            if ($ip >= 3221225984 && $ip <= 3221226239) {
                return false;
            }
            if ($ip >= 3232235520 && $ip <= 3232301055) {
                return false;
            }
            if ($ip >= 4294967040) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get customer group
     *
     * @param int $groupId
     * @return GroupInterface|null
     */
    public function getCustomerGroup($groupId)
    {
        try {
            $group = $this->groupRepository->getById($groupId);
        } catch (LocalizedException $e) {
            $group = null;
        }
        return $group;
    }
}
