<?php
/**
 * Copyright © 2018 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Eye4Fraud\Connector\Observer\Status;

use DateInterval;
use DateTime;
use Exception;
use Eye4Fraud\Connector\Exception\DelayUpdate;
use Eye4Fraud\Connector\Helper\Order\Config;
use Eye4Fraud\Connector\Helper\Order\Management;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Fraud status SaveAfter event
 */
class SaveBefore implements ObserverInterface
{
    /** @var Management */
    protected $management;
    /**
     * @var \Eye4Fraud\Connector\Model\ResourceModel\Status
     */
    private $statusResource;
    /**
     * @var StatusFactory
     */
    private $statusFactory;
    /**
     * @var Config
     */
    private $managementConfig;

    /**
     * SaveAfter constructor.
     * @param Management $management
     * @param Config $managementConfig
     * @param \Eye4Fraud\Connector\Model\ResourceModel\Status $statusResource
     * @param StatusFactory $statusFactory
     */
    public function __construct(
        Management $management,
        Config $managementConfig,
        \Eye4Fraud\Connector\Model\ResourceModel\Status $statusResource,
        StatusFactory $statusFactory
    ) {
        $this->management = $management;
        $this->managementConfig = $managementConfig;
        $this->statusResource = $statusResource;
        $this->statusFactory = $statusFactory;
    }

    /**
     * Call events of status update, increase retries before
     *
     * @param Observer $observer
     * @return void
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Status $fraud_status */
        $fraud_status = $observer->getData('fraud_status');
        if (!$fraud_status) {
            return;
        }

        try {
            $this->management->fraudStatusUpdated($fraud_status);
        } catch (DelayUpdate $e) {
            $currentFraudStatus = $this->statusFactory->create();
            $this->statusResource->load($currentFraudStatus, $fraud_status->getId());
            if ((int)$currentFraudStatus->getAttempts() < $this->managementConfig->getRetryTimes()) {
                // Revert fraud status for next attempt
                $fraud_status->setAttempts($currentFraudStatus->getAttempts() + 1);
                $fraud_status->setStatus($currentFraudStatus->getStatus());
                $fraud_status->setDetails($currentFraudStatus->getDetails());

                $intervalValue = $this->managementConfig->getTimeBetweenRetries();
                $newUpdateAt = new DateTime();
                $interval = new DateInterval("PT{$intervalValue}M");
                $newUpdateAt->add($interval);
                $fraud_status->setUpdatedAt($newUpdateAt->format('Y-m-d H:i:s'));
            } else {
                $this->management->setOrderFailedCapture($fraud_status);
            }
        }
    }
}
