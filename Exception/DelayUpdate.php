<?php

namespace Eye4Fraud\Connector\Exception;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;

/**
 * Custom exception to delay fraud status update
 */
class DelayUpdate extends LocalizedException
{
    /**
     * @param Phrase|null $phrase
     * @param Exception|null $cause
     * @param int $code
     */
    public function __construct(Phrase $phrase = null, Exception $cause = null, $code = 0)
    {
        if ($phrase === null) {
            $phrase = new Phrase('Order Management Failed');
        }
        parent::__construct($phrase, $cause, $code);
    }
}
