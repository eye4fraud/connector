<?php

namespace Eye4Fraud\Connector\Model\Logger;

use Eye4Fraud\Connector\Model\Compress\Factory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File as FileDriver;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Logger\Handler\Base as BaseHandler;

class Base extends BaseHandler
{
    /**
     * @var File
     */
    private File $ioFile;

    /**
     * @var Factory
     */
    private Factory $compressFactory;

    /**
     * @param FileDriver $filesystem
     * @param File $ioFile
     * @param Factory $compressFactory
     * @param string|null $filePath
     * @param string|null $fileName
     */
    public function __construct(
        FileDriver $filesystem,
        File $ioFile,
        Factory $compressFactory,
        ?string $filePath = null,
        ?string $fileName = null
    ) {
        parent::__construct($filesystem, $filePath, $fileName);
        $this->compressFactory = $compressFactory;
        $this->ioFile = $ioFile;
    }

    /**
     * Rotate log file
     *
     * @param int $logsCount
     * @return void
     */
    public function rotateLog($logsCount)
    {
        $logFileData =  $this->ioFile->getPathInfo($this->fileName);
        $suffix = $this->getLogFileSuffix();
        $compressor = $this->compressFactory->create();
        $newRotatedFileName = $logFileData['filename'] . '-' . $suffix;

        if ($compressor->checkExists($newRotatedFileName)) {
            return;
        }

        // Remove old file if limit is set
        if ($logsCount > 0) {
            // Remove old log file
            $oldFile = $logFileData['filename'] . '-' . $this->getLogFileSuffixToDelete($logsCount);
            if ($compressor->checkExists($oldFile)) {
                try {
                    $this->filesystem->deleteFile($compressor->getFileFullPath($oldFile));
                } catch (FileSystemException $e) {
                    $this->write(['context' => '', 'extra' => '', 'formatted' => $e->getMessage()]);
                }
            }
        }

        // Compress current log file
        $compressor->compressFile($this->fileName, $newRotatedFileName);

        if ($compressor->checkExists($newRotatedFileName)) {
            try {
                $this->filesystem->deleteFile($this->url);
            } catch (\Exception $e) {
                $this->write(['context' => '', 'extra' => '', 'formatted' => $e->getMessage()]);
            }
        }
    }

    /**
     * Get a suffix for a new rotated file
     *
     * @return string
     */
    public function getLogFileSuffix()
    {
        $m = date('m');
        $y = date('Y');
        $date = new \DateTime();
        $date->setDate($y, $m, 1);
        $date->modify("-1 day");
        return $date->format("Y-m-d");
    }

    /**
     * Get a suffix for a log file to remove
     *
     * @param int $logsCount
     * @return string
     */
    public function getLogFileSuffixToDelete($logsCount)
    {
        $month = date('m');
        $year = date('Y');
        $month -= $logsCount;
        if ($month < 1) {
            $month += 12;
            $year--;
        }

        $date = new \DateTime();
        $date->setDate($year, $month + 1, 1);
        $date->modify("-1 day");
        return $date->format("Y-m-d");
    }
}
