<?php

namespace Eye4Fraud\Connector\Model;

use Eye4Fraud\Connector\Model\Logger\Base;
use Magento\Framework\Exception\FileSystemException;

/**
 * Class Logger
 * Custom logger, handler inserted via DI
 */
class Logger extends \Monolog\Logger
{
    /**
     * Rotate logs
     *
     * @param int $logsCount
     * @throws FileSystemException
     */
    public function rotateLogs($logsCount)
    {
        /** @var Base $handler */
        foreach ($this->getHandlers() as $handler) {
            $handler->rotateLog($logsCount);
        }
    }
}
