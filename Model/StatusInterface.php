<?php

namespace Eye4Fraud\Connector\Model;

interface StatusInterface
{
    public const STATUS_APPROVED = 'A';
    public const STATUS_REVIEW = 'R';
    public const STATUS_DECLINED = 'D';
    public const STATUS_PENDING_INSURANCE = 'P';
    public const STATUS_UNPROCESSED = 'U';
    public const STATUS_INSURED = 'I';
    public const STATUS_CANCELED = 'C';
    public const STATUS_FRAUD = 'F';
    public const STATUS_MISSED_FRAUD = 'M';
    public const STATUS_QUEUED = 'Q';
    public const STATUS_AWAITING_RESPONSE = 'W';
    public const STATUS_ERROR = 'E';
    public const STATUS_NO_ORDER = 'N';
    public const STATUS_ERROR2 = 'ERR';
    public const STATUS_ERROR_ANSWER = 'RER';
    public const STATUS_NOT_RECEIVED = 'NR';
    public const STATUS_INVALID = 'INV';
    public const STATUS_ALLOWED = 'ALW';
}
