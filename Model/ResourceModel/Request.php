<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Flat sales order resource
 *
 * @author      Eye4Fraud
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Request extends AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eye4fraud_requests_cache', 'entity_id');
    }
}
