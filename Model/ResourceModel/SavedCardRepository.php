<?php
/**
 * Saved Cards repository
 */

namespace Eye4Fraud\Connector\Model\ResourceModel;

use Exception;
use Eye4Fraud\Connector\Api\Data\SavedCardInterface;
use Eye4Fraud\Connector\Api\Data\SavedCardInterfaceFactory;
use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\ResourceModel\SavedCard\Collection;
use Eye4Fraud\Connector\Model\ResourceModel\SavedCard\CollectionFactory;
use Eye4Fraud\Connector\Model\SavedCard as SavedCardAlias;
use Eye4Fraud\Connector\Model\SavedCardFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Vault\Api\PaymentTokenManagementInterface;

/**
 * Class CardRepository
 */
class SavedCardRepository implements SavedCardRepositoryInterface
{
    /**
     * @var SavedCard
     */
    public $resource;

    /**
     * @var SavedCardInterfaceFactory
     */
    public $savedCardInterfaceFactory;

    /**
     * @var SavedCardFactory
     */
    public $savedCardFactory;

    /**
     * @var CollectionFactory
     */
    public $savedCardCollectionFactory;
    /**
     * @var PaymentTokenManagementInterface
     */
    public $tokenManagement;

    /**
     * @param SavedCard $resource
     * @param SavedCardInterfaceFactory $savedCardInterfaceFactory
     * @param SavedCardFactory $savedCardFactory
     * @param CollectionFactory $savedCardCollectionFactory
     * @param PaymentTokenManagementInterface $tokenManagement
     */
    public function __construct(
        SavedCard $resource,
        SavedCardInterfaceFactory $savedCardInterfaceFactory,
        SavedCardFactory $savedCardFactory,
        CollectionFactory $savedCardCollectionFactory,
        PaymentTokenManagementInterface $tokenManagement
    ) {
        $this->resource = $resource;
        $this->savedCardFactory = $savedCardFactory;
        $this->savedCardInterfaceFactory = $savedCardInterfaceFactory;
        $this->savedCardCollectionFactory = $savedCardCollectionFactory;
        $this->tokenManagement = $tokenManagement;
    }

    /**
     * Save Card data
     *
     * @param SavedCardAlias $card
     * @return SavedCardAlias
     * @throws CouldNotSaveException
     */
    public function save($card)
    {
        try {
            $this->resource->save($card);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $card;
    }

    /**
     * Load Card data by given ID
     *
     * @param string $cardId
     * @return SavedCardAlias
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getById($cardId)
    {
        $card = $this->savedCardFactory->create();

        $this->resource->load($card, $cardId);

        if (!$card->getId()) {
            throw new NoSuchEntityException(__('Card with id "%1" does not exist.', $cardId));
        }

        return $card;
    }

    /**
     * Create new instance
     *
     * @return SavedCardInterface
     * @throws LocalizedException
     */
    public function create()
    {
        return $this->savedCardInterfaceFactory->create();
    }

    /**
     * Load Card data by ID
     *
     * @param string $cardId
     * @return SavedCardInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function load($cardId)
    {
        return $this->getById($cardId);
    }

    /**
     * Load Card by Payment Method and card ID
     *
     * @param string $paymentMethod
     * @param string $cardId
     * @return bool|SavedCardInterface
     */
    public function loadCard($paymentMethod, $cardId)
    {
        /** @var Collection $collection */
        $collection = $this->savedCardCollectionFactory->create();

        $collection->addFieldToFilter('payment_method', $paymentMethod);
        $collection->addFieldToFilter('card_id', $cardId);

        $card = false;
        if ($collection->getSize()) {
            /** @var SavedCardInterface $card */
            $card = $collection->getFirstItem();
        }

        return $card;
    }

    /**
     * Load Card by Payment Method and card public hash
     *
     * @param string $paymentMethod
     * @param string $cardHash
     * @param int $customerId
     * @return bool|SavedCardInterface
     */
    public function loadCardByHash($paymentMethod, $cardHash, $customerId)
    {
        $paymentToken = $this->tokenManagement->getByPublicHash($cardHash, $customerId);

        /** @var Collection $collection */
        $collection = $this->savedCardCollectionFactory->create();

        $collection->addFieldToFilter('payment_method', $paymentMethod);
        $collection->addFieldToFilter('card_id', $paymentToken->getGatewayToken());

        $card = false;
        if ($collection->getSize()) {
            /** @var SavedCardInterface $card */
            $card = $collection->getFirstItem();
        }

        return $card;
    }

    /**
     * Get saved cards collection
     *
     * @return Collection
     */
    public function getCollection()
    {
        /** @var Collection $collection */
        $collection = $this->savedCardCollectionFactory->create();

        return $collection;
    }

    /**
     * Delete Card
     *
     * @param SavedCardInterface $card
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete($card)
    {
        try {
            $this->resource->delete($card);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete Card by given Card Identity
     *
     * @param string $cardId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($cardId)
    {
        return $this->delete($this->getById($cardId));
    }
}
