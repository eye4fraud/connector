<?php
/**
 * Copyright © 2018 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Flat sales order resource
 *
 * @author      Eye4Fraud
 */
class SavedCard extends AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eye4fraud_saved_cards', 'entity_id');
    }
}
