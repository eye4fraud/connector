<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */

namespace Eye4Fraud\Connector\Model\ResourceModel\Request;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Cached requests collection
 */
class Collection extends AbstractCollection
{
    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'eye4fraud_request_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'eye4fraud_request_collection';

    /**
     * Collection main table
     *
     * @var string
     */
    protected $_mainTable = 'eye4fraud_requests_cache';

    /**
     * Initialize models
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Eye4Fraud\Connector\Model\Request',
            'Eye4Fraud\Connector\Model\ResourceModel\Request'
        );
    }
}
