<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Flat sales order resource
 *
 * @author      Eye4Fraud
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Status extends AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eye4fraud_status', 'entity_id');
    }
}
