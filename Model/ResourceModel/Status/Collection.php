<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */

namespace Eye4Fraud\Connector\Model\ResourceModel\Status;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * Collection for displaying grid of sales documents
 */
class Collection extends AbstractCollection
{
    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'eye4fraud_status_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'eye4fraud_status_collection';

    /**
     * Collection main table
     *
     * @var string
     */
    protected $_mainTable = 'eye4fraud_status';

    /**
     * Initialize models
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Eye4Fraud\Connector\Model\Status',
            'Eye4Fraud\Connector\Model\ResourceModel\Status'
        );
    }

    /**
     * Add raw condition
     *
     * @param string $condition
     */
    public function addRawCondition($condition)
    {
        $this->getSelect()->where($condition);
    }
}
