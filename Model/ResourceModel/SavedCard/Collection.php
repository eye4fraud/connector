<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */

namespace Eye4Fraud\Connector\Model\ResourceModel\SavedCard;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Cached requests collection
 */
class Collection extends AbstractCollection
{
    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'e4f_saved_card_collection';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'collection';

    /**
     * Initialize models
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Eye4Fraud\Connector\Model\SavedCard',
            'Eye4Fraud\Connector\Model\ResourceModel\SavedCard'
        );
    }
}
