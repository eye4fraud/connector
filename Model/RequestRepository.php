<?php

namespace Eye4Fraud\Connector\Model;

use Eye4Fraud\Connector\Api\RequestRepositoryInterface;
use Eye4Fraud\Connector\Model\ResourceModel\Request\CollectionFactory as RequestCollectionFactory;
use Eye4Fraud\Connector\Model\ResourceModel\Request\Collection as RequestCollection;
use Eye4Fraud\Connector\Model\ResourceModel\Request as RequestResource;
use Magento\Framework\Exception\AlreadyExistsException;

class RequestRepository implements RequestRepositoryInterface
{
    /**
     * @var RequestCollectionFactory
     */
    public $requestCollectionFactory;

    /**
     * @var RequestResource
     */
    public $requestResource;

    /**
     * @var RequestFactory
     */
    public $requestFactory;

    /**
     * @param RequestCollectionFactory $requestCollectionFactory
     * @param RequestResource $requestResource
     * @param RequestFactory $requestFactory
     */
    public function __construct(
        RequestCollectionFactory $requestCollectionFactory,
        RequestResource $requestResource,
        RequestFactory $requestFactory
    ) {
        $this->requestCollectionFactory = $requestCollectionFactory;
        $this->requestResource = $requestResource;
        $this->requestFactory = $requestFactory;
    }

    /**
     * Save request
     *
     * @param Request $cachedRequest
     * @return RequestRepositoryInterface
     * @throws AlreadyExistsException
     */
    public function save(Request $cachedRequest): RequestRepositoryInterface
    {
        $this->requestResource->save($cachedRequest);
        return $this;
    }

    /**
     * Get request
     *
     * @param string $cachedRequestId
     * @return Request
     */
    public function get(string $cachedRequestId): Request
    {
        $cachedRequest = $this->requestFactory->create();
        $this->requestResource->load($cachedRequest, $cachedRequestId);
        return $cachedRequest;
    }

    /**
     * Get collection
     *
     * @return RequestCollection
     */
    public function getCollection(): RequestCollection
    {
        return $this->requestCollectionFactory->create();
    }

    /**
     * Get not active request for order
     *
     * @param string $orderIncrementId
     * @return RequestCollection
     */
    public function getNotActiveRequests(string $orderIncrementId): RequestCollection
    {
        $collection = $this->getCollection();
        $collection->addFieldToFilter('increment_id', $orderIncrementId);
        $collection->addFieldToFilter('is_ready', 0);
        return $collection;
    }

    /**
     * Get all not active requests since
     *
     * @param string $since
     * @return RequestCollection
     */
    public function getOldNotActiveRequests(string $since): RequestCollection
    {
        $collection = $this->getCollection();
        $collection->addFieldToFilter('created_at', ['lt' => $since]);
        $collection->addFieldToFilter('is_ready', 0);
        return $collection;
    }

    /**
     * Create empty object
     *
     * @return Request
     */
    public function create(): Request
    {
        return $this->requestFactory->create();
    }

    /**
     * Delete cached request
     *
     * @param Request $request
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $this->requestResource->delete($request);
    }

    /**
     * Delete cached request by ID
     *
     * @param int $requestId
     * @throws \Exception
     */
    public function deleteById(int $requestId)
    {
        $cachedRequest = $this->get($requestId);
        $this->delete($cachedRequest);
    }
}
