<?php

namespace Eye4Fraud\Connector\Model\Compress;

use Eye4Fraud\Connector\Api\CompressInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File;

class RawCompressor implements CompressInterface
{
    use Common;

    /**
     * @var DirectoryList
     */
    private DirectoryList $directoryList;

    /**
     * @var File
     */
    private File $filesystem;

    /**
     * Target Extensions
     *
     * @var string
     */
    private $targetExt = '.log';

    /**
     * @param DirectoryList $directoryList
     * @param File $filesystem
     */
    public function __construct(
        DirectoryList $directoryList,
        File $filesystem
    ) {
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
    }

    /**
     * Compress File
     *
     * @param string $sourceFile
     * @param string $destinationFile
     * @return string|
     * @throws FileSystemException
     */
    public function compressFile($sourceFile, $destinationFile)
    {
        $targetFile = $this->getFileFullPath($destinationFile);
        if ($this->filesystem->isExists($targetFile)) {
            return false;
        }

        $this->filesystem->copy($sourceFile, $targetFile);

        return $destinationFile . $this->targetExt;
    }
}
