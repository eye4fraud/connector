<?php

namespace Eye4Fraud\Connector\Model\Compress;

use Eye4Fraud\Connector\Api\CompressInterface;
use Magento\Framework\ObjectManagerInterface;

class Factory
{
    /**
     * Object Manager instance
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * Factory constructor
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create class instance with specified parameters
     *
     * @return CompressInterface
     */
    public function create()
    {
        if (class_exists('ZipArchive')) {
            return $this->objectManager->create(ZipArchiveCompressor::class);
        }

        return $this->objectManager->create(RawCompressor::class);
    }
}
