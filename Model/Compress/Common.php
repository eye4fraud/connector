<?php

namespace Eye4Fraud\Connector\Model\Compress;

use Magento\Framework\Exception\FileSystemException;

trait Common
{

    /**
     * Check if file exists
     *
     * @param string $destinationFile
     * @return bool
     * @throws FileSystemException
     */
    public function checkExists($destinationFile)
    {
        $file = $this->getFileFullPath($destinationFile);
        return $this->filesystem->isExists($file);
    }

    /**
     * Get full path
     *
     * @param string $destinationFile
     * @return string
     * @throws FileSystemException
     */
    public function getFileFullPath($destinationFile)
    {
        $logDir = $this->directoryList->getPath('log');
        return $logDir . DS . $destinationFile . $this->targetExt;
    }
}
