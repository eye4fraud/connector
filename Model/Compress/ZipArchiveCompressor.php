<?php

namespace Eye4Fraud\Connector\Model\Compress;

use Eye4Fraud\Connector\Api\CompressInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Compress log file
 */
class ZipArchiveCompressor implements CompressInterface
{
    use Common;

    /**
     * @var DirectoryList
     */
    private DirectoryList $directoryList;

    /**
     * @var File
     */
    private File $filesystem;

    /**
     * Target Extensions
     *
     * @var string
     */
    private $targetExt = '.zip';

    /**
     * @param DirectoryList $directoryList
     * @param File $filesystem
     */
    public function __construct(
        DirectoryList $directoryList,
        File $filesystem
    ) {
        $this->directoryList = $directoryList;
        $this->filesystem = $filesystem;
    }

    /**
     * Compress File
     *
     * @param string $sourceFile
     * @param string $destinationFile
     * @return string|bool
     * @throws FileSystemException
     */
    public function compressFile($sourceFile, $destinationFile)
    {
        $logDir = $this->directoryList->getPath('log');

        $targetFile = $this->getFileFullPath($destinationFile);
        if ($this->filesystem->isExists($targetFile)) {
            return false;
        }

        $zip = new \ZipArchive();

        # create a temp file & open it
        $zip->open($targetFile, \ZipArchive::CREATE);

        $zip->addFile($sourceFile, basename($sourceFile));
        $zip->setCompressionIndex(0, \ZipArchive::CM_BZIP2);
        $zip->close();

        return $destinationFile . $this->targetExt;
    }
}
