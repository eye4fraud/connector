<?php

namespace Eye4Fraud\Connector\Model\Request;

class StatusRequestData
{
    /**
     * @var string
     */
    public $SiteName;
    /**
     * @var string
     */
    public $ApiLogin;
    /**
     * @var string
     */
    public $ApiKey;
    /**
     * @var string
     */
    public $OrderNumber;
    /**
     * @var string
     */
    public $Action = 'getOrderStatus';
}
