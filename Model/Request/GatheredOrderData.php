<?php
namespace Eye4Fraud\Connector\Model\Request;

/**
 * Class to prepare a request data
 */
class GatheredOrderData
{
    /**
     * @var string
     */
    public $SiteName;
    /**
     * @var string
     */
    public $ApiLogin;
    /**
     * @var string
     */
    public $ApiKey;

    /**
     * @var string
     */
    public $BillingFirstName;
    /**
     * @var string
     */
    public $BillingMiddleName;
    /**
     * @var string
     */
    public $BillingLastName;
    /**
     * @var string
     */
    public $BillingCompany;
    /**
     * @var string
     */
    public $BillingAddress1;
    /**
     * @var string
     */
    public $BillingAddress2;
    /**
     * @var string
     */
    public $BillingCity;
    /**
     * @var string
     */
    public $BillingState;
    /**
     * @var string
     */
    public $BillingZip;
    /**
     * @var string
     */
    public $BillingCountry;
    /**
     * @var string
     */
    public $BillingEveningPhone;
    /**
     * @var string
     */
    public $BillingEmail;

    /**
     * @var string
     */
    public $ShippingFirstName;
    /**
     * @var string
     */
    public $ShippingMiddleName;
    /**
     * @var string
     */
    public $ShippingLastName;
    /**
     * @var string
     */
    public $ShippingCompany;
    /**
     * @var string
     */
    public $ShippingAddress1;
    /**
     * @var string
     */
    public $ShippingAddress2;
    /**
     * @var string
     */
    public $ShippingCity;
    /**
     * @var string
     */
    public $ShippingState;
    /**
     * @var string
     */
    public $ShippingZip;
    /**
     * @var string
     */
    public $ShippingCountry;
    /**
     * @var string
     */
    public $ShippingEveningPhone;
    /**
     * @var string
     */
    public $ShippingEmail;

    /**
     * @var string
     */
    public $ShippingCost;
    /**
     * @var string
     */
    public $GrandTotal;
    /**
     * @var string
     */
    public $OrderDate;
    /**
     * @var string
     */
    public $OrderNumber;
    /**
     * @var string
     */
    public $IPAddress;

    /**
     * @var string
     */
    public $CCType;
    /**
     * @var string
     */
    public $RawCCType;
    /**
     * @var string
     */
    public $CCFirst6;
    /**
     * @var string
     */
    public $CCLast4;
    /**
     * @var string
     */
    public $CIDResponse;
    /**
     * @var string
     */
    public $AVSCode;
    /**
     * @var string
     */
    public $LineItems;

    /**
     * @var string
     */
    public $TransactionId;

    /**
     * @var string
     */
    public $ShippingMethod;
    /**
     * @var string
     */
    public $RawShippingMethod;

    /**
     * @var string
     */
    public $PayPalPayerID;
    /**
     * @var string
     */
    public $PayPalPayerStatus;
    /**
     * @var string
     */
    public $PayPalAddressStatus;

    /**
     * @var string
     */
    public $CustomerComments;

    /**
     * Fill info object with data
     *
     * @param array $data
     */
    public function fill($data)
    {
        foreach ($data as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }
    }
}
