<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Payment\Model\Method\Adapter;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;

class BrainTreePayPal implements PaymentInterface
{
    public const PAYER_ID = 'payerId';
    public const PAYER_STATUS = 'payerStatus';

    /** @var RequestInterface */
    protected $request;
    /** @var Info */
    protected $paymentInfo;
    /**
     * Format avsPostalCodeResponseCode + avsStreetAddressResponseCode = avs Code
     * @var array
     */
    protected $codes = [
        'MM' => 'Y', // Street address and 5-digit postal code match
        'NM' => 'Z',
        'UM' => 'Z',
        'IM' => 'Z',
        'AM' => 'Z', //Street address does not match, but 5-digit postal code matches.
        'MN' => 'A',
        'MU' => 'A',
        'MI' => 'A',
        'MA' => 'A', //Street address matches, but 5-digit and 9-digit postal code do not match.
        'AA' => 'G', //Non-U.S. issuing bank does not support AVS.
    ];

    /**
     * @param RequestInterface $request
     * @param Info $paymentInfo
     */
    public function __construct(
        RequestInterface $request,
        Info $paymentInfo
    ) {
        $this->request = $request;
        $this->paymentInfo = $paymentInfo;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        /** @var Adapter $method */
        $info->AVSCode = '';
        $info->CIDResponse = '';
        $info->CCLast4 = '';
        $info->CCFirst6 = '';
        $info->PayPalPayerID = $paymentInstance->getAdditionalInformation(self::PAYER_ID);
        $info->PayPalPayerStatus = strtolower($paymentInstance->getAdditionalInformation(self::PAYER_STATUS));

        $info->CCType = 'PAYPAL';
        $info->RawCCType = 'BraintreePaypal';
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        return false;
    }
}
