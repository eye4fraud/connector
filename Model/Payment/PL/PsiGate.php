<?php
namespace Eye4Fraud\Connector\Model\Payment\PL;

use Eye4Fraud\Connector\Helper\Traits\AssignCardType;
use Eye4Fraud\Connector\Model\Payment\UpdateInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

class PsiGate implements UpdateInterface
{
    use AssignCardType;

    /** @var Json */
    public $jsonHelper;

    /**
     * @param Json $jsonHelper
     */
    public function __construct(
        Json $jsonHelper
    ) {
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * Update data using data from payment
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     * @return bool
     */
    public function updateFromPayment(Payment $paymentInstance, GatheredOrderData $info): bool
    {
        $additionalData = $paymentInstance->getAdditionalInformation('payment_additional_info');
        if (!$additionalData) {
            return false;
        }
        try {
            $additionalData = $this->jsonHelper->unserialize($additionalData);
        } catch (\Exception $e) {
            return false;
        }

        $info->AVSCode = $additionalData['AVSResult'] ?? "";
        $info->CIDResponse = $additionalData['CardIDResult'] ?? "";
        $info->CCLast4 = substr($additionalData['CardNumber'] ?? "", -4, 4);
        $info->RawCCType = $additionalData['CardType'] ?? "";
        $this->assignCardType($info);
        $info->TransactionId = $additionalData['TransRefNumber'] ?? "";

        return true;
    }
}
