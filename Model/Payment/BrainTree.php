<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;

class BrainTree implements PaymentInterface
{

    /** @var Info */
    public Info $paymentInfo;

    /**
     * @var array|string[]
     */
    public array $map = [
        'MM' => 'Y',
        'MN' => 'Z',
        'MU' => 'R',
        'MI' => 'U',
        'MA' => 'G',
        'MB' => 'U',

        'NM' => 'A',
        'NN' => 'N',
        'NU' => 'R',
        'NI' => 'U',
        'NA' => 'G',
        'NB' => 'U',

        'UM' => 'R',
        'UN' => 'R',
        'UU' => 'R',
        'UI' => 'R',
        'UA' => 'R',
        'UB' => 'R',

        'IM' => 'U',
        'IN' => 'U',
        'IU' => 'U',
        'II' => 'U',
        'IA' => 'U',
        'IB' => 'U',

        'AM' => 'G',
        'AN' => 'G',
        'AU' => 'G',
        'AI' => 'G',
        'AA' => 'G',
        'AB' => 'G',

        'BM' => 'U',
        'BN' => 'U',
        'BU' => 'U',
        'BI' => 'U',
        'BA' => 'U',
        'BB' => 'U'
    ];

    /**
     * @param Info $paymentInfo
     */
    public function __construct(
        Info $paymentInfo
    ) {
        $this->paymentInfo = $paymentInfo;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $postalCode = $paymentInstance->getAdditionalInformation('avsPostalCodeResponseCode');
        $addressCode = $paymentInstance->getAdditionalInformation('avsStreetAddressResponseCode');
        $avsCode = $this->map[$postalCode.$addressCode] ?? 'S';

        $info->AVSCode = $avsCode;
        $info->CIDResponse = $paymentInstance->getAdditionalInformation('cvvResponseCode');
        $info->CCLast4 = $paymentInstance->getData('cc_last_4');

        $info->RawCCType = $paymentInstance->getData('cc_type');
        //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
        $card_type = match ($info->RawCCType) {
            'VI' => 'VISA',
            'MC' => 'MC',
            'AE' => 'AMEX',
            'DI' => 'DISC',
            default => 'OTHER',
        };
        $info->CCType = $card_type;
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData): bool
    {
        $first_cc6 = $paymentData->getData('cc_first6');
        if (!$first_cc6) {
            $first_cc6 = $paymentData->getDataByPath('additional_data/cc_first6');
        }

        if ($info->CCFirst6 === $first_cc6) {
            return false;
        }
        $info->CCFirst6 = $paymentData->getDataByPath('additional_data/cc_first6');
        return true;
    }
}
