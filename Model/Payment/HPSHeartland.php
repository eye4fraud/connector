<?php
namespace Eye4Fraud\Connector\Model\Payment;

use Eye4Fraud\Connector\Helper\Traits\AssignCardType;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;
use Eye4Fraud\Connector\Model\Logger;

class HPSHeartland implements PaymentInterface
{
    use AssignCardType;

    /** @var RequestInterface */
    protected $_request;
    /** @var Info */
    protected $_paymentInfo;
    /** @var array */
    protected $_cardTypes = [
        0 => 'Visa',
        1 => 'MasterCard',
        2 => 'Discover',
        3 => 'AmericanExpress',
        4 => 'DinersClub',
        5 => 'JCB'
    ];

    /** @var Logger */
    protected $_logger;

    /**
     * @param RequestInterface $request
     * @param Logger $logger
     * @param Info $paymentInfo
     */
    public function __construct(
        RequestInterface $request,
        Logger $logger,
        Info $paymentInfo
    ) {
        $this->_request = $request;
        $this->_paymentInfo = $paymentInfo;
        $this->_logger = $logger;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        if (!$info->AVSCode) {
            $t = explode(':', $paymentInstance->getCcAvsStatus());
            $info->AVSCode = $t[0];
        }
        if (!$info->CIDResponse) {
            $t = explode(":", $paymentInstance->getCcCidStatus());
            $info->CIDResponse = $t[0];
        }
        if (!$info->CCLast4) {
            $info->CCLast4 = $paymentInstance->getData('cc_last_4');
        }

        if (!$info->RawCCType) {
            $info->RawCCType = $paymentInstance->getData('cc_type');
            $this->assignCardType($info);
        }
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $changed = false;
        $cc_first6 = $paymentData->getData('cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }
        $cc_first6 = $paymentData->getDataByPath('additional_data/cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }

        $card_type = $this->_request->getParam('CARDTYPE');
        if ($card_type!==null) {
            $changed = true;
            if (isset($this->_cardTypes[$card_type])) {
                $info->RawCCType = $this->_cardTypes[$card_type];
            } else {
                $info->RawCCType = $card_type;
            }
            switch ($info->RawCCType) {
                case 'Visa':
                    $card_type_short = 'VISA';
                    break;
                case 'MasterCard':
                    $card_type_short = 'MC';
                    break;
                case 'AmericanExpress':
                    $card_type_short = 'AMEX';
                    break;
                case 'Discover':
                    $card_type_short = 'DISC';
                    break;
                case 'DinersClub':
                case 'JCB':
                default:
                    $card_type_short = 'OTHER';
                    break;
            }
            $info->CCType = $card_type_short;
        }

        return $changed;
    }
}
