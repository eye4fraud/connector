<?php
namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;

class PayPalPayflowPro implements PaymentInterface
{
    /** @var RequestInterface */
    protected $_request;
    /** @var Info */
    protected $_paymentInfo;
    /** @var array */
    protected $_cardTypes = [
        0 => 'Visa',
        1 => 'MasterCard',
        2 => 'Discover',
        3 => 'AmericanExpress',
        4 => 'DinersClub',
        5 => 'JCB'
    ];

    /**
     * @param RequestInterface $request
     * @param Info $paymentInfo
     */
    public function __construct(
        RequestInterface $request,
        Info $paymentInfo
    ) {
        $this->_request = $request;
        $this->_paymentInfo = $paymentInfo;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->TransactionId = $paymentInstance->getTransactionId();

        if (!$info->CCLast4) {
            $info->CCLast4 = $paymentInstance->getData('cc_last_4');
        }

        if (!$info->AVSCode) {
            $info->AVSCode = $paymentInstance->getAdditionalInformation('avszip');
        }

        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getAdditionalInformation('cvv2match');
        }

        if (!$info->CCType) {
            $info->RawCCType = $paymentInstance->getData('cc_type');
            //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
            switch ($info->RawCCType) {
                case 'VI':
                    $card_type = 'VISA';
                    break;
                case 'MC':
                    $card_type = 'MC';
                    break;
                case 'AE':
                    $card_type = 'AMEX';
                    break;
                case 'DI':
                    $card_type = 'DISC';
                    break;
                default:
                    $card_type = 'OTHER';
                    break;
            }
            $info->CCType = $card_type;
        }
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $changed = false;
        $cc_first6 = $paymentData->getData('cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }
        $cc_first6 = $paymentData->getDataByPath('additional_data/cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }

        $avszip = $this->_request->getParam('PROCAVS');
        if ($avszip!==null) {
            $changed = true;
            $info->AVSCode = $avszip;
        }
        $cc_last4 = $this->_request->getParam('ACCT');
        if ($cc_last4!==null) {
            $changed = true;
            $info->CCLast4 = $cc_last4;
        }
        $cid_response = $this->_request->getParam('PROCCVV2');
        if ($cid_response!==null) {
            $changed = true;
            $info->CIDResponse = $cid_response;
        }
        $card_type = $this->_request->getParam('CARDTYPE');
        if ($card_type!==null) {
            $changed = true;
            if (isset($this->_cardTypes[$card_type])) {
                $info->RawCCType = $this->_cardTypes[$card_type];
            } else {
                $info->RawCCType = $card_type;
            }
            switch ($info->RawCCType) {
                case 'Visa':
                    $card_type_short = 'VISA';
                    break;
                case 'MasterCard':
                    $card_type_short = 'MC';
                    break;
                case 'AmericanExpress':
                    $card_type_short = 'AMEX';
                    break;
                case 'Discover':
                    $card_type_short = 'DISC';
                    break;
                case 'DinersClub':
                case 'JCB':
                default:
                    $card_type_short = 'OTHER';
                    break;
            }
            $info->CCType = $card_type_short;
        }

        return $changed;
    }
}
