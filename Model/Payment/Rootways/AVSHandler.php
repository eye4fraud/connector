<?php
namespace Eye4Fraud\Connector\Model\Payment\Rootways;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

class AVSHandler implements HandlerInterface
{
    public const AVSRESPCODE = 'avsResultCode';
    public const CVV2RESPCODE = 'cvvResultCode';
    public const RESCCNUMLAST4 = 'accountNumber';

    /**
     * Mapping
     * @var string[]
     */
    public $additionalInformationMapping = [
        'avs_response_code' => self::AVSRESPCODE,
        'cvd_response_code' => self::CVV2RESPCODE,
        'cc_numlast4' => self::RESCCNUMLAST4
    ];
    
    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = SubjectReader::readPayment($handlingSubject);
        $payment = $paymentDO->getPayment();
        
        if (!empty($response['transactionResponse']['avsResultCode'])) {
            $payment->setCcAvsStatus($response['transactionResponse']['avsResultCode']);
        }
        if (!empty($response['transactionResponse']['cvvResultCode'])) {
            $payment->setCcCidStatus($response['transactionResponse']['cvvResultCode']);
        }
        
        foreach ($this->additionalInformationMapping as $informationKey => $responseKey) {
            if (isset($response['transactionResponse'][$responseKey])) {
                $payment->setAdditionalInformation($informationKey, $response['transactionResponse'][$responseKey]);
            }
        }
    }
}
