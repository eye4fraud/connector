<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Eye4Fraud\Connector\Model\Logger;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

class CardKnox implements PaymentInterface
{
    /** @var RequestInterface */
    public $request;

    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var string[]
     */
    public $avsCodes = [
        'YYY' => 'Y', //Address: Match & 5 Digit Zip: Match
        'NYZ' => 'Z', //Address: No Match & 5 Digit Zip: Match
        'YNA' => 'A', //Address: Match & 5 Digit Zip: No Match
        'NNN' => 'N', //Address: No Match & 5 Digit Zip: No Match
        'XXU' => 'S', //Address Information not verified for domestic transaction
        'YYX' => 'X', //Address: Match & 9 Digit Zip: Match
        'NYW' => 'W', //Address: No Match & 9 Digit Zip: Match
        'XXR' => 'R', //Retry / System Unavailable
        'XXS' => 'S', //Service Not Supported
        'XXW' => 'S', //Card Number Not On File
        'XXE' => 'S', //Address Verification Not Allowed For Card Type
        'XXG' => 'S', //Global Non-AVS participant
        'YYG' => 'A', //International Address: Match & Zip: Not Compatible
        'GGG' => 'Y', //International Address: Match & Zip: Match
        'YGG' => 'Z' //International Address: Not Compatible & Zip: Match
    ];

    /**
     * @param RequestInterface $request
     * @param Logger $logger
     */
    public function __construct(
        RequestInterface $request,
        Logger $logger
    ) {
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->AVSCode = $paymentInstance->getAdditionalInformation('xAvsResultCode');
        $info->CIDResponse = $paymentInstance->getAdditionalInformation('xCvvResultCode');
        $this->logger->debug(
            'Fill payment info: Alternative avsCode '.$paymentInstance->getAdditionalInformation('xAvsResult').
            'Alternative cvvCode '.$paymentInstance->getAdditionalInformation('xCvvResult')
        );
        if (!$info->AVSCode) {
            $info->AVSCode = $paymentInstance->getAdditionalInformation('xAvsResult');
        }
        if (isset($this->avsCodes[$info->AVSCode])) {
            $info->AVSCode = $this->avsCodes[$info->AVSCode];
        }

        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getAdditionalInformation('xCvvResult');
        }
        $info->CCLast4 = substr($paymentInstance->getAdditionalInformation('xMaskedCardNumber'), -4);
        $info->CCFirst6 = substr($paymentInstance->getAdditionalInformation('xCardNum'), 0, 6);
        $info->RawCCType = $paymentInstance->getAdditionalInformation('xCardType');
        //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
        switch ($info->RawCCType) {
            case 'Visa':
                $card_type = 'VISA';
                break;
            case 'MasterCard':
                $card_type = 'MC';
                break;
            case 'AmericanExpress':
                $card_type = 'AMEX';
                break;
            case 'Discover':
                $card_type = 'DISC';
                break;
            default:
                $card_type = 'OTHER';
                break;
        }
        $info->CCType = $card_type;
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        return false;
    }
}
