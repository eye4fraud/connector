<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Framework\ObjectManagerInterface;

/**
 * Factory class for @see \Magento\Payment\Model\Checks\Composite
 */
class Factory
{
    /**
     * Object Manager instance
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;

    /** @var  array mapping */
    protected $mapping;

    /**
     * Factory constructor
     * @param ObjectManagerInterface $objectManager
     * @param array $mapping
     */
    public function __construct(ObjectManagerInterface $objectManager, $mapping)
    {
        $this->_objectManager = $objectManager;
        $this->mapping = $mapping;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param string $data
     * @return null|\Eye4Fraud\Connector\Model\Payment\PaymentInterface
     */
    public function create($data)
    {
        if (!isset($this->mapping[$data])) {
            $data = 'default';
        }
        return $this->_objectManager->create($this->mapping[$data]);
    }
}
