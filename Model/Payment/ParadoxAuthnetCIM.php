<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

use Magento\Sales\Model\Order\Payment;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

class ParadoxAuthnetCIM implements PaymentInterface
{

    /** @var RequestInterface */
    protected $request;

    /**
     * @var SavedCardRepositoryInterface
     */
    protected $savedCardRepository;

    /**
     * AuthorizeDirectPost constructor.
     * @param RequestInterface $request
     * @param SavedCardRepositoryInterface $savedCardRepository
     */
    public function __construct(
        RequestInterface $request,
        SavedCardRepositoryInterface $savedCardRepository
    ) {
        $this->request = $request;
        $this->savedCardRepository = $savedCardRepository;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->AVSCode = $paymentInstance->getCcAvsStatus()
            ?? $paymentInstance->getData('additional_information/avs_result_code');
        $info->CIDResponse = $paymentInstance->getCcCidStatus()
            ?? $paymentInstance->getData('additional_information/card_code_response_code');
        $info->CCLast4 = $paymentInstance->getCcLast4();
        $info->CCFirst6 = $info->CCFirst6
            ?? $paymentInstance->getData('additional_information/cc_first6');
        $info->RawCCType = $paymentInstance->getData('additional_information/card_type');
        $info->CCType = strtoupper($paymentInstance->getData('additional_information/card_type'))
            ?? $paymentInstance->getCcType();
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $cc_first6 = $paymentData->getData('cc_first6')
            ?? $paymentData->getDataByPath('additional_data/cc_first6')
            ?? $paymentData->getDataByPath('additional_information/cc_first6');
        $cc_last4 = $paymentData->getData('cc_last4')
            ?? $paymentData->getDataByPath('additional_data/cc_last4')
            ?? $paymentData->getDataByPath('additional_information/cc_last4');
        $card_id = $paymentData->getData('card_id')
            ?? $paymentData->getDataByPath('additional_data/card_id')
            ?? $paymentData->getDataByPath('additional_information/card_id');

        if (!$cc_last4 && $card_id) {
            $savedCard = $this->savedCardRepository->loadCard($paymentData->getData('method'), $card_id);
            if ($savedCard) {
                $cached_data = $savedCard->getCachedData();
                $info->fill($cached_data);
                return true;
            }
        }

        if ($info->CCFirst6 == $cc_first6) {
            return false;
        }
        $info->CCFirst6 = $cc_first6;
        $info->CCLast4 = $cc_last4;
        return true;
    }
}
