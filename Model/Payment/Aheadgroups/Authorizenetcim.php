<?php

namespace Eye4Fraud\Connector\Model\Payment\Aheadgroups;

use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\Payment\PaymentInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

use Magento\Sales\Model\Order\Payment;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

class Authorizenetcim implements PaymentInterface
{

    public const CODE = "aheadgroups_authorizenetcim";

    /** @var RequestInterface */
    public $request;

    /**
     * @var SavedCardRepositoryInterface
     */
    public $savedCardRepository;

    /**
     * @param RequestInterface $request
     * @param SavedCardRepositoryInterface $savedCardRepository
     */
    public function __construct(
        RequestInterface $request,
        SavedCardRepositoryInterface $savedCardRepository
    ) {
        $this->request = $request;
        $this->savedCardRepository = $savedCardRepository;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->AVSCode = $paymentInstance->getCcAvsStatus();
        if (!$info->AVSCode) {
            $info->AVSCode = $paymentInstance->getData('additional_information/avs_result_code');
        }
        if (method_exists($paymentInstance->getExtensionAttributes(), 'getVaultPaymentToken')) {
            $vaultToken = $paymentInstance->getExtensionAttributes()->getVaultPaymentToken();
            if ($vaultToken && $vaultToken->getIsActive() && $vaultToken->getType() === 'card') {
                $savedCard = $this->savedCardRepository->loadCard(self::CODE, $vaultToken->getGatewayToken());
                if (!$savedCard) {
                    $savedCard = $this->savedCardRepository->create();
                    $savedCard->setCardId($vaultToken->getGatewayToken());
                    $savedCard->setPaymentMethod(self::CODE);
                }
                $cachedData = [
                    'CCFirst6' => $info->CCFirst6,
                    'CCLast4' =>  $info->CCLast4
                ];
                $savedCard->setCachedData($cachedData);
                $this->savedCardRepository->save($savedCard);
            }
        };

        $info->CIDResponse = $paymentInstance->getCcCidStatus();
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getData('additional_information/card_code_response_code');
        }
        $info->RawCCType = $paymentInstance->getCcType();
        $info->CCType = $paymentInstance->getCcType();
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $cc_first6 = $paymentData->getDataByPath('additional_data/cc_first6');
        $cc_last4 = $paymentData->getDataByPath('additional_data/cc_last4');

        $card_id = $paymentData->getDataByPath('additional_data/saved_vaulet_card_results');
        if ($card_id) {
            $card_id = substr($card_id, 0, strrpos($card_id, '-'));
        }

        if (!$cc_last4 && $card_id) {
            $savedCard = $this->savedCardRepository->loadCard($paymentData->getData('method'), $card_id);
            if ($savedCard) {
                $cached_data = $savedCard->getCachedData();
                $info->fill($cached_data);
                return true;
            }
        }

        if ($info->CCFirst6 == $cc_first6) {
            return false;
        }
        $info->CCFirst6 = $cc_first6;
        $info->CCLast4 = $cc_last4;
        return true;
    }
}
