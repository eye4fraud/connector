<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;

class PayPalPayFlowAdvanced implements PaymentInterface
{
    /** @var RequestInterface */
    protected $_request;
    /** @var Info */
    protected $_paymentInfo;
    /** @var array */
    protected $_cardTypes = [
        0 => 'Visa',
        1 => 'MasterCard',
        2 => 'Discover',
        3 => 'AmericanExpress',
        4 => 'DinersClub',
        5 => 'JCB'
    ];

    /**
     * AuthorizeDirectPost constructor.
     * @param RequestInterface $request
     * @param Info $paymentInfo
     */
    public function __construct(
        RequestInterface $request,
        Info $paymentInfo
    ) {
        $this->_request = $request;
        $this->_paymentInfo = $paymentInfo;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $changed = false;
        $cc_first6 = $paymentData->getData('cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }
        $avszip = $this->_request->getParam('PROCAVS');
        if ($avszip!==null) {
            $changed = true;
            $info->AVSCode = $avszip;
        }
        $cc_last4 = $this->_request->getParam('ACCT');
        if ($cc_last4!==null) {
            $changed = true;
            $info->CCLast4 = $cc_last4;
        }
        $cid_response = $this->_request->getParam('PROCCVV2');
        if ($cid_response!==null) {
            $changed = true;
            $info->CIDResponse = $cid_response;
        }
        $card_type = $this->_request->getParam('CARDTYPE');
        if ($card_type!==null) {
            $changed = true;
            if (isset($this->_cardTypes[$card_type])) {
                $info->RawCCType = $this->_cardTypes[$card_type];
            } else {
                $info->RawCCType = $card_type;
            }
            switch ($info->RawCCType) {
                case 'Visa':
                    $card_type_short = 'VISA';
                    break;
                case 'MasterCard':
                    $card_type_short = 'MC';
                    break;
                case 'AmericanExpress':
                    $card_type_short = 'AMEX';
                    break;
                case 'Discover':
                    $card_type_short = 'DISC';
                    break;
                case 'DinersClub':
                    $card_type_short = 'OTHER';
                    break;
                case 'JCB':
                    $card_type_short = 'OTHER';
                    break;
                default:
                    $card_type_short = 'OTHER';
                    break;
            }
            $info->CCType = $card_type_short;
        }

        return $changed;
    }
}
