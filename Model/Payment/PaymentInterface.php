<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\DataObject;

interface PaymentInterface
{
    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     * @return mixed
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info);

    /**
     * Fill first digits
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return mixed
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData);
}
