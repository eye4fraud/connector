<?php
namespace Eye4Fraud\Connector\Model\Payment;

use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Helper\Traits\AssignCardType;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;
use Eye4Fraud\Connector\Model\Logger;

class DefaultPayment implements PaymentInterface
{
    use AssignCardType;

    /**
     * @var bool
     */
    public $logPaymentDataEnabled = true;
    /**
     * @var bool
     */
    public $logPaymentImportDataEnabled = true;

    /** @var RequestInterface */
    protected $_request;
    /** @var Info */
    protected $_paymentInfo;
    /** @var array */
    protected $_cardTypes = [
        0 => 'Visa',
        1 => 'MasterCard',
        2 => 'Discover',
        3 => 'AmericanExpress',
        4 => 'DinersClub',
        5 => 'JCB'
    ];

    /** @var Logger */
    protected $_logger;
    /**
     * @var SavedCardRepositoryInterface
     */
    public $savedCardRepository;

    /**
     * @param RequestInterface $request
     * @param Logger $logger
     * @param Info $paymentInfo
     * @param SavedCardRepositoryInterface $savedCardRepository
     */
    public function __construct(
        RequestInterface $request,
        Logger $logger,
        Info $paymentInfo,
        SavedCardRepositoryInterface $savedCardRepository
    ) {
        $this->_request = $request;
        $this->_paymentInfo = $paymentInfo;
        $this->_logger = $logger;
        $this->savedCardRepository = $savedCardRepository;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        if (!$info->AVSCode) {
            $postalCode = $paymentInstance->getCcAvsStatus();
            $info->AVSCode = $postalCode;
        }
        if (!$info->AVSCode) {
            $postalCode = $paymentInstance->getAdditionalInformation('avsPostalCodeResponseCode');
            $info->AVSCode = $postalCode;
        }
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getCcCidStatus();
        }
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getAdditionalInformation('cvvResponseCode');
        }
        if (!$info->CCLast4) {
            $info->CCLast4 = $paymentInstance->getData('cc_last_4');
        }

        if (!$info->RawCCType) {
            $info->RawCCType = $paymentInstance->getData('cc_type');
            $this->assignCardType($info);
        }
        $info->TransactionId = $paymentInstance->getTransactionId();

        $this->logPaymentData($paymentInstance);
    }

    /**
     * Log payment info to log file
     *
     * @param Payment $paymentInstance
     * @return void
     */
    public function logPaymentData($paymentInstance)
    {
        if (!$this->logPaymentDataEnabled) {
            return;
        }

        $this->_logger->debug('Payment place after data available:');
        $additional_params = $paymentInstance->getAdditionalInformation();
        if (is_array($additional_params)) {
            $additional_params = array_filter($additional_params, function ($value) {
                return !empty($value);
            });
            $this->_logger->debug(print_r(array_keys($additional_params), true));
        }
        $payment_params = $paymentInstance->getData();
        if (is_array($payment_params)) {
            $payment_params = array_filter($payment_params, function ($value) {
                return !empty($value);
            });
            $this->_logger->debug(print_r(array_keys($payment_params), true));
        }
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $changed = false;
        $cc_first6 = $paymentData->getData('cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }
        $cc_first6 = $paymentData->getDataByPath('additional_data/cc_first6');
        if ($cc_first6!==null) {
            $changed = true;
            $info->CCFirst6 = $cc_first6;
        }

        $card_type = $this->_request->getParam('CARDTYPE');
        if ($card_type!==null) {
            $changed = true;
            if (isset($this->_cardTypes[$card_type])) {
                $info->RawCCType = $this->_cardTypes[$card_type];
            } else {
                $info->RawCCType = $card_type;
            }
            $this->assignCardType($info);
        }

        $this->logPaymentImportData($paymentData);

        return $changed;
    }

    /**
     * Log payment data on payment import
     *
     * @param DataObject $paymentData
     * @return void
     */
    public function logPaymentImportData($paymentData)
    {
        if (!$this->logPaymentImportDataEnabled) {
            return;
        }

        $this->_logger->debug('Payment import data available:');
        $all_params = $this->_request->getParams();
        if (is_array($all_params) && count($all_params)) {
            $all_params = array_filter($all_params, function ($value) {
                return !empty($value);
            });
            if (count($all_params)) {
                $this->_logger->debug('Data available in request:');
                $this->_logger->debug(print_r(array_keys($all_params), true));
            }
        }
        $all_params = $paymentData->getData();
        if (is_array($all_params) && count($all_params)) {
            $all_params = array_filter($all_params, function ($value) {
                return !empty($value);
            });
            if (count($all_params)) {
                $this->_logger->debug('Data available in input:');
                $this->_logger->debug(print_r(array_keys($all_params), true));
            }
        }
        $all_params = $paymentData->getData('additional_data');
        if (is_array($all_params) && count($all_params)) {
            $all_params = array_filter($all_params, function ($value) {
                return !empty($value);
            });
            if (count($all_params)) {
                $this->_logger->debug('Additional data available in input:');
                $this->_logger->debug(print_r(array_keys($all_params), true));
            }
        }
    }

    /**
     * Add saved card info to info block
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     * @return void
     */
    public function addSavedCardInfo($paymentInstance, $info)
    {
        $hash = $paymentInstance->getAdditionalInformation('public_hash');
        $customerId = (int)$paymentInstance->getOrder()->getCustomerId();
        $savedCard = $this->savedCardRepository->loadCardByHash(
            $paymentInstance->getMethod(),
            $hash,
            $customerId
        );
        if ($savedCard) {
            $cached_data = $savedCard->getCachedData();
            $info->fill($cached_data);
        }
    }
}
