<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Info;
use Magento\Framework\DataObject;
use Magento\Paypal\Model\Info as PayPalInfo;

class PayPalExpress implements PaymentInterface
{
    public const PAYER_ID = "paypal_payer_id";
    /** @var RequestInterface */
    protected $_request;
    /** @var Info */
    protected $_paymentInfo;
    /** @var array */
    protected $_cardTypes = [
        0 => 'Visa',
        1 => 'MasterCard',
        2 => 'Discover',
        3 => 'AmericanExpress',
        4 => 'DinersClub',
        5 => 'JCB'
    ];

    /** @var ScopeConfigInterface */
    protected $_scopeConfig;

    /**
     * AuthorizeDirectPost constructor.
     * @param RequestInterface $request
     * @param Info $paymentInfo
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        RequestInterface $request,
        Info $paymentInfo,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_request = $request;
        $this->_paymentInfo = $paymentInfo;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->CCFirst6 = '';
        $info->CCLast4 = '';
        // Delay request, it will be unblocked later by IPN request
        if ($paymentInstance->getOrder()->getState()===Order::STATE_PENDING_PAYMENT) {
            $info->OrderNumber = null;
        }

        //$info->PayPalPayerID = $paymentInstance->getAdditionalInformation(self::PAYER_ID);
        $info->PayPalPayerID = $paymentInstance->getAdditionalInformation(PayPalInfo::PAYPAL_PAYER_ID);
        $info->PayPalAddressStatus = $paymentInstance->getAdditionalInformation(PayPalInfo::PAYPAL_ADDRESS_STATUS);
        $info->PayPalPayerStatus = $paymentInstance->getAdditionalInformation(PayPalInfo::PAYPAL_PAYER_STATUS);
        $info->RawCCType = 'PayPalExpress';
        $info->CCType = 'PayPal';
    }

    /**
     * Process IPN response
     *
     * @param RequestInterface $request
     * @param GatheredOrderData $info
     * @return void
     */
    public function processIpnResponse(RequestInterface $request, GatheredOrderData $info)
    {
        $paymentStatus = $this->filterPaymentStatus($request->getParam('payment_status'));
        if ($paymentStatus == PayPalInfo::PAYMENTSTATUS_COMPLETED) {
            if ($request->getParam('transaction_entity') == 'auth') {
                return;
            }
        }
        if ($paymentStatus == PayPalInfo::PAYMENTSTATUS_PENDING) {
            if ($request->getParam('pending_reason') != 'authorization') {
                return;
            }
        }

        $info->PayPalPayerID = $request->getParam('payer_id');
        $info->PayPalAddressStatus = $request->getParam('address_status');
        $info->PayPalPayerStatus = $request->getParam('payer_status');
        $info->OrderNumber = $request->getParam('invoice');
        $info->TransactionId = $request->getParam('txn_id');
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        return false;
    }

    /**
     * Filter payment status from NVP into paypal/info format
     *
     * @param string $ipnPaymentStatus
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function filterPaymentStatus($ipnPaymentStatus)
    {
        switch ($ipnPaymentStatus) {
            case 'Created':
                // break is intentionally omitted
            case 'Completed':
                return PayPalInfo::PAYMENTSTATUS_COMPLETED;
            case 'Denied':
                return PayPalInfo::PAYMENTSTATUS_DENIED;
            case 'Expired':
                return PayPalInfo::PAYMENTSTATUS_EXPIRED;
            case 'Failed':
                return PayPalInfo::PAYMENTSTATUS_FAILED;
            case 'Pending':
                return PayPalInfo::PAYMENTSTATUS_PENDING;
            case 'Refunded':
                return PayPalInfo::PAYMENTSTATUS_REFUNDED;
            case 'Reversed':
                return PayPalInfo::PAYMENTSTATUS_REVERSED;
            case 'Canceled_Reversal':
                return PayPalInfo::PAYMENTSTATUS_UNREVERSED;
            case 'Processed':
                return PayPalInfo::PAYMENTSTATUS_PROCESSED;
            case 'Voided':
                return PayPalInfo::PAYMENTSTATUS_VOIDED;
            default:
                return '';
        }
        // documented in NVP, but not documented in IPN:
        //PayPalInfo::PAYMENTSTATUS_NONE
        //PayPalInfo::PAYMENTSTATUS_INPROGRESS
        //PayPalInfo::PAYMENTSTATUS_REFUNDEDPART
    }
}
