<?php
namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

class PMCAuthnetCIM implements PaymentInterface
{
    /** @var RequestInterface */
    protected $request;

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        $info->AVSCode = $paymentInstance->getCcAvsStatus();
        $info->CIDResponse = $this->request->getParam('x_cvv2_resp_code');
        $info->CCLast4 = $paymentInstance->decrypt($paymentInstance->getCcLast4());
        $info->RawCCType = $this->request->getParam('x_card_type');
        //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
        switch ($info->RawCCType) {
            case 'Visa':
                $card_type = 'VISA';
                break;
            case 'MasterCard':
                $card_type = 'MC';
                break;
            case 'AmericanExpress':
                $card_type = 'AMEX';
                break;
            case 'Discover':
                $card_type = 'DISC';
                break;
            default:
                $card_type = 'OTHER';
                break;
        }
        $info->CCType = $card_type;
        $info->TransactionId = $paymentInstance->getTransactionId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $cc_first6 = $paymentData->getData('cc_first6');
        if (!$cc_first6) {
            $cc_first6 = $paymentData->getDataByPath('additional_data/cc_first6');
        }

        if ($info->CCFirst6 == $cc_first6) {
            return false;
        }
        $info->CCFirst6 = $cc_first6;
        return true;
    }
}
