<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

interface UpdateInterface
{
    /**
     * Update data from Magento payment
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     * @return bool
     */
    public function updateFromPayment(Payment $paymentInstance, GatheredOrderData $info): bool;
}
