<?php

namespace Eye4Fraud\Connector\Model\Payment;

use Eye4Fraud\Connector\Model\Logger;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;

class ZsUsaepay implements PaymentInterface
{
    /** @var RequestInterface */
    public $request;

    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var string[]
     */
    public $avsCodes = [
        '' => 'E', // AVS Data is invalid
        // Domestic Response Codes
        'YYY' => 'Y', // Address and 5-digit Zip Code match
        'YYA' => 'Y',
        'YYD' => 'Y',
        'Y' => 'Y',
        'NYZ' => 'Z', // 5-digit Zip Code matches only
        'Z' => 'Z',
        'YNA' => 'A', // Address matches only
        'YNY' => 'A',
        'A' => 'A',
        'NNN' => 'N', // Neither Address nor Zip Code match<br />
        'NN' => 'N',
        'N' => 'N',
        'YYX' => 'X', // Address and 9-digit Zip Code match
        'X' => 'X',
        'NYW' => 'W', // 9-digit Zip Code matches only
        'W' => 'W',
        'XXW' => '?', // Card Number Not On File
        'XXU' => 'U', // Address info not verified for domestic transaction
        'XXR' => 'R', //Retry / System Unavailable
        'R' => 'R',
        'U' => 'R',
        'E' => 'R',
        'XXS' => 'S', // Service not supported
        'S' => 'S',
        'XXE' => 'E', // Address verification not allowed for card type
        'XXG' => 'G', // Global non-AVS participant
        'G' => 'G',
        'C' => 'G',
        'I' => 'G',
        // International Response Codes
        'YYG' => 'B', // Address: Match & Zip: Not Compatible
        'B' => 'B',
        'M' => 'B',
        'GGG' => 'D', // International Address: Match & Zip: Match<br />
        'YYF' => 'D',
        'D' => 'D',
        'YGG' => 'P', //International Address: No Compatible & Zip: Match
        'NYP' => 'P',
        'P' => 'P',
    ];

    /**
     * AuthorizeDirectPost constructor.
     * @param RequestInterface $request
     * @param Logger $logger
     */
    public function __construct(
        RequestInterface $request,
        Logger $logger
    ) {
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        if (!$info->AVSCode) {
            $postalCode = $paymentInstance->getCcAvsStatus();
            $info->AVSCode = $postalCode;
        }
        if (isset($this->avsCodes[$info->AVSCode])) {
            $info->AVSCode = $this->avsCodes[$info->AVSCode];
        }
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getCcCidStatus();
        }

        $info->TransactionId = $paymentInstance->getCcTransId();
    }

    /**
     * Save first 6 digits of card
     *
     * @param GatheredOrderData $info
     * @param DataObject $paymentData
     * @return bool
     */
    public function fillFirstCardDigits(GatheredOrderData $info, DataObject $paymentData)
    {
        $info->CCLast4 = substr($paymentData->getAdditionalData('cc_number'), -4);
        $info->CCFirst6 = $paymentData->getAdditionalData('cc_first6');
        if (!$info->CCFirst6) {
            $info->CCFirst6 = substr($paymentData->getAdditionalData('cc_number'), 0, 6);
        }

        $info->RawCCType = $paymentData->getAdditionalData('cc_type');
        switch ($info->RawCCType) {
            case 'VI':
                $card_type = 'VISA';
                break;
            case 'MC':
                $card_type = 'MC';
                break;
            case 'AE':
                $card_type = 'AMEX';
                break;
            case 'DI':
                $card_type = 'DISC';
                break;
            default:
                $card_type = 'OTHER';
                break;
        }
        $info->CCType = $card_type;

        return true;
    }
}
