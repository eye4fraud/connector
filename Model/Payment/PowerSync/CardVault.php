<?php
namespace Eye4Fraud\Connector\Model\Payment\PowerSync;

use Eye4Fraud\Connector\Model\Payment\DefaultPayment;
use Eye4Fraud\Connector\Model\Payment\PaymentInterface;

class CardVault extends DefaultPayment implements PaymentInterface
{
    /** @var bool */
    public $logPaymentDataEnabled = false;
    /** @var bool */
    public $logPaymentImportDataEnabled = false;
}
