<?php
namespace Eye4Fraud\Connector\Model\Payment\PowerSync;

use Eye4Fraud\Connector\Model\Payment\DefaultPayment;
use Eye4Fraud\Connector\Model\Payment\PaymentInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Payment;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

class AuthorizeNet extends DefaultPayment implements PaymentInterface
{
    /** @var bool */
    public $logPaymentDataEnabled = false;
    /** @var bool */
    public $logPaymentImportDataEnabled = false;

    /**
     * Fill payment info
     *
     * @param Payment $paymentInstance
     * @param GatheredOrderData $info
     */
    public function fillPaymentInfo(Payment $paymentInstance, GatheredOrderData $info)
    {
        if (!$info->AVSCode) {
            $info->AVSCode = $paymentInstance->getCcAvsStatus();
        }
        if (!$info->AVSCode) {
            $info->AVSCode = $paymentInstance->getAdditionalInformation('avs_code');
        }
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getCcCidStatus();
        }
        if (!$info->CIDResponse) {
            $info->CIDResponse = $paymentInstance->getAdditionalInformation('cvv_code');
        }
        if (!$info->CCLast4) {
            $info->CCLast4 = $paymentInstance->getData('cc_last_4');
        }

        if (!$info->RawCCType) {
            $info->RawCCType = $paymentInstance->getData('cc_type');
            $this->assignCardType($info);
        }
        $info->TransactionId = $paymentInstance->getTransactionId();

        try {
            $methodInstance = $paymentInstance->getMethodInstance();
            if ($methodInstance->getCode() === 'tnw_authorize_cim_vault') {
                $this->addSavedCardInfo($paymentInstance, $info);
            }
        } catch (LocalizedException $e) {
            $this->_logger->error($e->getMessage());
        }
    }
}
