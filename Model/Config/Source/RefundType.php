<?php
/**
 * Refund type source model
 */
namespace Eye4Fraud\Connector\Model\Config\Source;

use Eye4Fraud\Connector\Helper\Order\Management;
use Magento\Framework\Data\OptionSourceInterface;

class RefundType implements OptionSourceInterface
{
    public const UNDEFINED_OPTION_LABEL = '-- Please Select --';

    /**
     * Get list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => '', 'label' => __('-- Please Select --')],
            ['value' => Management::REFUND_SKIP, 'label' => __("No Refund")],
            ['value' => Management::REFUND_ONLINE, 'label' => __("Refund Online")],
            ['value' => Management::REFUND_OFFLINE, 'label' => __("Refund Offline")]
        ];
        return $options;
    }
}
