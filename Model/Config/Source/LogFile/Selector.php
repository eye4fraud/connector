<?php

namespace Eye4Fraud\Connector\Model\Config\Source\LogFile;

use Eye4Fraud\Connector\Helper\Files;
use Magento\Framework\Option\ArrayInterface;
use Magento\Sales\Model\Order\Config;

class Selector implements ArrayInterface
{
    /**
     * @var Config
     */
    public $orderConfig;
    /**
     * @var Files
     */
    public $filesHelper;

    /**
     * @param Config $orderConfig
     * @param Files $filesHelper
     */
    public function __construct(
        Config $orderConfig,
        Files $filesHelper
    ) {
        $this->filesHelper = $filesHelper;
        $this->orderConfig = $orderConfig;
    }

    /**
     * Get options list
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $logFiles = $this->filesHelper->getLogFiles();
        foreach ($logFiles as $name => $file) {
            $options[] = ['value' => $name, 'label' => $name];
        }
        return $options;
    }
}
