<?php
/**
 * Order State source model
 */
namespace Eye4Fraud\Connector\Model\Config\Source\Order;

use Magento\Framework\Option\ArrayInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config;

class FraudStates implements ArrayInterface
{
    public const UNDEFINED_OPTION_LABEL = '-- Please Select --';

    /**
     * @var string[]
     */
    protected $_stateStatuses = [
        //Order::STATE_NEW,
        //Order::STATE_PROCESSING,
        //Order::STATE_COMPLETE,
        //Order::STATE_CLOSED,
        Order::STATE_CANCELED,
        Order::STATE_HOLDED,
    ];

    /**
     * @var Config
     */
    protected $_orderConfig;

    /**
     * @param Config $orderConfig
     */
    public function __construct(Config $orderConfig)
    {
        $this->_orderConfig = $orderConfig;
    }

    /**
     * Get list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [['value' => '', 'label' => __('-- Please Select --')]];
        foreach ($this->_stateStatuses as $code) {
            $options[] = ['value' => $code, 'label' => $code];
        }
        return $options;
    }
}
