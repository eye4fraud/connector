<?php
/**
 * Copyright © 2013-2020 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Model\Config\Source\Order\Status;

/**
 * Order Statuses source model
 */
class Hold extends \Magento\Sales\Model\Config\Source\Order\Status
{
    /**
     * @var string
     */
    protected $_stateStatuses = \Magento\Sales\Model\Order::STATE_HOLDED;
}
