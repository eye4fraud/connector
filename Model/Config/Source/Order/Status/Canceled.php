<?php
/**
 * Copyright © 2013-2020 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Model\Config\Source\Order\Status;

use Magento\Sales\Model\Config\Source\Order\Status;
use Magento\Sales\Model\Order;

/**
 * Order Statuses source model
 */
class Canceled extends Status
{
    /**
     * @var string
     */
    protected $_stateStatuses = Order::STATE_CANCELED;
}
