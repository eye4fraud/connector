<?php
/**
 * Payment capture methods source model
 */
namespace Eye4Fraud\Connector\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\Order\Invoice;

class PaymentCapture implements OptionSourceInterface
{
    public const UNDEFINED_OPTION_LABEL = '-- Please Select --';

    /**
     * Options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => '', 'label' => __('-- Please Select --')],
            ['value' => Invoice::NOT_CAPTURE, 'label' => __("Not Capture")],
            ['value' => Invoice::CAPTURE_ONLINE, 'label' => __("Capture Online")],
            ['value' => Invoice::CAPTURE_OFFLINE, 'label' => __("Capture Offline")]
        ];
        return $options;
    }
}
