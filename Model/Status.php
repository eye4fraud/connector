<?php
namespace Eye4Fraud\Connector\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Zend_Db_Expr;

/**
 * Fraud Status class
 * @method getStatus() Get status
 * @method setStatus(int $status) Set status
 * @method getStoreId() Get store_id
 * @method setStoreId(int $storeId) Set store_id
 * @method getOrderId() Get Order ID
 * @method getAttempts() Get attempts count
 * @method getRequestVersion() Get a request version
 * @method setRequestVersion(int $version) Set a request version
 * @method setAttempts(int $attempts) Set attempts count
 * @method setDetails(string $details)
 * @method string getDetails()
 * @method setUpdatedAt(string $newDate)
 */
class Status extends AbstractModel
{

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ef_status_model';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'fraud_status';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
        if ($resource===null) {
            $this->_init('Eye4Fraud\Connector\Model\ResourceModel\Status');
        }
    }

    /**
     * Load order by system increment identifier
     *
     * @param string $incrementId
     * @return $this
     */
    public function loadByIncrementId($incrementId)
    {
        return $this->load($incrementId, 'increment_id');
    }

    /**
     * Set the new status to the queued response
     *
     * @param Order $orderInstance
     * @return $this
     */
    public function setQueued($orderInstance)
    {
        $this->addData([
            'order_id' => $orderInstance->getId(),
            'status' => StatusInterface::STATUS_QUEUED,
            'details' => 'Queued',
            'increment_id' => $orderInstance->getIncrementId(),
            'store_id' => $orderInstance->getStoreId(),
            'updated_at',
            new Zend_Db_Expr('now()'),
            'created_at',
            new Zend_Db_Expr('now()')
        ])->isObjectNew(true);
        return $this;
    }

    /**
     * Set the waiting status after the cached request was sent
     *
     * @return $this
     */
    public function setWaiting()
    {
        $this->setData('status', StatusInterface::STATUS_AWAITING_RESPONSE);
        $this->setData('details', 'Waiting Update');
        return $this;
    }

    /**
     * Set error status with details
     *
     * @param string $details
     * @return $this
     */
    public function setError($details)
    {
        $this->setData('status', StatusInterface::STATUS_ERROR2);
        $this->setData('details', $details);
        return $this;
    }
}
