<?php
namespace Eye4Fraud\Connector\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Fraud Status class
 * @method getStatus() Get status
 * @method getOrderId() Get Order ID
 */
class SavedCard extends AbstractModel
{

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = 'e4f_saved_card_model';

    /**
     * Parameter name in event
     * In observe method you can use $observer->getEvent()->getObject() in this case
     * @var string
     */
    protected $_eventObject = 'saved_card';

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Constructor
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Eye4Fraud\Connector\Model\ResourceModel\SavedCard');
    }

    /**
     * Set cached data
     *
     * @param array $data
     * @return $this
     */
    public function setCachedData($data)
    {
        $this->setData('cached_data', json_encode($data));
        return $this;
    }

    /**
     * Get cached data
     *
     * @return array
     */
    public function getCachedData()
    {
        return json_decode($this->getData('cached_data'));
    }
}
