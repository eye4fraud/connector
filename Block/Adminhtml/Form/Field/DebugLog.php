<?php

namespace Eye4Fraud\Connector\Block\Adminhtml\Form\Field;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Escaper;

/**
 * Export Log file
 */
class DebugLog extends AbstractElement
{
    /**
     * @var UrlInterface
     */
    protected $backendUrl;

    /**
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param UrlInterface $backendUrl
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        UrlInterface $backendUrl,
        array $data = []
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        $this->backendUrl = $backendUrl;
    }

    /**
     * Get element html
     *
     * @return string
     */
    public function getElementHtml()
    {
        $url = $this->backendUrl->getUrl('eye4fraud/system_config/DownloadEye4FraudLog');
        $html = sprintf(
            '<a href="%s" target="_blank" title="%s" id="%s">%s</a>',
            $url,
            __('Download Log File'),
            $this->getHtmlId(),
            __('Download Log File')
        );
        $html .= '<script type="application/javascript">

require(["jquery"], function($){
var $selectedFile = $("#eye4fraud_connector_general_log_file_selector");
var downloadUrl = "'.$url.'";
$selectedFile.removeAttr("name");

$("#'.$this->getHtmlId().'").click(function(){
    var downloadLink = downloadUrl;
    if ($selectedFile.length) {
        var selectedFile = $selectedFile.find("option:selected").val();
        downloadLink += "file/" + selectedFile;
    }
    $(this).attr("href", downloadLink);
    return true;
})
});
</script>';
        return $html;
    }
}
