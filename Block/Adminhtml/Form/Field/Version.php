<?php

namespace Eye4Fraud\Connector\Block\Adminhtml\Form\Field;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\CollectionFactory;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\Escaper;
use Magento\Framework\Module\ModuleListInterface;

/**
 * Export Log file
 */
class Version extends AbstractElement
{
    public const MODULE_NAME = 'Eye4Fraud_Connector';

    /**
     * @var UrlInterface
     */
    protected $backendUrl;

    /** @var ModuleListInterface */
    protected $moduleList;

    /**
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param ModuleListInterface $moduleList
     * @param array $data
     */
    public function __construct(
        Factory $factoryElement,
        CollectionFactory $factoryCollection,
        Escaper $escaper,
        ModuleListInterface $moduleList,
        array $data = []
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        $this->moduleList = $moduleList;
    }

    /**
     * Return element html
     *
     * @return string
     */
    public function getElementHtml()
    {
        $version = $this->moduleList->getOne(self::MODULE_NAME)['setup_version'];
        $html = '<span>'.$version.'</span>';
        return $html;
    }
}
