<?php

namespace Eye4Fraud\Connector\Block\Adminhtml\Form\Field;

use Magento\Framework\Data\Form\Element\Select;

/**
 * Export Log file names
 */
class Selector extends Select
{
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return '';
    }
}
