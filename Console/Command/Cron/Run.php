<?php
declare(strict_types=1);

namespace Eye4Fraud\Connector\Console\Command\Cron;

use Eye4Fraud\Connector\Cron\RequestStatuses;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Run extends Command
{
    /**
     * @var RequestStatuses
     */
    private $cron;

    /**
     * @param RequestStatuses $cron
     * @param string|null $name
     */
    public function __construct(
        RequestStatuses $cron,
        string $name = null
    ) {
        $this->cron = $cron;

        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('e4f:cron:run');
        $this->setDescription('Run cron for E4F');

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cron->execute(true);
    }
}
