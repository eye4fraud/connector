<?php

namespace Eye4Fraud\Connector\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

/**
 * Update details
 * @noinspection PhpUnused
 */
class E4FStatusDetailsUpdate implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * PatchInitial constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $connection = $this->moduleDataSetup->getConnection();
        $connection->beginTransaction();
        $connection->update(
            $this->moduleDataSetup->getTable('eye4fraud_status'),
            ['details'=>'Queued'],
            'details = "status:Q"'
        );
        $connection->update(
            $this->moduleDataSetup->getTable('eye4fraud_status'),
            ['details'=>'Waiting Update'],
            'details = "status:W"'
        );
        $connection->update(
            $this->moduleDataSetup->getTable('eye4fraud_status'),
            ['details'=>'No Order'],
            'details = "status:N"'
        );
        $connection->commit();
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getVersion(): string
    {
        return '1.0.9';
    }

    /**
     * @inheritdoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
