<?php

namespace Eye4Fraud\Connector\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

/**
 * Update details
 * @noinspection PhpUnused
 */
class E4FAddStatuses implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * PatchInitial constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $setup = $this->moduleDataSetup->startSetup();
        /**
         * Install order statuses from config
         */
        $data = [];
        $statuses = [
            'e4f_hold' => __('On Hold (E4F)'),
            'e4f_review' => __('Under Review (E4F)'),
            'e4f_insured' => __('Insured (E4F)'),
            'e4f_fraud' => __('Fraud (E4F)'),
            'e4f_approved' => __('Approved (E4F)'),
            'e4f_declined' => __('Declined (E4F)'),
        ];
        foreach ($statuses as $code => $info) {
            $data[] = ['status' => $code, 'label' => $info];
        }
        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status'),
            ['status', 'label'],
            $data
        );

        /**
         * Install order states from config
         */
        $data = [];
        $states = [
            'processing' => [
                'statuses' => [
                    'e4f_hold' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_review' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_insured' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_approved' => ['is_default' => 0,'visible_on_front'=>1],
                ],
            ],
            'canceled' => [
                'statuses' => [
                    'e4f_fraud' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_declined' => ['is_default' => 0,'visible_on_front'=>1]
                ],
            ],
            'holded' => [
                'statuses' => [
                    'e4f_hold' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_review' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_fraud' => ['is_default' => 0,'visible_on_front'=>1],
                    'e4f_declined' => ['is_default' => 0,'visible_on_front'=>1]
                ],
            ],
        ];

        foreach ($states as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status => $statusInfo) {
                    $is_default = is_array($statusInfo) && isset($statusInfo['is_default']) ?
                        $statusInfo['is_default'] : 0;
                    $visibleOnFront = is_array($statusInfo) && isset($statusInfo['visible_on_front']) ?
                        $statusInfo['visible_on_front'] : 0;
                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => $is_default,
                        'visible_on_front' => $visibleOnFront,
                    ];
                }
            }
        }
        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            $data
        );
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getVersion(): string
    {
        return '1.1.0';
    }

    /**
     * @inheritdoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
