<?php
/**
 * Copyright © 2013-2021 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Store\Model\ScopeInterface;

/**
 * Sales module base helper
 *
 */
class Files extends AbstractHelper
{
    public const LOG_FILE_REG = 'eye4fraud-debug(-[\d]{4})?(-[\d]{2})?(-[\d]{2})?';

    /**
     * @var DirectoryList
     */
    public $directoryList;
    /**
     * @var bool
     */
    public $logEnabled;

    /**
     * @param Context $context
     * @param DirectoryList $directoryList
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList
    ) {
        $this->directoryList = $directoryList;
        parent::__construct($context);
        $this->logEnabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/debug',
            ScopeInterface::SCOPE_STORE
        ) == "1";
    }

    /**
     * Get list of log files
     *
     * @return array
     */
    public function getLogFiles()
    {
        $options = [];
        try {
            $logDir = $this->directoryList->getPath('log');
        } catch (FileSystemException $e) {
            return $options;
        }

        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($logDir),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        $sort = [];
        foreach ($files as $file) {
            if ($file->isDir()) {
                continue;
            }
            $matches = [];
            preg_match("/".$this::LOG_FILE_REG."/", $file, $matches);
            if (!count($matches)) {
                continue;
            }
            $filePath = $file->getRealPath();
            $fileName = $file->getFilename();
            $options[$fileName] = $filePath;
            $sort[] = $fileName;
        }

        array_multisort($sort, SORT_DESC, $options);

        return $options;
    }
}
