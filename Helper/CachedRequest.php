<?php

namespace Eye4Fraud\Connector\Helper;

use Exception;
use Eye4Fraud\Connector\Model\ResourceModel\Request as ResourceRequest;
use Eye4Fraud\Connector\Model\ResourceModel\Status as ResourceStatus;
use Magento\Framework\Serialize\SerializerInterface;
use Eye4Fraud\Connector\Model\Request;
use Eye4Fraud\Connector\Model\ResourceModel\Request\Collection;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;

class CachedRequest
{
    /** @var Collection */
    public $collection;

    /**
     * List of order ids used in request
     * @var array
     */
    public $orderIds;

    /** @var RequestHelper */
    public $helper;

    /** @var bool True if used by cron */
    public $cron;

    /**
     * @var SerializerInterface
     */
    public $serializer;
    /**
     * @var ResourceRequest
     */
    public $resourceRequest;

    /**
     * @var StatusFactory
     */
    public $statusFactory;

    /**
     * @var OrderFactory
     */
    public $orderFactory;
    /**
     * @var Config
     */
    public $config;
    /** @var Status */
    public $fraudStatus;
    /**
     * @var ResourceStatus
     */
    public $resourceStatus;
    /** @var Order */
    public $order;

    /**
     * @param Collection $collection
     * @param RequestHelper $helper
     * @param StatusFactory $statusFactory
     * @param OrderFactory $orderFactory
     * @param ResourceRequest $resourceRequest
     * @param ResourceStatus $resourceStatus
     * @param Config $config
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Collection $collection,
        RequestHelper $helper,
        StatusFactory $statusFactory,
        OrderFactory $orderFactory,
        ResourceRequest $resourceRequest,
        ResourceStatus $resourceStatus,
        Config $config,
        SerializerInterface $serializer
    ) {
        $this->collection = $collection;
        $this->helper = $helper;
        $this->statusFactory = $statusFactory;
        $this->orderFactory = $orderFactory;
        $this->resourceRequest = $resourceRequest;
        $this->resourceStatus = $resourceStatus;
        $this->config = $config;
        $this->serializer = $serializer;
    }

    /**
     * Set cron flag
     *
     * @param bool $value
     */
    public function setCronFlag($value)
    {
        $this->cron = (bool)$value;
    }

    /**
     * Send all requests
     *
     * @throws Exception
     */
    public function sendRequests()
    {
        $this->collection->clear();

        $this->collection->addFieldToFilter('send_time', ['lt' => new \Zend_Db_Expr('now()')]);
        $this->collection->addFieldToFilter('is_ready', 1);
        foreach ($this->collection->getItems() as $item) {
            $this->fraudStatus = null;
            $this->order = null;
            /** @var Request $item */
            try {
                /** @var array $request_data */
                $request_data = $this->serializer->unserialize($item->getData('request_data'));
                if (isset($request_data['LineItems']) && is_array($request_data['LineItems'])) {
                    foreach ($request_data['LineItems'] as $k => $orderItem) {
                        $decodedDesc = urldecode($orderItem['ProductDescription']);
                        $request_data['LineItems'][$k]['ProductDescription'] = $decodedDesc;
                    }
                }
            } catch (\InvalidArgumentException $e) {
                $this->removeAbnormalRequest($item);
                continue;
            }

            if (empty($request_data['OrderNumber'])) {
                continue;
            }

            $order = $this->getOrder($item->getData('increment_id'));
            $request_data['SiteName'] = $this->config->getSiteName($order->getStoreId());
            $request_data['ApiLogin'] = $this->config->getApiLogin($order->getStoreId());
            $request_data['ApiKey'] = $this->config->getApiKey($order->getStoreId());

            $logLine = [
                '  ==  Send request for order:',
                $item->getData('increment_id'),
                'Payment method:',
                $item->getData('payment_method')
            ];
            $this->helper->logInfo(implode(' ', $logLine));
            /** @var Request $item */
            if ($this->helper->sendOrderInfo($request_data, $item)) {
                $status = $this->getFraudStatus($item->getData('increment_id'));
                if ($status->isEmpty()) {
                    $order = $this->getOrder($item->getData('increment_id'));
                    $status->setQueued($order);
                }
                $status->setWaiting();

                try {
                    $this->resourceStatus->save($status);
                    $this->helper->logInfo('Set '.$status->getStatus().' for order ' . $item->getData('increment_id'));
                } catch (Exception $e) {
                    $this->helper->log('Error while saving status: ' . $e->getMessage());
                }
                try {
                    $this->resourceRequest->delete($item);
                } catch (Exception $e) {
                    $this->helper->log('Error while deleting request: ' . $e->getMessage());
                }
                $this->helper->log('Delayed request for order ' . $item->getData('increment_id') . ' removed');
            }
        }
    }

    /**
     * Remove abnormal request
     *
     * @param Request $item
     */
    public function removeAbnormalRequest($item)
    {
        $requestId = implode(
            ':',
            [
                $item->getData('entity_id'),
                $item->getData('quote_id'),
                $item->getData('increment_id')
            ]
        );
        $this->helper->log('Error while unserialize data for #'.$requestId);
        $deleteInterval = (float)$this->config->getRemoveMalformedCachedRequestsTime();
        if (time() - strtotime($item->getData('created_at')) > $deleteInterval*60*60) {
            try {
                $this->resourceRequest->delete($item);
                $this->helper->log('Cached request removed #'.$requestId);
                if ($item->getData('increment_id')) {
                    $status = $this->getFraudStatus($item->getData('increment_id'));
                    if ($status->getId()) {
                        $status->setError('Request serialization error');
                        $this->resourceStatus->save($status);
                        $this->helper->log(
                            'Set '.$status->getStatus().
                            ' for order ' . $item->getData('increment_id')
                        );
                    }
                }
            } catch (Exception $e) {
                $this->helper->log('Error while deleting request #'.$requestId);
            }
        }
    }

    /**
     * Get fraud status object and use cache
     *
     * @param string $incrementId
     * @return Status
     */
    public function getFraudStatus($incrementId)
    {
        if ($this->fraudStatus) {
            return $this->fraudStatus;
        }
        /** @var Status $status */
        $this->fraudStatus = $this->statusFactory->create();
        $this->fraudStatus->loadByIncrementId($incrementId);

        return $this->fraudStatus;
    }

    /**
     * Get order and use cache
     *
     * @param string $incrementId
     * @return Order
     */
    public function getOrder($incrementId)
    {
        if ($this->order) {
            return $this->order;
        }
        $this->order = $this->orderFactory->create();
        $this->order->loadByIncrementId($incrementId);
        return $this->order;
    }
}
