<?php
/**
 * Copyright © 2013-2023 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Helper;

use DateInterval;
use DateTime;
use DOMElement;
use DOMNodeList;
use Exception;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\DomDocument\DomDocumentFactory;
use Magento\Framework\HTTP\Client\Curl;
use Eye4Fraud\Connector\Model\Logger;
use Eye4Fraud\Connector\Model\Request;

/**
 * Sales module base helper
 *
 * @author      Eye4Fraud
 */
class RequestHelper extends AbstractHelper
{
    /** @var Logger */
    public $eye4fraudLogger;
    /** @var bool */
    public $logEnabled;
    /** @var int */
    public $maxSendAttempts = 5;
    /** @var DomDocumentFactory */
    public $domDocumentFactory;
    /** @var Curl */
    public $curl;
    /** @var Config */
    public $config;

    /**
     * @param Context $context
     * @param Logger $logger
     * @param DomDocumentFactory $domDocumentFactory
     * @param Curl $curl
     * @param Config $config
     */
    public function __construct(
        Context $context,
        Logger $logger,
        DomDocumentFactory $domDocumentFactory,
        Curl $curl,
        Config $config
    ) {
        $this->eye4fraudLogger = $logger;
        $this->domDocumentFactory = $domDocumentFactory;
        $this->curl = $curl;
        $this->config = $config;
        parent::__construct($context);
        $this->logEnabled = $this->config->isDebugEnabled();
    }

    /**
     * Write data to log
     *
     * @param string|array $data
     * @param bool $infoLevel
     * @return void
     */
    public function log($data, $infoLevel = false)
    {
        if (!$this->logEnabled) {
            return;
        }

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_object($v)) {
                    $data[$k] = "Object: " . get_class($v);
                }
            }
            $data = print_r($data, true);
        }
        if ($infoLevel) {
            $this->eye4fraudLogger->info($data);
        } else {
            $this->eye4fraudLogger->debug($data);
        }
    }

    /**
     * Short for log on info level
     *
     * @param string|array $data
     * @return void
     */
    public function logInfo($data)
    {
        $this->log($data, true);
    }

    /***
     * Send order info to Eye4Fraud Service
     *
     * @param array $post_array
     * @param Request $requestItem
     * @return bool Send result
     * @throws Exception
     */
    public function sendOrderInfo($post_array, $requestItem)
    {
        if (isset($post_array['PayPalPayerID']) && empty($post_array['PayPalPayerID'])) {
            unset($post_array['PayPalPayerID'], $post_array['PayPalAddressStatus'], $post_array['PayPalPayerStatus']);
        }
        $post_array = array_filter($post_array, function ($v) {
            return $v != '';
        });
        $response = $this->send($post_array);
        //Log $code for bad response if in debug mode
        if ($response != 'ok') {
            $this->log("Order data was sent, but answer is wrong");
            if ($requestItem->getData('attempts') > $this->maxSendAttempts) {
                $allowedResendDate = new DateTime($requestItem->getData('created_at'));
                $timeout = new DateInterval("P{$this->config->getResendTimeout()}D");
                $allowedResendDate->add($timeout);
                $now = new DateTime();
                if ($now > $allowedResendDate) {
                    return false;
                }
                $intervalValue = $this->config->getResendInterval() * 60;
            } else {
                $intervalValue = (int)$this->config->getUpdateInterval();
            }

            try {
                $interval = new DateInterval("PT{$intervalValue}M");
                $date = new DateTime();
                $date->add($interval);
                $this->log(
                    "Queue order ".$requestItem->getData("increment_id").
                    " to resend on " . $date->format('Y-m-d H:i:s')
                );
                $requestItem->setData('send_time', $date->format('Y-m-d H:i:s'));
                $requestItem->setData('attempts', $requestItem->getData('attempts') + 1);
                $requestItem->save();
            } catch (Exception $e) {
                $this->log("Error while caching request: ".$e->getMessage());
            }
            return false;
        }
        return true;
    }

    /**
     * Send request to eye4fraud servers
     *
     * @param  array $post_array
     * @return string
     */
    public function send($post_array)
    {
        // Go back to curl from Magento curl because of http/2 error in framework
        //Log $post_array if in debug mode
        $this->log("Sending post:");
        $this->log($post_array);

        $post_query = http_build_query($post_array);

//        $ch = curl_init('https://www-stag.eye4fraud.com/api/');
        $ch = curl_init('https://www.eye4fraud.com/api/');
//        $this->curl->setOptions([
//            CURLOPT_SSL_VERIFYPEER => false,
//        ]);
//        $this->curl->post('https://www.eye4fraud.com/api/', $post_array);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        //Log $response if in debug mode
        $this->log("Response:");
        $this->log($response);
//        $this->log($this->curl->getBody());

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        if ($this->curl->getStatus() != 200) {
        if ($code != 200) {
            $this->log('Eye4fraud service finished with wrong code: ' . $this->curl->getStatus());
        }
        curl_close($ch);

        return $response;
//        return $this->curl->getBody();
    }

    /**
     * Send request to get the order fraud status from eye4fraud service
     *
     * @param array $post_array
     * @return array
     */
    public function getOrderStatus($post_array)
    {
        $response = $this->send($post_array);
//        $response = '<response>
//    <keyvalue>
//        <key>OrderNumber</key>
//        <value>'.$orderId.'</value>
//    </keyvalue>
//    <keyvalue>
//        <key>StatusCode</key>
//        <value>D</value>
//    </keyvalue>
//    <keyvalue>
//        <key>Description</key>
//        <value>Declined</value>
//    </keyvalue>
//</response>';
        $result = [];
        if ($response && strpos($response, '<response>') !== false) {
            $dom = $this->domDocumentFactory->create();
            $dom->loadXML($response);
            $nodesList = $dom->getElementsByTagName("keyvalue");
            $result['error'] = true;
            $result['status'] = 'RER';
            $result['details'] = 'Unknown service response';
            if ($nodesList->length) {
                $parsedAnswer = $this->parseNode($nodesList);
                $result['error'] = false;
                $result['details'] = trim($parsedAnswer['Description']);
                $result['status'] = trim($parsedAnswer['StatusCode']);
            } else {
                $nodesList = $dom->getElementsByTagName("errors");
                if ($nodesList->length) {
                    $item = $this->parseNodeForError($nodesList);
                    if ($item['error']) {
                        $result['details'] = $item['error'];
                    }
                }
            }
            if ($result['status'] == 'E' && $result['details'] == 'No Order') {
                $result['status'] = StatusInterface::STATUS_NO_ORDER;
            }
        } else {
            $result['error'] = true;
            $result['status'] = 'RER';
            $result['details'] = 'Error Answer';
            if ($response && strlen($response) < 30) {
                $result['details'] = $response;
            }
        }
        return $result;
    }

    /**
     * Parse node to array
     *
     * @param DOMNodeList $nodesList
     * @return array
     */
    public function parseNode($nodesList)
    {
        $parsedAnswer = [];
        /** @var DOMElement $node */
        foreach ($nodesList as $node) {
            $item = [];
            /** @var DOMElement $node2 */
            foreach ($node->childNodes as $node2) {
                if (in_array($node2->nodeName, ['key', 'value'])) {
                    $item[$node2->nodeName] = $node2->nodeValue;
                }
            }
            $parsedAnswer[$item['key']] = $item['value'];
        }

        return $parsedAnswer;
    }

    /**
     * Parse node for error
     *
     * @param DOMNodeList $nodesList
     * @return array
     */
    public function parseNodeForError($nodesList)
    {
        $item = [];
        /** @var DOMElement $node */
        foreach ($nodesList as $node) {
            /** @var DOMElement $node2 */
            foreach ($node->childNodes as $node2) {
                if (in_array($node2->nodeName, ['error'])) {
                    $item[$node2->nodeName] = $node2->nodeValue;
                }
            }
        }
        return $item;
    }

    /**
     * Update card type value for required format
     *
     * @param string $ccType
     * @return string
     */
    public function updateCCType($ccType)
    {
        switch (strtolower($ccType)) {
            case 'vi':
                $cardType = 'VISA';
                break;
            case 'mc':
                $cardType = 'MC';
                break;
            case 'ae':
                $cardType = 'AMEX';
                break;
            case 'di':
                $cardType = 'DISC';
                break;
            default:
                $cardType = $ccType;
                break;
        }
        return $cardType;
    }

    /**
     * Get server variable value
     *
     * @param string $varName
     * @param string $default
     * @return mixed|Parameters|ParametersInterface
     */
    public function getServer($varName, $default = '')
    {
        /** @var Http $request */
        $request = $this->_getRequest();
        return $request->getServer($varName, $default);
    }

    /**
     * Add suffix to order ID for request
     *
     * @param string $incrementId
     * @param Status $status
     * @return string
     */
    public function getIncrementIdVersion(string $incrementId, Status $status)
    {
        $version = $status->getRequestVersion();
        if ($version === null) {
            return $incrementId;
        }
        $suffixCode = ord('A') + (int)$version - 1;
        if ($suffixCode > ord('Z')) {
            $suffixCode = $version - ord('A');
            return $incrementId . '-' . $suffixCode;
        }
        return $incrementId . '-' . sprintf("%c", $suffixCode);
    }
}
