<?php
/**
 * Copyright © 2013-2024 Eye4Fraud. All rights reserved.
 */

namespace Eye4Fraud\Connector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Eye4Fraud\Connector\Helper\Order\Config as OrderConfig;
use Magento\Sales\Model\Order;

class Config extends AbstractHelper
{
    /**
     * @var OrderConfig
     */
    private $orderConfig;

    /**
     * @param Context $context
     * @param OrderConfig $orderConfig
     */
    public function __construct(
        Context $context,
        OrderConfig $orderConfig
    ) {
        parent::__construct($context);
        $this->orderConfig = $orderConfig;
    }

    /**
     * Order management feature is enabled
     *
     * @param null|int|string $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->isSetFlag(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Is module debug enabled
     *
     * @param null|int|string $store
     * @return bool
     */
    public function isDebugEnabled($store = null)
    {
        $moduleEnabled = $this->isEnabled($store);
        $debugEnabled = $this->scopeConfig->isSetFlag(
            'eye4fraud_connector/general/debug',
            ScopeInterface::SCOPE_STORE,
            $store
        );
        return $moduleEnabled && $debugEnabled;
    }

    /**
     * Get new order state for Order Create phase
     *
     * @param null|int|string $store
     * @return string
     */
    public function getCreateState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/create_state',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get site name
     *
     * @param null|int|string $store
     * @return mixed
     */
    public function getSiteName($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/auth/site_name',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get api login
     *
     * @param null|int|string $store
     * @return mixed
     */
    public function getApiLogin($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/auth/login',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get api key
     *
     * @param null|int|string $store
     * @return mixed
     */
    public function getApiKey($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/auth/key',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Is cron enabled
     *
     * @param null|int|string $store
     * @return bool
     */
    public function isCronEnabled($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/cron/cron_enabled',
            ScopeInterface::SCOPE_STORE,
            $store
        ) === '1';
    }

    /**
     * Get update limit time for not final status, days
     *
     * @param null|int|string $store
     * @return int
     */
    public function getUpdateLimit($store = null)
    {
        return (int)($this->scopeConfig->getValue(
            'eye4fraud_connector/general/update_limit',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 7);
    }

    /**
     * Get update limit for No Order status, hours
     *
     * @param null|int|string $store
     * @return int
     */
    public function getUpdateLimitNoOrder($store = null)
    {
        return (int)($this->scopeConfig->getValue(
            'eye4fraud_connector/general/update_limit_no_order',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 2);
    }

    /**
     * Is update limit enabled for Declined status
     *
     * @param null|int|string $store
     * @return bool
     */
    public function isDeclinedLimitEnabled($store = null)
    {
        $isDeclinedLimitEnabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/declined_not_final',
            ScopeInterface::SCOPE_STORE,
            $store
        ) === '1';
        $state = $this->orderConfig->getDeclinedState($store);
        return $isDeclinedLimitEnabled && $state === Order::STATE_HOLDED;
    }

    /**
     * Get update limit for Declined status, hours
     *
     * @param null|int|string $store
     * @return int
     */
    public function getUpdateLimitDeclined($store = null)
    {
        return (int)($this->scopeConfig->getValue(
            'eye4fraud_connector/general/update_limit_declined',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 24);
    }

    /**
     * Get update interval for order, min
     *
     * @param null|int|string $store
     * @return int
     */
    public function getUpdateInterval($store = null)
    {
        return (int)($this->scopeConfig->getValue(
            'eye4fraud_connector/cron/update_interval',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 5);
    }

    /**
     * Get the max allowed time to resend failed orders, days
     *
     * @param int|null|string $store
     * @return int
     */
    public function getResendTimeout($store = null)
    {
        return (int)($this->scopeConfig->getValue(
            'eye4fraud_connector/additional/resend_on_error_for_time',
            ScopeInterface::SCOPE_STORE,
            $store
        ));
    }

    /**
     * Get interval between send attempts of failed orders, hours
     *
     * @param int|null|string $store
     * @return float
     */
    public function getResendInterval($store = null)
    {
        return (float)($this->scopeConfig->getValue(
            'eye4fraud_connector/additional/resend_on_error_interval',
            ScopeInterface::SCOPE_STORE,
            $store
        ));
    }

    /**
     * Get flag if all payment methods allowed to catch
     *
     * @return bool
     */
    public function getAllowAllMethods()
    {
        return $this->scopeConfig->isSetFlag('eye4fraud_connector/additional/allow_all_methods');
    }

    /**
     * Get interval between send attempts of failed orders, hours
     *
     * @return int
     */
    public function getRemoveObsoleteRequestsDays()
    {
        return (int)($this->scopeConfig->getValue('eye4fraud_connector/additional/remove_obsolete_requests_days'));
    }

    /**
     * Get amount of minutes to remove malformed cached request
     *
     * @return int
     */
    public function getRemoveMalformedCachedRequestsTime()
    {
        return (int)($this->scopeConfig->getValue('eye4fraud_connector/additional/wrong_cached_request_delete'));
    }

    /**
     * Is log rotation enabled
     *
     * @param mixed $store
     * @return bool
     */
    public function isLogRotationEnabled($store = null)
    {
        $debugEnabled = $this->isDebugEnabled($store);
        $logRotateEnabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/log_rotate',
            ScopeInterface::SCOPE_STORE,
            $store
        );
        return $debugEnabled && $logRotateEnabled;
    }

    /**
     * Return of months count for logs
     *
     * @param mixed $store
     * @return int
     */
    public function getLogRotationCount($store = null)
    {
        return (int)$this->scopeConfig->getValue(
            'eye4fraud_connector/general/log_files_count',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }
}
