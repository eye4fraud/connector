<?php
/**
 * Helper for statuses collection
 * @author Mikhail Valiushka
 */
namespace Eye4Fraud\Connector\Helper;

use DateInterval;
use DateTime;
use Exception;
use Eye4Fraud\Connector\Model\Request\StatusRequestData;
use Eye4Fraud\Connector\Model\ResourceModel\Status as ResourceStatus;
use Eye4Fraud\Connector\Model\ResourceModel\Status\Collection;
use Eye4Fraud\Connector\Model\ResourceModel\Status\CollectionFactory;
use Eye4Fraud\Connector\Model\Status;

use Eye4Fraud\Connector\Model\StatusInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Zend_Db_Expr;

/**
 * Statuses collection Helper
 */
class Statuses
{
    /**
     * @var ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var StatusRequestData
     */
    public $statusRequestData;

    /** @var array Final statuses which not require update */
    public $finalStatuses = ['A', 'D', 'I', 'C', 'F', 'M', 'INV', 'ALW', 'Q', 'ERR'];

    /**
     * @var CollectionFactory
     */
    public $statusCollectionFactory;

    /**
     * @var bool
     */
    public $cronFlag = false;

    /**
     * Limit orders in collection
     * @var array
     */
    public $orderIds;

    /**
     * @var RequestHelper
     */
    public $helper;

    /**
     * @var array
     */
    public $incrementIds;

    /**
     * @var array
     */
    public $storeIds;
    /**
     * @var OrderFactory
     */
    public $orderFactory;
    /**
     * @var ResourceStatus
     */
    public $resourceStatus;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Emulation
     */
    private $emulation;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var Config
     */
    private $config;

    /**
     * SaveAfter constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StatusRequestData $statusRequestData
     * @param CollectionFactory $statusCollectionFactory
     * @param RequestHelper $requestHelper
     * @param OrderFactory $orderFactory
     * @param ResourceStatus $resourceStatus
     * @param Emulation $emulation
     * @param Json $json
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StatusRequestData $statusRequestData,
        CollectionFactory $statusCollectionFactory,
        RequestHelper $requestHelper,
        OrderFactory $orderFactory,
        ResourceStatus $resourceStatus,
        Emulation $emulation,
        Json $json,
        StoreManagerInterface $storeManager,
        Config $config
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->statusRequestData = $statusRequestData;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->helper = $requestHelper;
        $this->orderFactory = $orderFactory;
        $this->resourceStatus = $resourceStatus;
        $this->emulation = $emulation;
        $this->json = $json;
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * Update fraud statuses by cron
     *
     * @param Store $store
     */
    public function updateFromCron($store)
    {
        $statusCollection = $this->statusCollectionFactory->create();

        $this->addFilterOutdatedStatus($statusCollection);
        $this->addFilterLimitUpdateTime($statusCollection);
        $this->addFilterStore($statusCollection, $store);
        $this->setCronFlag(true);
        $statusCollection->load();

        $orderIds = [];
        $incrementIds = [];
        $storeIds = [];
        /** @var Status $status */
        foreach ($statusCollection as $status) {
            $orderIds[] = $status->getOrderId();
            $incrementIds[$status->getOrderId()] = $status->getIncrementId();
            $storeIds[$status->getOrderId()] = $status->getStoreId();
        }

        $this->setOrderIds($orderIds);
        $this->setIncrementIds($incrementIds);
        $this->setStoreIds($storeIds);

        try {
            $this->processUpdate($statusCollection);
        } catch (Exception $e) {
            $this->helper->log('Error while update statuses: '.$e->getMessage());
        }
    }

    /**
     * Update fraud statuses by mass action
     *
     * @param OrderCollection $collection
     */
    public function updateFromGridAction($collection)
    {
        $orderIds = [];
        $incrementIds = [];
        $storeIds = [];

        foreach ($collection as $order) {
            $orderIds[] = $order->getId();
            $incrementIds[$order->getId()] = $order->getIncrementId();
            $storeIds[$order->getId()] = $order->getStoreId();
        }

        $this->helper->log('******* Process mass action update for: '.implode(',', $orderIds));
        $statusCollection = $this->statusCollectionFactory->create();

        $this->setOrderIds($orderIds, $statusCollection);
        $this->setIncrementIds($incrementIds);
        $this->setStoreIds($storeIds);
        $statusCollection->load();

        try {
            $this->processUpdate($statusCollection, true);
        } catch (Exception $e) {
            $this->helper->log('Error while update statuses: '.$e->getMessage());
        }
        $this->helper->log('Mass action update finished');
    }

    /**
     * Update fraud statuses for sales grid
     *
     * @param array $ids
     * @param array $storeIds
     * @param array $incrementIds
     * @return Collection
     */
    public function prepareStatusesForGrid($ids, $storeIds, $incrementIds)
    {
        $this->helper->log('Load statuses collection for orders grid');
        $statusCollection = $this->statusCollectionFactory->create();

        $this->setOrderIds($ids, $statusCollection);
        $this->setStoreIds($storeIds);
        $this->setIncrementIds($incrementIds);
        $statusCollection->load();

        try {
            $this->processUpdate($statusCollection);
        } catch (Exception $e) {
            $this->helper->log('Error while update statuses: '.$e->getMessage());
        }

        return $statusCollection;
    }

    /**
     * Redeclare after load method for specifying collection items original data
     *
     * @param Collection $statusCollection
     * @param bool $force
     * @return $this
     * @throws Exception
     */
    protected function processUpdate($statusCollection, $force = false)
    {
        $this->helper->log('Process statuses update');

        if ($this->cronFlag) {
            $this->helper->log('Cron job - update ' . count($this->orderIds). " orders");
            if (!empty($this->orderIds)) {
                $this->helper->log("Updated order ids: ".implode(",", $this->orderIds));
            }
        }

        foreach ($this->orderIds as $id) {
            /** @var Status $statusItem */
            $statusItem = $statusCollection->getItemByColumnValue('order_id', $id);

            $storeId = $this->storeIds[$id] ?? $this->storeManager->getDefaultStoreView()->getId();

            if (!$this->config->isEnabled($storeId)) {
                continue;
            }

            $this->statusRequestData->SiteName = $this->config->getSiteName($storeId);
            $this->statusRequestData->ApiLogin = $this->config->getApiLogin($storeId);
            $this->statusRequestData->ApiKey = $this->config->getApiKey($storeId);

            if ($this->statusRequestData->SiteName === null ||
                $this->statusRequestData->ApiLogin === null ||
                $this->statusRequestData->ApiKey === null
            ) {
                continue;
            }

            $finalStatuses = $this->finalStatuses;
            if ($this->config->isDeclinedLimitEnabled($storeId)) {
                $finalStatuses = array_diff($this->finalStatuses, ['D']);
            }

            $cron_update_enabled = $this->config->isCronEnabled($storeId);

            // Create empty status
            if ($statusItem === null) {
                /** @var Status $newItem */
                $newItem = $statusCollection->getNewEmptyItem();
                $this->resourceStatus->load($newItem, $id, 'order_id');

                if (!$newItem->getId()) {
                    $this->helper->log('process update: status for #' . $id . ' is empty, create new');
                    /** @var Status $newItem */
                    $newItem = $statusCollection->getNewEmptyItem();
                    $newItem->setData([
                       'order_id' => $id,
                       'increment_id' => $this->incrementIds[$id],
                       'status' => StatusInterface::STATUS_NOT_RECEIVED,
                       'store_id' => $storeId
                    ]);
                    $newItem->setData('created_at', new Zend_Db_Expr('now()'));
                    $newItem->setData('updated_at', new Zend_Db_Expr('now()'));

                    if ($force || !$cron_update_enabled || ($cron_update_enabled && $this->cronFlag)) {
                        $this->statusRequestData->OrderNumber = $this->incrementIds[$id];
                        $this->helper->log($this->json->serialize($newItem->getData()));
                        $result = $this->helper->getOrderStatus(get_object_vars($this->statusRequestData));
                        $newItem->setData('status', $result['status']);
                        $newItem->setData('details', $result['details']);

                        try {
                            $this->resourceStatus->save($newItem);
                        } catch (Exception $e) {
                            $this->helper->log("Status save error: ".$e->getMessage());
                        }
                    }
                }

                try {
                    $statusCollection->addItem($newItem);
                } catch (Exception $e) {
                    $this->helper->log("Error while adding new fraud status to collection: " . $e->getMessage());
                }

                continue;
            }

            $is_status_final = in_array($statusItem['status'], $finalStatuses);

            if (!$is_status_final &&
                (   $force ||
                    (!$cron_update_enabled && $this->isUpdateAllowed($statusItem)) ||
                    ($cron_update_enabled && $this->cronFlag)
                )
            ) {
                // Update in case if wrong
                if ($statusItem->getStoreId() != $storeId) {
                    $statusItem->setStoreId($storeId);
                }

                $this->statusRequestData->OrderNumber = $this->helper->getIncrementIdVersion(
                    $statusItem->getData('increment_id'),
                    $statusItem
                );

                try {
                    $this->helper->log($this->json->serialize($statusItem->getData()));
                    $result = $this->helper->getOrderStatus(get_object_vars($this->statusRequestData));
                    $statusItem->setData('status', $result['status']);
                    $statusItem->setData('details', $result['details']);
                    $statusItem->setData('updated_at', new Zend_Db_Expr('now()'));
                } catch (Exception $e) {
                    $this->helper->log("Error while adding retrieving new order status: ".$e->getMessage());
                    $statusItem->setData('status', StatusInterface::STATUS_ERROR_ANSWER);
                    $statusItem->setData('details', $e->getMessage());
                    $statusItem->setData('updated_at', new Zend_Db_Expr('now()'));
                }

                try {
                    $this->resourceStatus->save($statusItem);
                } catch (Exception $e) {
                    $this->helper->log("Status save error: ".$e->getMessage());
                }
            }
        }

        return $this;
    }

    /**
     * Check if status can be updated
     *
     * @param Status $status_item
     * @return bool
     * @throws Exception
     */
    protected function isUpdateAllowed($status_item)
    {
        $created_at = new DateTime($status_item->getData('created_at'));

        $intervalValue = $this->config->getUpdateLimit();

        $date_not_final = new DateTime();
        try {
            $interval = new DateInterval("P{$intervalValue}D");
            $date_not_final->sub($interval);
        } catch (Exception $e) {
            $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
        }

        $intervalValue = $this->config->getUpdateLimitNoOrder();

        $date_no_order = new DateTime();
        try {
            $interval = new DateInterval("PT{$intervalValue}H");
            $date_no_order->sub($interval);
        } catch (Exception $e) {
            $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
        }

        $date_declined = null;
        $declinedNotFinal = $this->config->isDeclinedLimitEnabled();
        if ($declinedNotFinal) {
            $intervalValue = $this->config->getUpdateLimitDeclined();
            try {
                $date_declined = new DateTime();
                $interval = new DateInterval("PT{$intervalValue}H");
                $date_declined->sub($interval);
            } catch (Exception $e) {
                $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
            }
        }

        switch ($status_item->getData('status')) {
            case StatusInterface::STATUS_NO_ORDER:
                $result = ($created_at >= $date_no_order);
                break;
            case StatusInterface::STATUS_DECLINED:
                if ($date_declined && $declinedNotFinal) {
                    $result = ($created_at >= $date_declined);
                    break;
                }
            /** Fallthrough */
            default:
                $result = ($created_at >= $date_not_final);
        }

        return $result;
    }

    /**
     * Set cron flag
     *
     * @param bool $value
     */
    public function setCronFlag($value)
    {
        $this->cronFlag = (bool)$value;
    }

    /**
     * Filter "forever not final" statuses
     *
     * @param Collection $statusCollection
     */
    public function addFilterLimitUpdateTime($statusCollection)
    {

        $intervalValue = $this->config->getUpdateLimit();

        $conditions = [];

        try {
            $date_not_final = new DateTime();
            $interval = new DateInterval("P{$intervalValue}D");
            $date_not_final->sub($interval);
            $conditions[] = '(status!="' . StatusInterface::STATUS_NO_ORDER .
                '" and created_at>="' . $date_not_final->format('Y-m-d H:i:s') . '")';
        } catch (Exception $e) {
            $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
        }

        $intervalValue = $this->config->getUpdateLimitNoOrder();

        $interval = new DateInterval("PT{$intervalValue}H");
        $conditions[] = $this->createCondition($interval, StatusInterface::STATUS_NO_ORDER);

        $declinedNotFinal = $this->config->isDeclinedLimitEnabled();
        if ($declinedNotFinal) {
            $intervalValue = $this->config->getUpdateLimitDeclined();

            $interval = new DateInterval("PT{$intervalValue}H");
            $conditions[] = $this->createCondition($interval, StatusInterface::STATUS_DECLINED);
        }
        $conditions = array_filter($conditions, 'strlen');
        $condition = implode(" or ", $conditions);

        $statusCollection->addRawCondition($condition);
    }

    /**
     * Add filter of store
     *
     * @param Collection $statusCollection
     * @param Store $store
     * @return void
     */
    public function addFilterStore($statusCollection, $store)
    {
        $statusCollection->addFieldToFilter('store_id', $store->getId());
    }

    /**
     * Create condition
     *
     * @param DateInterval $interval
     * @param string $status
     * @return string
     */
    private function createCondition($interval, $status)
    {
        $condition = '';
        try {
            $date_no_order = new DateTime();
            $date_no_order->sub($interval);
            $condition = '(status="' . $status .
                '" and created_at>="' . $date_no_order->format('Y-m-d H:i:s') . '")';
        } catch (Exception $e) {
            $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
        }

        return $condition;
    }

    /**
     * Filter final statuses and not yet required to update records
     *
     * @param Collection $statusCollection
     */
    public function addFilterOutdatedStatus($statusCollection)
    {
        $intervalValue = (int)$this->scopeConfig->getValue(
            'eye4fraud_connector/cron/update_interval',
            ScopeInterface::SCOPE_STORE
        );
        try {
            $date = new DateTime();
            $interval = new DateInterval("PT{$intervalValue}M");
            $date->sub($interval);
        } catch (Exception $e) {
            $this->helper->log('Error while calculating DateInterval: ' . $e->getMessage());
        }

        $finalStatuses = $this->finalStatuses;
        if ($this->config->isDeclinedLimitEnabled()) {
            $finalStatuses = array_diff($this->finalStatuses, ['D']);
        }
        $statusCollection->addFieldToFilter('updated_at', ['lteq' => $date->format('Y-m-d H:i:s')]);
        $statusCollection->addFieldToFilter('status', ['nin' => $finalStatuses]);
    }

    /**
     * Remember order store ids
     *
     * @param array $ids
     * @return $this
     */
    public function setStoreIds($ids)
    {
        $this->storeIds = $ids;
        return $this;
    }

    /**
     * Remember order increment ids
     *
     * @param array $ids
     * @return $this
     */
    public function setIncrementIds($ids)
    {
        $this->incrementIds = $ids;
        return $this;
    }

    /**
     * Limit request to order's ids
     *
     * @param array $ids
     * @param Collection|null $statusCollection
     * @return $this
     */
    public function setOrderIds($ids, $statusCollection = null)
    {
        $this->orderIds = $ids;
        if ($statusCollection) {
            $statusCollection->addFieldToFilter('order_id', ['in' => $ids]);
        }
        return $this;
    }

    /**
     * Check Queued statuses
     *
     * @param Store $store
     */
    public function checkQueuedStatuses($store)
    {
        /** @var Collection $statusCollection */
        $statusCollection = $this->statusCollectionFactory->create();
        $statusCollection->addFieldToFilter('status', StatusInterface::STATUS_QUEUED);
        $statusCollection->addFieldToFilter('store_id', $store->getId());
        $statusCollection->getSelect()->joinLeft(
            ['erc' => $statusCollection->getTable('eye4fraud_requests_cache')],
            'erc.increment_id = main_table.increment_id',
            ['request_id' => 'erc.entity_id']
        )->having('erc.entity_id IS NULL');

        /** @var Status $status */
        foreach ($statusCollection as $status) {
            if (!$status->getData('increment_id')) {
                $this->helper->log('Status without order ID: '.$this->json->serialize($status->getData()));
                continue;
            }
            $order = $this->orderFactory->create();
            $order->loadByIncrementId($status->getData('increment_id'));
            $status->setWaiting();
            try {
                $this->resourceStatus->save($status);
            } catch (Exception $e) {
                $this->helper->log('Error while updating status '.$e->getMessage());
            }
        }
    }
}
