<?php
namespace Eye4Fraud\Connector\Helper\Traits;

use Eye4Fraud\Connector\Model\Request\GatheredOrderData;

trait AssignCardType
{
    /**
     * Assign card type
     *
     * @param GatheredOrderData $info
     * @return void
     */
    public function assignCardType($info)
    {
        //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
        switch (strtolower($info->RawCCType)) {
            case 'visa':
            case 'vi':
                $card_type = 'VISA';
                break;
            case 'mastercard':
            case 'mc':
                $card_type = 'MC';
                break;
            case 'american express':
            case 'americanexpress':
            case 'ae':
                $card_type = 'AMEX';
                break;
            case 'discover card':
            case 'discover':
            case 'discovercard':
            case 'dc':
            case 'di':
                $card_type = 'DISC';
                break;
            default:
                $card_type = 'OTHER';
                break;
        }
        $info->CCType = $card_type;
    }
}
