<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Eye4Fraud\Connector\Helper\Order;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Eye4Fraud\Connector\Model\Logger;

/**
 * Helper for order management feature
 *
 * @author      Eye4Fraud
 */
class Config extends AbstractHelper
{
    /** @var Logger */
    protected $_eye4fraudLogger;

    /**
     * @param Context $context
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Logger $logger
    ) {
        $this->_eye4fraudLogger = $logger;
        parent::__construct($context);
    }

    /**
     * Is debug enabled for logging
     *
     * @param null|int|string $store
     * @return bool
     */
    public function isDebugEnabled($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/general/debug',
            ScopeInterface::SCOPE_STORE,
            $store
        ) == "1";
    }

    /**
     * Write log lines
     *
     * @param array $data
     * @param null|int|string $store
     * @return void
     */
    public function log($data, $store = null)
    {
        if (!$this->isDebugEnabled($store)) {
            return;
        }

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_object($v)) {
                    $data[$k] = "Object: " . get_class($v);
                }
            }
            $data = print_r($data, true);
        }
        $this->_eye4fraudLogger->debug($data);
    }

    /**
     * Order management feature is enabled
     *
     * @param null|int|string $store
     * @return bool
     */
    public function enabled($store = null)
    {
        $module_enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/enabled',
            ScopeInterface::SCOPE_STORE,
            $store
        ) == "1";
        $management_enabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/management/enabled',
            ScopeInterface::SCOPE_STORE,
            $store
        ) == "1";

        return ($module_enabled && $management_enabled);
    }

    /**
     * Get new order state for Order Create phase
     *
     * @param null|int|string $store
     * @return string
     */
    public function getCreateState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/create_state',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new status for approved
     *
     * @param null|int|string $store
     * @return bool
     */
    public function getCreateStatus($store = null)
    {
        $state = $this->getCreateState($store);
        $suffix = 'processing';
        if ($state === Order::STATE_HOLDED) {
            $suffix = 'hold';
        }
        if ($state === Order::STATE_PROCESSING) {
            $suffix = 'processing';
        }
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/create_status_'.$suffix,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new order state for approved
     *
     * @return string
     */
    public function getApprovedState()
    {
        return Order::STATE_PROCESSING;
    }

    /**
     * Get new status for insured
     *
     * @param null|int|string $store
     * @return string
     */
    public function getApprovedStatus($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/approved_state_processing',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new order state for Order Create phase
     *
     * @return string
     */
    public function getInsuredState()
    {
        return Order::STATE_PROCESSING;
    }

    /**
     * Get new status for insured
     *
     * @param null|int|string $store
     * @return string
     */
    public function getInsuredStatus($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/insured_state_processing',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new order state for fraud
     *
     * @param null|int|string $store
     * @return string
     */
    public function getFraudState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/fraud_state',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new status for fraud
     *
     * @param null|int|string $store
     * @return bool
     */
    public function getFraudStatus($store = null)
    {
        $state = $this->getFraudState($store);
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/fraud_status_'.$state,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new order state for declined
     *
     * @param null|int|string $store
     * @return string
     */
    public function getDeclinedState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/declined_state',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get new status for declined
     *
     * @param null|int|string $store
     * @return bool
     */
    public function getDeclinedStatus($store = null)
    {
        $state = $this->getDeclinedState($store);
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/declined_status_'.$state,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get auto-invoice option
     *
     * @param null|int|string $store
     * @return bool
     */
    public function getAutoInvoiceState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/auto_invoice',
            ScopeInterface::SCOPE_STORE,
            $store
        )==='1';
    }

    /**
     * Get new order state for Order Create phase
     *
     * @param null|int|string $store
     * @return string
     */
    public function getPaymentCapture($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/payment_capture',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get state for order when capture failed
     *
     * @param null|int|string $store
     * @return string
     */
    public function getCaptureFailedState($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/capture_failed_state',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get status for order when capture failed
     *
     * @param null|int|string $store
     * @return bool
     */
    public function getCaptureFailedStatus($store = null)
    {
        $state = $this->getCaptureFailedState($store);
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/capture_failed_status_'.$state,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get refund type for Order management
     *
     * @param null|int|string $store
     * @return string
     */
    public function getRefundType($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/refund_type',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get refund back to stock flat for Order management
     *
     * @param null|int|string $store
     * @return string
     */
    public function getRefundBackStock($store = null)
    {
        return $this->scopeConfig->getValue(
            'eye4fraud_connector/management/refund_back',
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }
    /**
     * Get count of retries of updating status
     *
     * @param null|int|string $store
     * @return int
     */
    public function getRetryTimes($store = null)
    {
        return (int)$this->scopeConfig->getValue(
            'eye4fraud_connector/management/retry_times',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 0;
    }
    /**
     * Get time between retries, minutes
     *
     * @param null|int|string $store
     * @return int
     */
    public function getTimeBetweenRetries($store = null)
    {
        return (int)$this->scopeConfig->getValue(
            'eye4fraud_connector/management/time_between_retries',
            ScopeInterface::SCOPE_STORE,
            $store
        ) ?? 0;
    }
}
