<?php
/**
 * Copyright © 2013-2022 Eye4Fraud. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eye4Fraud\Connector\Helper\Order;

use Exception;
use Eye4Fraud\Connector\Exception\DelayUpdate;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DB\Transaction;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Store\Model\ScopeInterface;
use Eye4Fraud\Connector\Model\Logger;

/**
 * Helper for order management feature
 *
 * @author      Eye4Fraud
 */
class Management extends AbstractHelper
{
    public const REFUND_SKIP = 'skip';
    public const REFUND_ONLINE = 'online';
    public const REFUND_OFFLINE = 'offline';

    /** @var Logger */
    protected $_eye4fraudLogger;
    /** @var bool */
    protected $_logEnabled;
    /** @var Config */
    protected $orderConfig;
    /** @var OrderRepository */
    protected $orderRepository;
    /** @var InvoiceService */
    protected $invoiceService;
    /** @var TransactionFactory */
    protected $transactionFactory;
    /** @var ScopeConfigInterface */
    protected $scopeConfig;
    /** @var CreditmemoFactory */
    public $creditmemoFactory;
    /** @var CreditmemoManagementInterface */
    public $creditmemoManagement;
    /**
     * @var OrderResource
     */
    private $resourceOrder;
    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @param Context $context
     * @param Logger $logger
     * @param Config $orderConfig
     * @param OrderRepository $orderRepository
     * @param OrderResource $resourceOrder
     * @param OrderFactory $orderFactory
     * @param InvoiceService $invoiceService
     * @param CreditmemoFactory $creditmemoFactory
     * @param CreditmemoManagementInterface $creditmemoManagement
     * @param TransactionFactory $transactionFactory
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        Logger $logger,
        Config $orderConfig,
        OrderRepository $orderRepository,
        OrderResource $resourceOrder,
        OrderFactory $orderFactory,
        InvoiceService $invoiceService,
        CreditmemoFactory $creditmemoFactory,
        CreditmemoManagementInterface $creditmemoManagement,
        TransactionFactory $transactionFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_eye4fraudLogger = $logger;
        $this->orderConfig = $orderConfig;
        $this->orderRepository = $orderRepository;
        $this->resourceOrder = $resourceOrder;
        $this->orderFactory = $orderFactory;
        $this->invoiceService = $invoiceService;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoManagement = $creditmemoManagement;
        $this->transactionFactory = $transactionFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
        $this->_logEnabled = $this->scopeConfig->getValue(
            'eye4fraud_connector/general/debug',
            ScopeInterface::SCOPE_STORE
        ) == "1";
    }

    /**
     * Write a log line
     *
     * @param string|array $data
     * @return void
     */
    public function log($data)
    {
        if (!$this->_logEnabled) {
            return;
        }

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_object($v)) {
                    $data[$k] = "Object: " . get_class($v);
                }
            }
            $data = print_r($data, true);
        }
        $this->_eye4fraudLogger->debug($data);
    }

    /**
     * Update order status after payment was placed
     *
     * @param Payment $payment
     * @return bool
     */
    public function orderCreated($payment)
    {
        if (!$this->orderConfig->enabled()) {
            return false;
        }
        $order = $payment->getOrder();
        $this->log('Order #'.$order->getIncrementId().' created with state "'.$order->getState().'"');
        if ($order->getState()!== Order::STATE_PROCESSING) {
            $this->log('Order state not math "'.Order::STATE_PROCESSING.'", order skipped');
            return false;
        }

        if ($this->orderConfig->getCreateState()===Order::STATE_HOLDED) {
            try {
                $order->hold();
                $this->log('Order #'.$order->getIncrementId().' hold');
            } catch (LocalizedException $e) {
                $this->_eye4fraudLogger->warning("Error while trying to hold order: ".$e->getMessage());
            }
        }

        $order->setStatus($this->orderConfig->getCreateStatus())
            ->addStatusHistoryComment(__("Order status was automatically changed by Eye4Fraud"))
            ->setIsCustomerNotified(null);
        $this->log('Order #'.$order->getIncrementId().' status changed to '.$this->orderConfig->getCreateStatus());

        return true;
    }

    /**
     * Process new fraud status
     *
     * @param Status $fraud_status
     * @return bool
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     * @throws DelayUpdate
     */
    public function fraudStatusUpdated($fraud_status)
    {
        if (!$this->orderConfig->enabled()) {
            return false;
        }
        if ($fraud_status->isObjectNew()
            || $fraud_status->getData('status')===$fraud_status->getOrigData('status')
        ) {
            return false;
        }
        $order = $this->getOrder($fraud_status);
        if ($order===false) {
            $this->_eye4fraudLogger->error("Order status update failed!");
            return false;
        }
        $this->log(sprintf(
            "Process new fraud status for order #%s with state %s",
            $order->getIncrementId(),
            $order->getState()
        ));

        switch ($fraud_status->getStatus()) {
            case StatusInterface::STATUS_APPROVED:
                $result = $this->statusApproved($order);
                break;
            case StatusInterface::STATUS_INSURED:
                $result = $this->statusInsured($order);
                break;
            case StatusInterface::STATUS_DECLINED:
                $result = $this->statusDeclined($order);
                break;
            case StatusInterface::STATUS_FRAUD:
                $result = $this->statusFraud($order);
                break;
            case StatusInterface::STATUS_MISSED_FRAUD:
            case StatusInterface::STATUS_INVALID:
            case StatusInterface::STATUS_ALLOWED:
            case StatusInterface::STATUS_CANCELED:
                $result = $this->unholdOrder($order);
                break;
            default:
                $result = false;
        }
        if ($result) {
            $this->orderRepository->save($order);
        }

        $this->log('New fraud status processing for order #'.$order->getIncrementId().' finished');

        //protected $_finalStatuses = ['A', 'D', 'I', 'C', 'F', 'M', 'INV', 'ALW', 'Q'];
        return $result;
    }

    /**
     * Set approved status
     *
     * @param Order $order
     * @return bool
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function setApprovedStatus($order)
    {
        if (!$this->orderConfig->enabled()) {
            return false;
        }

        $order->setState($this->orderConfig->getApprovedState())->setStatus($this->orderConfig->getApprovedStatus())
            ->addStatusHistoryComment(__("Order was approved by Eye4Fraud"))
            ->setIsCustomerNotified(null);

        $this->orderRepository->save($order);

        return true;
    }

    /**
     * Set approved status
     *
     * @param Order $order
     * @return bool
     * @throws DelayUpdate
     */
    protected function statusApproved($order)
    {
        $this->log('Order #'.$order->getIncrementId().' approve routine');
        if ($order->getState()!== Order::STATE_PROCESSING && $order->getState()!==Order::STATE_HOLDED) {
            $this->log(
                'Order state not math "'.
                Order::STATE_PROCESSING.'" and "'.
                Order::STATE_HOLDED.'", order skipped'
            );
            return false;
        }

        if ($order->getState() === Order::STATE_HOLDED) {
            try {
                $this->log('Unhold order #'.$order->getIncrementId());
                $order->unhold();
            } catch (LocalizedException $e) {
                $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
                return false;
            }
        }

        if (!$this->invoice($order)) {
            return false;
        }

        $order->setState($this->orderConfig->getApprovedState())->setStatus($this->orderConfig->getApprovedStatus())
            ->addCommentToStatusHistory(__("Order was approved by Eye4Fraud"))
            ->setIsCustomerNotified(null);
        $this->log('Order #'.$order->getIncrementId().' approved');

        return true;
    }

    /**
     * Create invoice for order
     *
     * @param Order $order
     * @return bool
     * @throws DelayUpdate
     * @throws Exception
     */
    protected function invoice($order)
    {
        $result = true;
        if (!$this->orderConfig->getAutoInvoiceState()) {
            return $result;
        }
        try {
            if (!$order->canInvoice()) {
                $allProductsFinished = true;
                /** @var Order\Item $item */
                foreach ($order->getItemsCollection() as $item) {
                    if ($item->getQtyToInvoice()) {
                        $allProductsFinished = false;
                        break;
                    }
                }
                if (!$allProductsFinished) {
                    throw new LocalizedException(__('The order does not allow an invoice to be created.'));
                }
                $this->_eye4fraudLogger->error('No need to create invoice');
                return true;
            }

            $invoice = $this->invoiceService->prepareInvoice($order);

            if (!$invoice) {
                throw new LocalizedException(__('We can\'t save the invoice right now.'));
            }

            if (!$invoice->getTotalQty()) {
                throw new LocalizedException(__('You can\'t create an invoice without products.'));
            }

            /** @noinspection PhpUndefinedMethodInspection */
            $invoice->setRequestedCaptureCase($this->orderConfig->getPaymentCapture());

            $invoice->addComment(__("Automatically created invoice by Eye4Fraud"));

            $invoice->setCustomerNote(false);
            $invoice->setCustomerNoteNotify(false);

            $invoice->register();
            $this->log('Order #'.$order->getIncrementId().' invoice registered');

            $invoice->getOrder()->setCustomerNoteNotify(null);
            /** @noinspection PhpUndefinedMethodInspection */
            $invoice->getOrder()->setIsInProcess(true);

            /* @var $saveTransaction Transaction */
            $saveTransaction = $this->transactionFactory->create();
            $saveTransaction->addObject($invoice);
            $saveTransaction->addObject($invoice->getOrder());
            $saveTransaction->save();
            $this->log('Order #'.$order->getIncrementId().' transaction saved');
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->_eye4fraudLogger->error('Error while creating invoice: '.$message);
            if (strpos($message, "Transaction has been declined") !== false) {
                $history = $order->addCommentToStatusHistory("Eye4Fraud capture error: ".$e->getMessage());
                $historyCollection = $order->getStatusHistoryCollection();
                $historyCollection->addItem($history);
                $historyCollection->save();
                throw new DelayUpdate(__("Transaction has been declined"));
            }
            $result = false;
        }
        return $result;
    }

    /**
     * Set insured status
     *
     * @param Order $order
     * @return bool
     * @throws DelayUpdate
     */
    protected function statusInsured($order)
    {
        $this->log('Order #'.$order->getIncrementId().' insure routine');
        if ($order->getState()!== Order::STATE_PROCESSING && $order->getState()!==Order::STATE_HOLDED) {
            $this->log('Order state not math "'.
                Order::STATE_PROCESSING.'" and "'.
                Order::STATE_HOLDED.'", order skipped');
            return false;
        }
        
        if ($order->getState()===Order::STATE_HOLDED) {
            try {
                $order->unhold();
            } catch (LocalizedException $e) {
                $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
                return false;
            }
        }

        if (!$this->invoice($order)) {
            return false;
        }

        $order->setState($this->orderConfig->getInsuredState())->setStatus($this->orderConfig->getInsuredStatus())
            ->addCommentToStatusHistory(__("Order was insured by Eye4Fraud"))
            ->setIsCustomerNotified(null);

        $this->log('Order #'.$order->getIncrementId().' insured');
        return true;
    }

    /**
     * Set declined status
     *
     * @param Order $order
     * @return bool
     */
    protected function statusDeclined($order)
    {
        $this->log('Order #'.$order->getIncrementId().' decline routine');
        if ($order->getState()!== Order::STATE_PROCESSING && $order->getState()!==Order::STATE_HOLDED) {
            $this->log('Order state not math "'.
                Order::STATE_PROCESSING.'" and "'.
                Order::STATE_HOLDED.'", order skipped');
            return false;
        }

        if ($this->orderConfig->getDeclinedState()===Order::STATE_CANCELED) {
            if ($order->getState()===Order::STATE_HOLDED) {
                try {
                    $order->unhold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
                    return false;
                }
            }
            $this->cancelOrder(
                $order,
                $this->orderConfig->getDeclinedStatus(),
                __("Order was declined by Eye4Fraud")
            );
        }
        if ($this->orderConfig->getDeclinedState()===Order::STATE_HOLDED) {
            if ($order->getState()!==Order::STATE_HOLDED) {
                try {
                    $order->hold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to hold order: ".$e->getMessage());
                    return false;
                }
            }
            $order->setStatus($this->orderConfig->getDeclinedStatus())
                ->addStatusHistoryComment(__("Order was declined by Eye4Fraud"))
                ->setIsCustomerNotified(null);
        }

        return true;
    }

    /**
     * Unhold order
     *
     * @param Order $order
     * @return bool
     */
    protected function unholdOrder($order)
    {
        $this->log('Unhold Order');
        if ($order->getState()!==Order::STATE_HOLDED) {
            return false;
        }

        try {
            $order->unhold();
            $order->addCommentToStatusHistory(__("Order was unhold by Eye4Fraud"))
                ->setIsCustomerNotified(null);
        } catch (LocalizedException $e) {
            $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Set fraud status
     *
     * @param Order $order
     * @return bool
     */
    protected function statusFraud($order)
    {
        $this->log('Order #'.$order->getIncrementId().' fraud routine');
        if ($order->getState()!== Order::STATE_PROCESSING && $order->getState()!==Order::STATE_HOLDED) {
            $this->log('Order state not math "'.
                Order::STATE_PROCESSING.'" and "'.
                Order::STATE_HOLDED.'", order skipped');
            return false;
        }

        if ($this->orderConfig->getFraudState()===Order::STATE_CANCELED) {
            if ($order->getState()===Order::STATE_HOLDED) {
                try {
                    $order->unhold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
                    return false;
                }
            }
            $this->cancelOrder(
                $order,
                $this->orderConfig->getFraudStatus(),
                __("Order was recognized as fraud by Eye4Fraud")
            );
        }
        if ($this->orderConfig->getFraudState()===Order::STATE_HOLDED) {
            if ($order->getState()!==Order::STATE_HOLDED) {
                try {
                    $order->hold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to hold order: ".$e->getMessage());
                    return false;
                }
            }
            $order->addCommentToStatusHistory(
                __("Order was recognized as fraud by Eye4Fraud"),
                $this->orderConfig->getFraudStatus()
            );
            $order->setIsCustomerNotified(null);
        }

        return true;
    }

    /**
     * Get order
     *
     * @param Status $fraud_status
     * @param bool $force
     * @return bool|Order
     */
    protected function getOrder($fraud_status, $force = false)
    {
        $order_id = $fraud_status->getOrderId();
        if (!$order_id) {
            return false;
        }

        try {
            /** @var Order $order */
            if ($force) {
                $order = $this->orderFactory->create();
                $this->resourceOrder->load($order, $order_id);
            } else {
                $order = $this->orderRepository->get($order_id);
            }
        } catch (InputException $e) {
            $this->_eye4fraudLogger->warn("Error while loading order ".$order_id.": ".$e->getMessage());
            return false;
        } catch (NoSuchEntityException $e) {
            $this->_eye4fraudLogger->warn("Error while loading order ".$order_id.": ".$e->getMessage());
            return false;
        }
        return $order;
    }

    /**
     * Cancel order
     *
     * @param Order $order
     * @param string $status
     * @param string $comment
     */
    public function cancelOrder($order, $status, $comment)
    {
        try {
            if ($order->canCancel()) {
                $order->cancel();
            } else {
                $this->_eye4fraudLogger->warning("Order can't be cancelled, trying to refund");
                $this->refundOrder($order);
            }
        } catch (Exception $e) {
            $this->_eye4fraudLogger->warning("Error while trying to cancel order: ".$e->getMessage());
            $order->addCommentToStatusHistory(__("Error occurred while cancelled order, order state set to canceled"));
            $order->setState(Order::STATE_CANCELED);
        }

        $order->addCommentToStatusHistory($comment, $status);
        $order->setIsCustomerNotified(null);
    }

    /**
     * Refund Order ID
     *
     * @param Order $order
     * @return void
     */
    public function refundOrder($order)
    {
        switch ($this->orderConfig->getRefundType()) {
            case self::REFUND_ONLINE:
                $invoices = $order->getInvoiceCollection();
                if (!$invoices->count()) {
                    $this->_eye4fraudLogger->warning(
                        __('No invoices found for order #'.$order->getIncrementId())
                    );
                    return;
                }
                $this->refundOnline($invoices);
                break;
            case self::REFUND_OFFLINE:
                $this->refundOffline($order);
                break;
            case self::REFUND_SKIP:
                $this->_eye4fraudLogger->warning(__('Credit memo creation skipped'));
                $order->addCommentToStatusHistory(__("Creditmemo skipped, order state set to canceled"));
                $order->setState(Order::STATE_CANCELED);
                break;
            default:
                $this->_eye4fraudLogger->warning(__('Wrong refund type in settings'));
        }
    }

    /**
     * Refund Offline
     *
     * @param Order $order
     * @return bool
     */
    public function refundOffline($order)
    {
        if (!$order->canCreditmemo()) {
            $this->_eye4fraudLogger->warning(
                __('We can\'t create credit memo for the order '.$order->getIncrementId())
            );
            return false;
        }

        $data = [
            'qtys' => []
        ];
        $creditmemo = $this->creditmemoFactory->createByOrder($order, $data);
        $this->setBackToStock($creditmemo);
        $this->_eventManager->dispatch(
            'adminhtml_sales_order_creditmemo_register_before',
            ['creditmemo' => $creditmemo, 'input' => null]
        );
        $this->creditmemoManagement->refund($creditmemo, true);
        $this->_eye4fraudLogger->debug("Order #".$order->getIncrementId()." was refunded offline");
        return true;
    }

    /**
     * Online refund
     *
     * @param Collection $invoices
     * @return bool
     */
    public function refundOnline($invoices)
    {
        $data = [
            'qtys' => []
        ];

        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            if (!$invoice->canRefund()) {
                $this->_eye4fraudLogger->warning(
                    __('We can\'t create credit memo for the invoice #'.$invoice->getIncrementId())
                );
                return false;
            }

            $creditmemo = $this->creditmemoFactory->createByInvoice($invoice, $data);
            $this->setBackToStock($creditmemo);
            $this->_eventManager->dispatch(
                'adminhtml_sales_order_creditmemo_register_before',
                ['creditmemo' => $creditmemo, 'input' => null]
            );
            $this->creditmemoManagement->refund($creditmemo, false);
            $this->_eye4fraudLogger->debug("Invoice #".$invoice->getIncrementId()." was refunded online");
        }

        return true;
    }

    /**
     * Set back in stock flag for items in creditmemo
     *
     * @param Order\Creditmemo $creditmemo
     */
    public function setBackToStock($creditmemo)
    {
        $backToStock = $this->orderConfig->getRefundBackStock() === '1';
        /**
         * Process back to stock flags
         * @var $creditmemoItem
         */
        foreach ($creditmemo->getAllItems() as $creditmemoItem) {
            if ($backToStock) {
                /** @noinspection PhpUndefinedMethodInspection */
                $creditmemoItem->setBackToStock(true);
            } else {
                /** @noinspection PhpUndefinedMethodInspection */
                $creditmemoItem->setBackToStock(false);
            }
        }
    }

    /**
     * Set order to requied state and status after capture failed
     *
     * @param Status $fraudStatus
     * @return bool
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function setOrderFailedCapture($fraudStatus)
    {
        if (!$this->orderConfig->enabled()) {
            return false;
        }
        $order = $this->getOrder($fraudStatus, true);
        if ($order === false) {
            $this->_eye4fraudLogger->error("Order status update failed!");
            return false;
        }
        $this->log(
            sprintf("Capture failed route, update order %s state %s", $order->getIncrementId(), $order->getState())
        );

        if ($this->orderConfig->getCaptureFailedState() === Order::STATE_CANCELED) {
            if ($order->getState()===Order::STATE_HOLDED) {
                try {
                    $order->unhold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to unhold order: ".$e->getMessage());
                    return false;
                }
            }
            $this->cancelOrder(
                $order,
                $this->orderConfig->getCaptureFailedStatus(),
                __("Order was cancelled by Eye4Fraud, capture failed")
            );
        }
        if ($this->orderConfig->getCaptureFailedState()===Order::STATE_HOLDED) {
            if ($order->getState()!==Order::STATE_HOLDED) {
                try {
                    $order->hold();
                } catch (LocalizedException $e) {
                    $this->_eye4fraudLogger->warning("Error while trying to hold order: ".$e->getMessage());
                    return false;
                }
            }
            $order->addCommentToStatusHistory(
                __("Order was processed by Eye4Fraud, but capture failed"),
                $this->orderConfig->getCaptureFailedStatus()
            )->setIsCustomerNotified(null);
        }

        $this->orderRepository->save($order);

        return true;
    }
}
