<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Eye4Fraud_Connector',
    __DIR__
);
