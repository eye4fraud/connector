<?php

namespace Eye4Fraud\Connector\Api;

use Eye4Fraud\Connector\Model\Request;
use Eye4Fraud\Connector\Model\ResourceModel\Request\Collection;
use Magento\Framework\Exception\AlreadyExistsException;

interface RequestRepositoryInterface
{
    /**
     * Save request
     *
     * @param Request $cachedRequest
     * @return self
     * @throws AlreadyExistsException
     */
    public function save(Request $cachedRequest): RequestRepositoryInterface;

    /**
     * Get request by ID
     *
     * @param string $cachedRequestId
     * @return Request
     */
    public function get(string $cachedRequestId): Request;

    /**
     * Get collection
     *
     * @return Collection
     */
    public function getCollection(): Collection;

    /**
     * Get not active request for order
     *
     * @param string $orderIncrementId
     * @return Collection
     */
    public function getNotActiveRequests(string $orderIncrementId): Collection;

    /**
     * Get all not active requests since
     *
     * @param string $since
     * @return Collection
     */
    public function getOldNotActiveRequests(string $since): Collection;

    /**
     * Create new instance
     *
     * @return Request
     */
    public function create(): Request;

    /**
     * Delete request.
     *
     * @param Request $request
     */
    public function delete(Request $request);

    /**
     * Delete request by ID.
     *
     * @param int $requestId
     */
    public function deleteById(int $requestId);
}
