<?php
/**
 */
namespace Eye4Fraud\Connector\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Cached saved card
 *
 * @api
 */
interface SavedCardInterface extends ExtensibleDataInterface
{
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $cardId
     * @return SavedCardInterface
     */
    public function setCardId($cardId);

    /**
     * Get method code
     *
     * @return string
     */
    public function getMethod();

    /**
     * Get cached card data
     *
     * @return array
     */
    public function getCachedData();

    /**
     * Set cached card data
     *
     * @param array $cachedData
     * @return $this
     */
    public function setCachedData($cachedData);

    /**
     * Set method code
     *
     * @param string $method
     * @return $this
     */
    public function setPaymentMethod($method);

    /**
     * Get created at date
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set created at date
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);
}
