<?php
/**
 * Saved Card Repository
 */

namespace Eye4Fraud\Connector\Api;

use Eye4Fraud\Connector\Api\Data\SavedCardInterface;
use Eye4Fraud\Connector\Model\ResourceModel\SavedCard\Collection;
use Eye4Fraud\Connector\Model\SavedCard;
use Magento\Framework\Exception\LocalizedException;

/**
 * Card CRUD interface.
 *
 * @api
 */
interface SavedCardRepositoryInterface
{
    /**
     * Save card.
     *
     * @param SavedCardInterface $card
     * @return SavedCard
     * @throws LocalizedException
     */
    public function save($card);

    /**
     * Retrieve card. Will accept numeric ID
     *
     * @param string $cardId
     * @return SavedCardInterface
     * @throws LocalizedException
     */
    public function getById($cardId);

    /**
     * Retrieve card.
     *
     * @param int $cardId
     * @return SavedCardInterface
     * @throws LocalizedException
     */
    public function load($cardId);

    /**
     * Load saved card by card ID and payment method
     *
     * @param string $paymentMethod
     * @param int $cardId
     * @return SavedCardInterface
     */
    public function loadCard($paymentMethod, $cardId);

    /**
     * Load saved card by card public hash and payment method
     *
     * @param string $paymentMethod
     * @param string $cardHash
     * @param int $customerId
     * @return SavedCardInterface
     */
    public function loadCardByHash($paymentMethod, $cardHash, $customerId);

    /**
     * Retrieve cards matching the specified criteria.
     *
     * @return Collection
     * @throws LocalizedException
     */
    public function getCollection();

    /**
     * Create new instance
     *
     * @return SavedCardInterface
     */
    public function create();

    /**
     * Delete card.
     *
     * @param SavedCardInterface $card
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete($card);

    /**
     * Delete card by ID.
     *
     * @param int $cardId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($cardId);
}
