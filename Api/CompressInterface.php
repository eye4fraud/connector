<?php

namespace Eye4Fraud\Connector\Api;

interface CompressInterface
{
    /**
     * Compress file
     *
     * @param string $sourceFile
     * @param string $destinationFile
     * @return string|bool
     */
    public function compressFile($sourceFile, $destinationFile);

    /**
     * Check if target exists, file extensions may differ
     *
     * @param string $destinationFile
     * @return bool
     */
    public function checkExists($destinationFile);

    /**
     * Get file with dir and extension
     *
     * @param string $destinationFile
     * @return string
     */
    public function getFileFullPath($destinationFile);
}
