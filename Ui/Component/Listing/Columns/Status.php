<?php
/**
 * Copyright © 2013-2020 Eye4Fraud. All rights reserved.
 */
namespace Eye4Fraud\Connector\Ui\Component\Listing\Columns;

use Exception;
use Eye4Fraud\Connector\Helper\CachedRequest;
use Eye4Fraud\Connector\Helper\Statuses;
use Eye4Fraud\Connector\Model\Logger;
use Eye4Fraud\Connector\Model\StatusInterface;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Date
 */
class Status extends Column
{

    /**
     * Eye4Fraud delayed requests collection
     * @var CachedRequest
     */
    protected $cachedRequestsHelper;

    /**
     * @var Statuses
     */
    protected $statusesHelper;

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var Escaper
     */
    public $escaper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param CachedRequest $cachedRequestHelper
     * @param Statuses $statusesHelper
     * @param Logger $logger
     * @param Escaper $escaper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CachedRequest $cachedRequestHelper,
        Statuses $statusesHelper,
        Logger $logger,
        Escaper $escaper,
        array $components = [],
        array $data = []
    ) {
        $this->cachedRequestsHelper = $cachedRequestHelper;
        $this->statusesHelper = $statusesHelper;
        $this->logger = $logger;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare data source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $this->logger->info('************ Prepare data for orders grid **************');

        $this->cachedRequestsHelper->sendRequests();

        if (!isset($dataSource['data']) || !is_array($dataSource['data']) || !count($dataSource['data'])) {
            $this->logger->error('Data in dataSource wrong');
            return $dataSource;
        }

        if (!isset($dataSource['data']['items']) ||
            !is_array($dataSource['data']['items']) ||
            !count($dataSource['data']['items'])
        ) {
            $this->logger->error('Items in dataSource wrong');
            return $dataSource;
        }

        $ids = [];
        $incrementIds = [];
        foreach ($dataSource['data']['items'] as $item) {
            $ids[] = $item['entity_id'];
            $incrementIds[$item['entity_id']] = $item['increment_id'];
        }

        $storeIds = $this->getStoreIds();

        $collection = $this->statusesHelper->prepareStatusesForGrid($ids, $storeIds, $incrementIds);

        foreach ($dataSource['data']['items'] as &$item) {
            $statusItem = $collection->getItemByColumnValue('order_id', $item['entity_id']);

            if ($statusItem === null) {
                $item['eye4fraud_status'] = "<span>".__('status:' . StatusInterface::STATUS_NO_ORDER)."</span>";
                continue;
            }

            $details_tooltip = '';
            if ($statusItem->getData('details')) {
                $details_tooltip = $statusItem->getData('details');
            }
            try {
                $detailsTooltip = $this->escaper->escapeHtml($details_tooltip);
            } catch (Exception $e) {
                $detailsTooltip = "";
            }
            $item['eye4fraud_status'] = '<span title="'.strip_tags($detailsTooltip).'">'
                .__("status:" . $statusItem['status'])
                ."</span>";
        }

        return $dataSource;
    }

    /**
     * Get storeIds for orders from unprocessed data
     *
     * @return array
     */
    private function getStoreIds()
    {
        $ordersData = $this->getContext()->getDataProvider()->getData();
        return array_column($ordersData['items'], 'store_id', 'entity_id');
    }
}
