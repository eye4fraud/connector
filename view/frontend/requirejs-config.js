/**
 * Copyright © 2013-2020 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/place-order': {
                'Eye4Fraud_Connector/js/model/place-order-mixin': true
            },
            'Magento_Checkout/js/action/set-payment-information': {
                'Eye4Fraud_Connector/js/model/set-payment-information-mixin': true
            },
            'ParadoxLabs_Authnetcim/js/accept': {
                'Eye4Fraud_Connector/js/model/paradox-authorizenet-mixin': true
            }
        }
    }
};
