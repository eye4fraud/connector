/**
 * Copyright © 2017 Eye4Fraud, Inc. All rights reserved.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Eye4Fraud_Connector/js/model/card-details-assigner'
], function ($, wrapper, cardDetailsAssigner) {
    'use strict';

    return function (placeOrderAction) {

        /** Override place-order-mixin for set-payment-information action as they differs only by method signature */
        return wrapper.wrap(placeOrderAction, function (originalAction, messageContainer, paymentData) {
            cardDetailsAssigner(paymentData);

            return originalAction(messageContainer, paymentData);
        });
    };
});
