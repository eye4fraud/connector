/**
 * Copyright © 2017 Eye4Fraud, Inc. All rights reserved.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery'
], function ($) {
    'use strict';

    var inserted = false;

    return function (widget) {

        $.widget('mage.authnetcimAcceptjs', widget, {
            handlePaymentResponse: function (response) {
                if (response.messages.resultCode !== 'Error') {
                    var $first6 = $('<input type="hidden" name="payment[cc_first6]" id="authnetcim-cc-first6">');
                    if (!inserted) {
                        $('#authnetcim-cc-last4').after($first6);
                    }
                    inserted = true;
                    var card_number = $('#authnetcim-cc-number').val();
                    card_number = card_number.replace(/[\D]/g, '');
                    $first6.val(card_number.substr(0,6));
                }
                return this._super(response);
            }
        });

        return $.mage.authnetcimAcceptjs;
    }
});
