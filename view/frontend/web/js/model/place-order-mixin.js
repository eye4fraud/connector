/**
 * Copyright © 2017 Eye4Fraud, Inc. All rights reserved.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Eye4Fraud_Connector/js/model/card-details-assigner'
], function ($, wrapper, cardDetailsAssigner) {
    'use strict';

    return function (placeOrderAction) {

        /** Override default place order action and add agreement_ids to request */
        return wrapper.wrap(placeOrderAction, function (originalAction, paymentData, messageContainer) {
            cardDetailsAssigner(paymentData);

            return originalAction(paymentData, messageContainer);
        });
    };
});