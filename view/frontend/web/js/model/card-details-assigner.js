/**
 * Copyright © 2013-2020 Eye4Fraud, Inc. All rights reserved.
 */

/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery'
], function ($) {
    'use strict';

    var getCardNumber = function (method) {
        var number = '';
        var $card_number;
        switch (method) {
            case 'braintree':{
                number = window.cc_first6;
                break;
            }
            case 'authnetcim':{
                $card_number = $('#'+method+'-cc-number');
                number = '';
                if ($card_number.length) {
                    number = $card_number.val();
                    number = number.replace(/[\D]/g, '');
                    number = number.slice(0,6);
                }
                break;
            }
            default:{
                $card_number = $('#'+method+'_cc_number');
                number = '';
                if ($card_number.length) {
                    number = $card_number.val();
                    number = number.replace(/[\D]/g, '');
                    number = number.slice(0,6);
                }
                break;
            }
        }
        return number;
    };


    /** Override default place order action and add agreement_ids to request */
    return function (paymentData) {
        var cc_first6 = getCardNumber(paymentData['method']);
        if (!cc_first6) {
            return;
        }

        if (typeof(paymentData['additional_data'])==='undefined') {
            paymentData['additional_data'] = {};
        }
        if (paymentData['additional_data']===null) {
            paymentData['additional_data'] = {};
        }
        paymentData['additional_data']['cc_first6'] = cc_first6;

    };
});
