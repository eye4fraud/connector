<?php
/**
 * Copyright © 2021 Eye4Fraud. All rights reserved.
 */

namespace Eye4Fraud\Connector\Cron;

use DateInterval;
use DateTime;
use Exception;
use Eye4Fraud\Connector\Api\RequestRepositoryInterface;
use Eye4Fraud\Connector\Block\Adminhtml\Form\Field\Version as VersionField;
use Eye4Fraud\Connector\Helper\CachedRequest;
use Eye4Fraud\Connector\Helper\Config;
use Eye4Fraud\Connector\Helper\Statuses;
use Eye4Fraud\Connector\Model\Logger;
use Eye4Fraud\Connector\Model\Request;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Store\Model\ScopeInterface;

class RequestStatuses
{
    /** @var CachedRequest  */
    public $cachedRequestsHelper;
    /** @var ScopeConfigInterface */
    public $scopeConfig;
    /** @var Logger */
    public $logger;
    /**
     * @var Statuses
     */
    public $statusesHelper;
    /**
     * @var State
     */
    public $state;
    /**
     * @var Emulation
     */
    public $emulation;
    /**
     * @var Config
     */
    public $config;
    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var bool
     */
    public $force;
    /**
     * @var ModuleListInterface
     */
    public $moduleList;
    /**
     * @var ProductMetadataInterface
     */
    public $productMetadata;
    /**
     * @var RequestRepositoryInterface
     */
    public $requestRepository;

    /**
     * RequestStatuses constructor.
     * @param CachedRequest $cachedRequestsHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $config
     * @param Logger $logger
     * @param Statuses $statusesHelper
     * @param Emulation $emulation
     * @param State $state
     * @param StoreManagerInterface $storeManager
     * @param ModuleListInterface $moduleList
     * @param RequestRepositoryInterface $requestRepository
     * @param ProductMetadataInterface $productMetadata
     * @throws LocalizedException
     */
    public function __construct(
        CachedRequest $cachedRequestsHelper,
        ScopeConfigInterface $scopeConfig,
        Config $config,
        Logger $logger,
        Statuses $statusesHelper,
        Emulation $emulation,
        State $state,
        StoreManagerInterface $storeManager,
        ModuleListInterface $moduleList,
        RequestRepositoryInterface $requestRepository,
        ProductMetadataInterface $productMetadata
    ) {
        $this->cachedRequestsHelper = $cachedRequestsHelper;
        $this->scopeConfig = $scopeConfig;
        $this->config = $config;
        $this->logger = $logger;
        $this->statusesHelper = $statusesHelper;
        $this->emulation = $emulation;
        $this->state = $state;
        $this->storeManager = $storeManager;
        $this->moduleList = $moduleList;
        $this->requestRepository = $requestRepository;
        $this->productMetadata = $productMetadata;
        try {
            $state->getAreaCode();
        } catch (LocalizedException $e) {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        }
    }

    /**
     * Send delayed request and receive statuses
     *
     * @param bool $force
     * @return void
     */
    public function execute($force = false)
    {
        $this->force = $force;
        $version = $this->moduleList->getOne(VersionField::MODULE_NAME)['setup_version'];
        $magentoVersion = $this->productMetadata->getVersion();
        try {
            $this->logger->info('******* Start cron ' . $magentoVersion . ":". $version .' ********');
            $this->sendDelayedOrders();
            if ($this->config->getAllowAllMethods()) {
                $this->clearInactiveRequests();
            }

            $stores = $this->storeManager->getStores();
            /** @var Store $store */
            foreach ($stores as $store) {
                $this->emulation->startEnvironmentEmulation($store->getId());
                if (!$store->getIsActive()) {
                    continue;
                }
                $enabled = $this->config->isEnabled($store->getId());
                if (!$enabled) {
                    continue;
                }

                $this->retrieveStatuses($store);
                $this->rotateLogs();
                $this->emulation->stopEnvironmentEmulation();
            }
        } catch (\Exception $e) {
            $this->logger->error('An error occurred: ' . $e->getMessage());
        }
    }

    /**
     * Send cached orders
     */
    public function sendDelayedOrders()
    {
        $this->cachedRequestsHelper->setCronFlag(true);
        $this->cachedRequestsHelper->sendRequests();
    }

    /**
     * Update fraud statuses
     *
     * @param Store $store
     */
    protected function retrieveStatuses($store)
    {
        $enabled = $this->config->isCronEnabled($store);
        if (!$this->force && !$enabled) {
            return;
        }
        $this->statusesHelper->checkQueuedStatuses($store);
        $this->statusesHelper->updateFromCron($store);
    }

    /**
     * Clear old obsolete requests
     *
     * @return void
     */
    protected function clearInactiveRequests()
    {
        $days = $this->config->getRemoveObsoleteRequestsDays();
        $since = new DateTime();
        $interval = new DateInterval("P{$days}D");
        $since->sub($interval);
        $collection = $this->requestRepository->getOldNotActiveRequests($since->format('Y-m-d H:i:s'));
        if ($collection->count()) {
            /** @var Request $cachedRequest */
            foreach ($collection as $cachedRequest) {
                $this->logger->info(
                    'Remove obsolete cached request for order #'
                    .$cachedRequest->getData('increment_id')
                    .' created at '
                    .$cachedRequest->getData('created_at')
                );
                $this->requestRepository->deleteById($cachedRequest->getId());
            }
        }
    }

    /**
     * Rotate log files
     */
    public function rotateLogs()
    {
        $date = date('d');
        if ($this->config->isLogRotationEnabled() && $date === '01') {
            try {
                $this->logger->rotateLogs($this->config->getLogRotationCount());
            } catch (FileSystemException $e) {
                $this->logger->error("Rotation logs error: ".$e->getMessage());
            }
        }
    }
}
