<?php
/**
 * Plugin for paypal method to catch additional data
 * @author Mikhail Valiushka
 */

namespace Eye4Fraud\Connector\Plugin;

use Magento\Paypal\Model\Api\Nvp;
use Magento\Paypal\Model\Info;

class PayPalPlugin
{
    /**
     * Collect payment info
     *
     * @param Nvp $subject
     * @param callable $proceed
     * @param string $methodName
     * @param array $request
     * @return mixed
     */
    public function aroundCall(Nvp $subject, callable $proceed, $methodName, array $request)
    {
        $returnValue = $proceed($methodName, $request);
        if ($methodName===Nvp::GET_EXPRESS_CHECKOUT_DETAILS) {
            if (isset($returnValue['PAYERSTATUS'])) {
                $subject->setData(Info::PAYER_STATUS, $returnValue['PAYERSTATUS']);
            }
            if (isset($returnValue['ADDRESSSTATUS'])) {
                $address_status = ($returnValue['ADDRESSSTATUS']==='Y'?'confirmed':'unconfirmed');
                $subject->setData(Info::ADDRESS_STATUS, $address_status);
            }
        }
        if ($methodName===Nvp::DO_EXPRESS_CHECKOUT_PAYMENT) {
            if (isset($returnValue['PAYMENTTYPE'])) {
                $subject->setData('payment_type', $returnValue['PAYMENTTYPE']);
            }
        }
        return $returnValue;
    }
}
