<?php

namespace Eye4Fraud\Connector\Plugin\Sales\ResourceModel\Order;

use Exception;
use Eye4Fraud\Connector\Model\ResourceModel\Status as StatusResource;
use Eye4Fraud\Connector\Model\Status;
use Eye4Fraud\Connector\Model\StatusFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Sales\Model\Order as MagentoOrder;
use Eye4Fraud\Connector\Model\Logger;
use Magento\Sales\Model\ResourceModel\Order as MagentoOrderResource;
use Magento\Store\Model\App\Emulation;

class Order
{
    /**
     * @var StatusFactory
     */
    public $statusFactory;

    /**
     * @var StatusResource
     */
    public $statusResource;

    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var Emulation
     */
    public $emulation;

    /**
     * @var ManagerInterface
     */
    public $eventManager;

    /**
     * @param StatusFactory $statusFactory
     * @param StatusResource $statusResource
     * @param Emulation $emulation
     * @param ManagerInterface $eventManager
     * @param Logger $logger
     */
    public function __construct(
        StatusFactory $statusFactory,
        StatusResource $statusResource,
        Emulation $emulation,
        ManagerInterface $eventManager,
        Logger $logger
    ) {
        $this->statusFactory = $statusFactory;
        $this->statusResource = $statusResource;
        $this->emulation = $emulation;
        $this->eventManager = $eventManager;
        $this->logger = $logger;
    }

    /**
     * Update request version and resend order
     *
     * @param MagentoOrderResource $subject1
     * @param MagentoOrderResource $subject2
     * @param MagentoOrder $order
     * @return void
     */
    public function afterSave($subject1, $subject2, $order)
    {
        try {
            if ($order->getOrigData('grand_total') !== null
                && $order->getGrandTotal() !== null
                && (float)$order->getGrandTotal() !== (float)$order->getOrigData('grand_total')
            ) {
                $status = $this->getFraudStatus($order);
                $lastVersion = (int)$status->getRequestVersion();
                $status->setRequestVersion($lastVersion + 1);
                $this->statusResource->save($status);
                $this->emulation->startEnvironmentEmulation($order->getStoreId());
                $this->eventManager->dispatch(
                    'eye_fraud_resend_order',
                    ['payment' => $order->getPayment(), 'resend_order' => 1]
                );
                $this->emulation->stopEnvironmentEmulation();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Load E4F status
     *
     * @param MagentoOrder $order
     * @return Status
     */
    public function getFraudStatus(MagentoOrder $order)
    {
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->loadByIncrementId($order->getIncrementId());
        return $status;
    }
}
