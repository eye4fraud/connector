<?php
/**
 * Plugin for paypal method to catch additional data
 * @author Mikhail Valiushka
 */

namespace Eye4Fraud\Connector\Plugin;

use Eye4Fraud\Connector\Api\SavedCardRepositoryInterface;
use Eye4Fraud\Connector\Model\Request\GatheredOrderData;
use ParadoxLabs\TokenBase\Api\Data\CardInterface;

class ParadoxAuthnetSavePlugin
{

    /**
     * @var SavedCardRepositoryInterface
     */
    protected $savedCardRepository;

    /**
     * @var GatheredOrderData
     */
    protected $info;

    /**
     * @var string
     */
    protected $paymentMethod = 'authnetcim';

    /**
     * @param SavedCardRepositoryInterface $savedCardRepository
     * @param GatheredOrderData $info
     */
    public function __construct(
        SavedCardRepositoryInterface $savedCardRepository,
        GatheredOrderData $info
    ) {
        $this->savedCardRepository = $savedCardRepository;
        $this->info = $info;
    }

    /**
     * Process aftersave event
     *
     * @param CardInterface $card
     * @return CardInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterAfterSave(CardInterface $card)
    {

        /** Card marked for deletion */
        if ($card->getActive()===0) {
            $savedCard = $this->savedCardRepository->loadCard($this->paymentMethod, $card->getHash());
            if ($savedCard!==false) {
                $this->savedCardRepository->delete($savedCard);
            }
            return $card;
        }

        if (!$this->info->CCFirst6) {
            return $card;
        }

        $cachedData = [
            'CCFirst6' => $this->info->CCFirst6,
            'CCLast4' =>  $this->info->CCLast4
        ];

        $savedCard = $this->savedCardRepository->loadCard($this->paymentMethod, $card->getHash());

        if (!$savedCard) {
            $savedCard = $this->savedCardRepository->create();
            $savedCard->setPaymentMethod($this->paymentMethod);
            $savedCard->setCardId($card->getHash());
        }

        $savedCard->setCachedData($cachedData);
        $this->savedCardRepository->save($savedCard);

        return $card;
    }
}
