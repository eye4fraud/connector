## How to install: ##

Add link to repository:

*composer config repositories.eye4fraud vcs https://eye4fraud@bitbucket.org/eye4fraud/connector.git*

Request extension from repository:

Add extension repository to Magento path:

* composer require eye4fraud/connector:dev-master

Install it:

* php bin/magento setup:upgrade

* php bin/magento setup:static-content:deploy

Clear cache

* var/cache

* var/generation



Extension can be update later with command:

* run to update extension from repository:

* * composer update eye4fraud/connector

* run to update database scheme to latest version:

* * php bin/magento setup:upgrade

remove files:

* pub\static\frontend\Magento\luma\en_US\Eye4Fraud_Connector\js\view\eye4fraud-override.js

* pub\static\frontend\Magento\blank\en_US\Eye4Fraud_Connector\js\view\eye4fraud-override.js

run to deploy latest version of script eye4fraud-override.js to themes:

* php bin/magento setup:static-content:deploy

clear magento cache from admin panel or by deleting folder

* var\cache

* var\generation

- master branch is for Magento2 release version

- master_beta is for Magento2 Merchant Beta version